package mesh;

public class MeshPathListEntry {

	private String meshPointLocalInterfaceName;
	
	private String meshPointLocalMacAddress;
	private String destinationMacAddress;
	private String nextHopMacAddress;
	
	private long destinationSequenceNumber;
	private long metric;
	private long expirationTime;
	
	public String getMeshPointLocalInterfaceName() {
		return meshPointLocalInterfaceName;
	}
	
	public void setMeshPointLocalMacAddress(String mac) {
		meshPointLocalMacAddress = mac;
	}
	public String getMeshPointLocalMacAddress() {
		return meshPointLocalMacAddress;
	}
	
	public String getDestinationMacAddress() {
		return destinationMacAddress;
	}
	public String getNextHopMacAddress() {
		return nextHopMacAddress;
	}
	public long getDestinationSequenceNumber() {
		return destinationSequenceNumber;
	}
	public long getMetric() {
		return metric;
	}
	public long getExpirationTime() {
		return expirationTime;
	}
	
}
