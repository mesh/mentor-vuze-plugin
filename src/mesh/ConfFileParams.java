package mesh;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

//TODO: mesh modification

public class ConfFileParams
{
	
	private boolean VANILLA; // switch between unmodified choking (vanilla plugin, UL rate) and modified choking (802.11s ALM)
	private boolean PREFER_ONEHOP_PEERS; // limit neighborhood scope to 1-hop peers or allow all to upload to
	
	private int NODE_UPLOAD_COUNT; // number of upload slots (default is 4 = 3 normal + 1 optimistic) -> 1 every 10 is always used as optimistic
	
	private boolean ALLOW_OPTIMISTIC;	// if we forbid optimistic peers, NODE_UPLOAD_COUNT denotes the number of
										// non-random peers chosen by the current strategy (rate- or ALM-based)
	
	private boolean ALLOW_UNINTERESTING;	// allow unchoking of peers we are not interested in (false in vanilla by default)
											// only affects Leechers, Seeds always upload to uninteresting peers
	
	private boolean ALLOW_SNUBBED; 	// allow unchoking of 'snubbed' peers (false in vanilla by default)
										// Leechers mark peers as snubbed if they fail to transmit blocks in less then a minute
										// may cause unwanted dropping of connections in mesh setup
	
	private short ALM_UPDATE_IVAL; // query interval to update local ALM info, in seconds
	
	private boolean SNMP_QUERY_ACTIVE; // use SNMP (MeshNodeSW) or local iw call
	private int SNMP_PORT;
	private String MESH_IP; // IP for SNMP query (default localhost)
	
	private String MESH_IFNAME; // if no SNMP query, get ALM info from this IF via iw call
	
	
	/* TODO
	 * 
	 * - piece size (configured within *.torrent file)
	 * 
	 * - choking period (normal / optimistic) -> default is 10s / 30s
	 *   (need to find out how to modify this in Vuze via plugin)
	 * 
	 * - enable/disable optimistic unchoke ?
	 * -> if disabled, NODE_UPLOAD_COUNT must denote number of normal upload slots
	 * 
	 * - hybrid mode -> weigh upload rate with ALM ?
	 * -> then let ALM weighing factor be configurable
	 * 
	 */
	
	
	static private ConfFileParams singleton;
	static private Object synchronizer = new Object();
	
	
	public static ConfFileParams getInstance()
	{
		if (singleton == null) {
			synchronized (synchronizer) {
				if(singleton == null)
				{
					singleton = new ConfFileParams();
				}
			}
		}
		return singleton;
	}
	
	
	private ConfFileParams()
	{	
		// assign parameter default values
		
		VANILLA = true;
		PREFER_ONEHOP_PEERS = false;
		
		NODE_UPLOAD_COUNT = 4;
		
		ALLOW_OPTIMISTIC = true;
		
		ALLOW_UNINTERESTING = false;
		ALLOW_SNUBBED = false;
		
		ALM_UPDATE_IVAL = 8;
		
		SNMP_QUERY_ACTIVE = false;
		SNMP_PORT = 2001;
		
		MESH_IP = "127.0.0.1";
		
		MESH_IFNAME = "wlan0";
		
		parseConfFile();
	}
	
	
	private void parseConfFile()
	{
		Scanner scan;
		try {
			FileReader read = new FileReader("conf.txt");
			scan = new Scanner(read);
			String buffer = new String();
			
			System.out.println("ConfFileParams  -  parseConfFile()");
			System.out.println("conf file found!");
			Date timestamp = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
			System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
			
			while (scan.hasNextLine())
			{
				buffer = scan.nextLine();
				
				if (!buffer.isEmpty())
				{
					// split line at all whitespace delimiters
					String[] array = buffer.split("\\s+");
					
					if (array.length >= 3)
					{
						if (array[1].equals("=") && !array[0].contains("//"))
						{
							if(array[0].equals("VANILLA"))
							{
								if(array[2].contains("false")) {
									VANILLA = false;
								}
								if(array[2].contains("true")) {
									VANILLA = true;
								}
							}
							if(array[0].equals("PREFER_ONEHOP_PEERS"))
							{
								if(array[2].contains("false")) {
									PREFER_ONEHOP_PEERS = false;
								}
								if(array[2].contains("true")) {
									PREFER_ONEHOP_PEERS = true;
								}
							}
							if(array[0].equals("NODE_UPLOAD_COUNT"))
							{
								NODE_UPLOAD_COUNT = Integer.parseInt(array[2]);
							}
							if(array[0].equals("ALLOW_OPTIMISTIC"))
							{
								if(array[2].contains("false")) {
									ALLOW_OPTIMISTIC = false;
								}
								if(array[2].contains("true")) {
									ALLOW_OPTIMISTIC = true;
								}
							}
							if(array[0].equals("ALLOW_UNINTERESTING"))
							{
								if(array[2].contains("false")) {
									ALLOW_UNINTERESTING = false;
								}
								if(array[2].contains("true")) {
									ALLOW_UNINTERESTING = true;
								}
							}
							if(array[0].equals("ALLOW_SNUBBED"))
							{
								if(array[2].contains("false")) {
									ALLOW_SNUBBED = false;
								}
								if(array[2].contains("true")) {
									ALLOW_SNUBBED = true;
								}
							}
							if(array[0].equals("ALM_UPDATE_IVAL"))
							{
								ALM_UPDATE_IVAL = Short.valueOf(array[2]);
							}
							if(array[0].equals("SNMP_QUERY_ACTIVE"))
							{
								if (array[2].contains("true")) {
									SNMP_QUERY_ACTIVE = true;
								}
								if (array[2].contains("false")) {
									SNMP_QUERY_ACTIVE = false;
								}
							}
							if(array[0].equals("SNMP_PORT")) {
								SNMP_PORT = Integer.valueOf(array[2]);
							}
							if(array[0].equals("MESH_IP"))
							{
								MESH_IP = array[2];
							}
							if(array[0].equals("MESH_IFNAME"))
							{
								MESH_IFNAME = array[2];
							}
						}
					}
				}
			}
			scan.close();
			
			System.out.println("ConfFileParams  -  parseConfFile()");
			System.out.println("finished parsing!");
			
			System.out.println("VANILLA: "+getVANILLA());
			System.out.println("PREFER_ONEHOP_PEERS: "+getPREFER_ONEHOP_PEERS());
			System.out.println("NODE_UPLOAD_COUNT: "+getNODE_UPLOAD_COUNT());
			System.out.println("ALLOW_OPTIMISTIC: "+getALLOW_OPTIMISTIC());
			System.out.println("ALLOW_UNINTERESTING: "+getALLOW_UNINTERESTING());
			System.out.println("ALLOW_SNUBBED: "+getALLOW_SNUBBED());
			System.out.println("ALM_UPDATE_IVAL: "+getALM_UPDATE_IVAL());
			System.out.println("SNMP_QUERY_ACTIVE: "+getSNMP_QUERY_ACTIVE());
			System.out.println("SNMP_PORT: "+getSNMP_PORT());
			System.out.println("MESH_IP: "+getMESH_IP());
			System.out.println("MESH_IFNAME: "+getMESH_IFNAME());
			
			
			timestamp = new Date();
			simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
			System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
			
		} 
		catch (FileNotFoundException e)
		{
			System.out.println("ConfFileParams  -  parseConfFile()");
			System.out.println("conf file not found! using default values ...");
			
			System.out.println("VANILLA: "+getVANILLA());
			System.out.println("PREFER_ONEHOP_PEERS: "+getPREFER_ONEHOP_PEERS());
			System.out.println("NODE_UPLOAD_COUNT: "+getNODE_UPLOAD_COUNT());
			System.out.println("ALLOW_OPTIMISTIC: "+getALLOW_OPTIMISTIC());
			System.out.println("ALLOW_UNINTERESTING: "+getALLOW_UNINTERESTING());
			System.out.println("ALLOW_SNUBBED: "+getALLOW_SNUBBED());
			System.out.println("ALM_UPDATE_IVAL: "+getALM_UPDATE_IVAL());
			System.out.println("SNMP_QUERY_ACTIVE: "+getSNMP_QUERY_ACTIVE());
			System.out.println("SNMP_PORT: "+getSNMP_PORT());
			System.out.println("MESH_IP: "+getMESH_IP());
			System.out.println("MESH_IFNAME: "+getMESH_IFNAME());
			
			Date timestamp = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
			System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		}
	}
	
	
	public boolean getVANILLA() { return VANILLA; }
	public boolean getPREFER_ONEHOP_PEERS() { return PREFER_ONEHOP_PEERS; }
	public int getNODE_UPLOAD_COUNT() { return NODE_UPLOAD_COUNT; }
	public boolean getALLOW_OPTIMISTIC() { return ALLOW_OPTIMISTIC; }
	public boolean getALLOW_UNINTERESTING() { return ALLOW_UNINTERESTING; }
	public boolean getALLOW_SNUBBED() { return ALLOW_SNUBBED; }
	public short getALM_UPDATE_IVAL() { return ALM_UPDATE_IVAL; }
	public boolean getSNMP_QUERY_ACTIVE() { return SNMP_QUERY_ACTIVE; }
	public int getSNMP_PORT() { return SNMP_PORT; }
	public String getMESH_IP() { return MESH_IP; }
	public String getMESH_IFNAME() { return MESH_IFNAME; }
	
}
