package mesh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.event.ResponseListener;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TableEvent;
import org.snmp4j.util.TableUtils;

// TODO: mesh modification
// SNMP client to query local status information (ALM)
// to be used in modified choking algorithm

public class SnmpClient {

	private String address;
	private Snmp snmp;
	
	public SnmpClient(String address) {
		super();
		this.address = address;
		try {
			start();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("rawtypes")
	private void start() throws IOException {
		TransportMapping transport = new DefaultUdpTransportMapping();
		snmp = new Snmp(transport);
		transport.listen();
	}
	
	public void stop() throws IOException {
		snmp.close();
	}
	
	
	public String getAsString(OID oid) throws IOException
	{
		ResponseEvent event = get(new OID[]{oid});
		
		String returnValue = new String();
		
		try {
			returnValue = event.getResponse().get(0).getVariable().toString();
		} catch (NullPointerException e) {
			
		}
		
		return returnValue;
	}
	
	public void getAsString(OID oids, ResponseListener listener) {
		try {
			snmp.send(getPDU(new OID[]{oids}), getTarget(), null, listener);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	private PDU getPDU(OID oids[]) {
		PDU pdu = new PDU();
		for (OID oid : oids) {
			pdu.add(new VariableBinding(oid));
		}
		pdu.setType(PDU.GET);
		return pdu;
	}
	
	public ResponseEvent get(OID oids[]) throws IOException {
	   ResponseEvent event = snmp.send(getPDU(oids), getTarget(), null);
	   if(event != null) {
		   return event;
	   }
	   throw new RuntimeException("GET timed out");
	}
	
	
	private PDU setPDU(OID oid, Variable var) {
		PDU pdu = new PDU();
		pdu.add(new VariableBinding(oid,var));
		pdu.setType(PDU.SET);
		return pdu;
	}
	
	private PDU setPDU(OID[] oids, Variable[] vars) {
		if (oids.length == vars.length) {
			PDU pdu = new PDU();
			for(int i=0; i<oids.length; i++) {
				pdu.add(new VariableBinding(oids[i],vars[i]));
			}
			pdu.setType(PDU.SET);
			return pdu;
		}
		else {
			return null;
		}
	}
	
	public ResponseEvent set(OID oid, Variable var) throws IOException
	{
		ResponseEvent event = snmp.send(setPDU(oid,var), getTarget(), null);
		return processSetResponse(event);
	}
	
	public ResponseEvent set(OID[] oids, Variable[] vars) throws IOException
	{
		ResponseEvent event = snmp.send(setPDU(oids,vars), getTarget(), null);
		return processSetResponse(event);
	}
	
	private ResponseEvent processSetResponse(ResponseEvent event)
	{
		if (event != null) {
			System.out.println("\nResponse:\nGot Snmp Set Response from Agent on " + address);
			PDU responsePDU = event.getResponse();
			if (responsePDU != null) {
				int errorStatus = responsePDU.getErrorStatus();
				int errorIndex = responsePDU.getErrorIndex();
				String errorStatusText = responsePDU.getErrorStatusText();
				if (errorStatus == PDU.noError) {
					System.out.println("Snmp Set Response = " + responsePDU.getVariableBindings());
					} else {
						System.out.println("Error: Request Failed");
						System.out.println("Error Status = " + errorStatus);
						System.out.println("Error Index = " + errorIndex);
						System.out.println("Error Status Text = " + errorStatusText);
						throw new RuntimeException("SET error");
					}
				} else {
					System.out.println("Error: Response PDU is null");
					throw new RuntimeException("SET error");
				}
			return event;
		} else {
			throw new RuntimeException("SET timed out");
		}
	}
	
	
	private Target getTarget() {
		Address targetAddress = GenericAddress.parse(address);
		CommunityTarget target = new CommunityTarget();
		target.setCommunity(new OctetString("public"));
		target.setAddress(targetAddress);
		
		// TODO: SNMP retries / response timeout
		// -> [ retries * timeout * # queried OIDs ] <= query interval !
		// -> timeout = query interval / [ # queried OIDs * retries ]
		target.setRetries(0); // TODO: cfg file parameter
		target.setTimeout(3000); // TODO: cfg file parameter
		
		target.setVersion(SnmpConstants.version2c);
		return target;
	}
	
	public List<List<String>> getTableAsStrings(OID[] oids)
	{	
		TableUtils tUtils = new TableUtils(snmp, new DefaultPDUFactory());
		
		List<TableEvent> events = tUtils.getTable(getTarget(), oids, null, null);
		
		List<List<String>> list = new ArrayList<List<String>>();
		
		for (TableEvent event : events)
		{
			if(event.isError()) {
				throw new RuntimeException(event.getErrorMessage());
			}
			List<String> strList = new ArrayList<String>();
			list.add(strList);
			for(VariableBinding vb : event.getColumns()) {
				if (vb != null)
					strList.add(vb.getVariable().toString());
			}
		}
		return list;
	}
	
	public static String extractSingleString(ResponseEvent event) {
		return event.getResponse().get(0).getVariable().toString();
	}
}
	

