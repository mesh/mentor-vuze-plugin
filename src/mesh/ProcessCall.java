package mesh;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class ProcessCall
{
	
	public static synchronized void runCommand(String cmdName, File path, String... parameters)
	{
		Process proc = runCommandStreamsLeftOpen(cmdName, path, parameters);
		closeProcessStreams(proc);
	}
	
	
	public static synchronized Process runCommandStreamsLeftOpen(String cmdName, File path, String... parameters)
	{
		List<String> fullCommand = new ArrayList<String>();

		if (cmdName == null || cmdName.length() == 0)
		{
			throw new IllegalArgumentException("cmdName is empty");
		}
		fullCommand.add(cmdName);

		for (String param : parameters)
		{
			if (param != null && param.length() > 0)
				fullCommand.add(param);
		}
		
		ProcessBuilder procBuilder = new ProcessBuilder(fullCommand);
		
		if (path != null)
			procBuilder.directory(path);
		
		Process proc = null;
		try {
			proc = procBuilder.start();
			proc.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return proc;
	}
	
	
	public static synchronized void runCommandThrowInterruptedException(
			String cmdName, File path, String... parameters) throws InterruptedException
	{
		List<String> fullCommand = new ArrayList<String>();

		if (cmdName == null || cmdName.length() == 0)
		{
			throw new IllegalArgumentException("cmdName is empty");
		}
		fullCommand.add(cmdName);

		for (String param : parameters)
		{
			if (param != null && param.length() > 0)
				fullCommand.add(param);
		}
		
		ProcessBuilder procBuilder = new ProcessBuilder(fullCommand);
		
		if (path != null)
			procBuilder.directory(path);
		
		Process proc = null;
		try {
			proc = procBuilder.start();
			proc.waitFor();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} finally {
			closeProcessStreams(proc);
		}
	}
	
	
	public static synchronized Scanner runCommandWithOutput(String cmdName, File path, String... parameters)
	{
		Process proc = runCommandStreamsLeftOpen(cmdName, path, parameters);
		
		CopyInputStream cis = new CopyInputStream(proc.getInputStream());
		InputStream in = cis.getCopy();
		Scanner scan = new Scanner(in);
		
		closeProcessStreams(proc);
		
		return scan;
	}
	
	
	public static synchronized int runCommandWithReturnValue(String cmdName, File path, String... parameters)
	{
		Process proc = runCommandStreamsLeftOpen(cmdName, path, parameters);
		int exitValue = proc.exitValue();
		closeProcessStreams(proc);
		return exitValue;
	}
	
	
	private static synchronized void closeProcessStreams(Process proc)
	{
		try
		{
			proc.getInputStream().close();
			proc.getOutputStream().close();
			proc.getErrorStream().close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
