package com.ti.bass.ui;

import java.util.*;
import java.util.List;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.*;
import org.gudy.azureus2.core3.download.DownloadManager;
import org.gudy.azureus2.core3.download.DownloadManagerPeerListener;
import org.gudy.azureus2.core3.internat.MessageText;
import org.gudy.azureus2.core3.ipfilter.IpFilterManagerFactory;
import org.gudy.azureus2.core3.peer.PEPeer;
import org.gudy.azureus2.core3.peer.PEPeerManager;
import org.gudy.azureus2.core3.util.Debug;
//import org.gudy.azureus2.plugins.peers.Peer;
import org.gudy.azureus2.plugins.ui.tables.*;
import org.gudy.azureus2.ui.swt.Messages;
import org.gudy.azureus2.ui.swt.views.ViewUtils;
import org.gudy.azureus2.ui.swt.views.table.TableViewSWT;
import org.gudy.azureus2.ui.swt.views.table.TableViewSWTMenuFillListener;
import org.gudy.azureus2.ui.swt.views.table.impl.TableViewSWTImpl;
import org.gudy.azureus2.ui.swt.views.table.impl.TableViewTab;
import org.gudy.azureus2.ui.swt.views.tableitems.peers.*;
import com.aelitis.azureus.ui.common.table.*;
import com.ti.bass.core.BassPlugin;

// heavily based on Vuze PeersView
public class BassPeersView extends TableViewTab<PEPeer> implements
	DownloadManagerPeerListener, TableDataSourceChangedListener,
	TableLifeCycleListener, TableViewSWTMenuFillListener, TableRefreshListener {

	private TableColumnCore[] getBasicColumnItems(String table_id) {
		return new TableColumnCore[] { new IpItem(table_id),
			new ClientItem(table_id), new TypeItem(table_id),
			new MessagingItem(table_id), new EncryptionItem(table_id),
			new PiecesItem(table_id), new PercentItem(table_id),
			new DownSpeedItem(table_id), new UpSpeedItem(table_id),
			new PeerSourceItem(table_id), new HostNameItem(table_id),
			new PortItem(table_id), new InterestedItem(table_id),
			new AltoCostItem(table_id, plugin_, this),
			new ConnectionTypeItem(table_id), 
			new ChokedItem(table_id), new DownItem(table_id),
			new InterestingItem(table_id), new ChokingItem(table_id),
			new OptimisticUnchokeItem(table_id), new UpItem(table_id),
			new UpDownRatioItem(table_id), new GainItem(table_id),
			new StatUpItem(table_id), new SnubbedItem(table_id),
			new TotalDownSpeedItem(table_id),
			new TimeUntilCompleteItem(table_id), new DiscardedItem(table_id),
			new UniquePieceItem(table_id), new TimeToSendPieceItem(table_id),
			new DLedFromOthersItem(table_id), new UpRatioItem(table_id),
			new StateItem(table_id), new ConnectedTimeItem(table_id),
			new PieceItem(table_id), new IncomingRequestCountItem(table_id),
			new OutgoingRequestCountItem(table_id),
			new UpSpeedLimitItem(table_id), new DownSpeedLimitItem(table_id),
			new LANItem(table_id), new PeerIDItem(table_id),
			new PeerByteIDItem(table_id),
			new HandshakeReservedBytesItem(table_id),
			new ClientIdentificationItem(table_id), new ASItem(table_id), };
	}

	private final TableColumnCore[]	basicItems_;
	private BassPlugin				plugin_;
	private List<DownloadManager>	managers_;
	private int						selectedManagerIndex_;
	private TableViewSWT<PEPeer>	tv_;
	private Shell					shell_;
	private Combo					downloadsCombo_;
	private boolean					downloadManagersModified_	= false;
//	private boolean					downloadManagerRemoved_	= false;

	/**
	 * Initialize
	 * 
	 */
	public BassPeersView(BassPlugin plugin) {
		super("BassPeersView");
		plugin_ = plugin;
		selectedManagerIndex_ = -1;
		managers_ = new LinkedList<DownloadManager>();
		basicItems_ = getBasicColumnItems(TableManager.TABLE_TORRENT_PEERS);
	}

	@Override
	public TableViewSWT<PEPeer> initYourTableView() {
		tv_ = new TableViewSWTImpl<PEPeer>(PEPeer.class,
			TableManager.TABLE_TORRENT_PEERS, "BassPeersView", basicItems_,
			"altocost", SWT.MULTI | SWT.FULL_SELECTION | SWT.VIRTUAL);
		// setTableView(tv_);
		tv_.setRowDefaultHeight(16);
		tv_.setEnableTabViews(true);
		tv_.addRefreshListener(this, false);
		tv_.addTableDataSourceChangedListener(this, false);
		tv_.addLifeCycleListener(this);
		tv_.addMenuFillListener(this);
		return tv_;
	}

	public void tableDataSourceChanged(Object newDataSources) {
		DownloadManager[] array;
		List<DownloadManager> newDownloadManagers;
		String selectedDownloadManager;
		
		array = (DownloadManager[]) newDataSources;
		newDownloadManagers = new ArrayList<DownloadManager>(array.length);
		Collections.addAll(newDownloadManagers, array);
		synchronized (managers_) {
			if (newDownloadManagers.size() != managers_.size() ||
				!managers_.containsAll(newDownloadManagers)) {
				// downloadManager list has been modified
				if (selectedManagerIndex_ >= 0) {
					selectedDownloadManager = managers_
						.get(selectedManagerIndex_).getDisplayName();
					selectedManagerIndex_ = -1;
					for (int i = 0; i < newDownloadManagers.size(); i++) {
						if (newDownloadManagers.get(i).getDisplayName()
							.equals(selectedDownloadManager)) {
							selectedManagerIndex_ = i;
							break;
						}
					}
				}
				managers_ = newDownloadManagers;
				downloadManagersModified_ = true;
			}
//		
//		
//		
//		
//		
//		DownloadManager newDownloadManager;
//		newDownloadManager = (DownloadManager) newDataSource;
//		synchronized (managers_) {
//			if (!managers_.contains(newDownloadManager)) {
//				managers_.add(newDownloadManager);
//				downloadManagerAdded_ = true;
//			} else {
//				managers_.remove(newDownloadManager);
//				downloadManagerRemoved_ = true;
//			}
		}
	}

	private String[] getDownloadsComboItems() {
		String[] result;
		synchronized (managers_) {
			result = new String[managers_.size()];
			for (int i = 0; i < result.length; i++)
				result[i] = managers_.get(i).getDisplayName();
		}
		return result;
	}

	public DownloadManager getDownloadManager() {
		synchronized (managers_) {
			if (managers_.isEmpty() || selectedManagerIndex_ < 0)
				return null;
			return managers_.get(selectedManagerIndex_);
		}
	}

	// @see
	// com.aelitis.azureus.ui.common.table.TableLifeCycleListener#tableViewInitialized()
	public void tableViewInitialized() {
		shell_ = tv_.getComposite().getShell();
		downloadsCombo_ = new Combo(tv_.getComposite(), SWT.READ_ONLY);
		downloadsCombo_.setItems(getDownloadsComboItems());
		downloadsCombo_.setLayoutData(new GridData(GridData.FILL,
			GridData.CENTER, true, false));
		downloadsCombo_.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				newManagerSelected();
			}
		});
	}

	public void tableViewDestroyed() {
		DownloadManager manager;
		manager = getDownloadManager();
		if (manager != null)
			manager.removePeerListener(this);
	}

	public void fillMenu(String sColumnName, Menu menu) {
		fillMenu(menu, tv_, shell_, true);
	}

	@SuppressWarnings("rawtypes")
	public static void fillMenu(final Menu menu, final TableView tv,
								final Shell shell, boolean download_specific) {
		Object[] peers = tv.getSelectedDataSources().toArray();
		boolean hasSelection = (peers.length > 0);
		boolean downSpeedDisabled = false;
		boolean downSpeedUnlimited = false;
		long totalDownSpeed = 0;
		long downSpeedSetMax = 0;
		long maxDown = 0;
		boolean upSpeedDisabled = false;
		boolean upSpeedUnlimited = false;
		long totalUpSpeed = 0;
		long upSpeedSetMax = 0;
		long maxUp = 0;
		if (hasSelection) {
			for (int i = 0; i < peers.length; i++) {
				PEPeer peer = (PEPeer) peers[i];
				try {
					int maxul = peer.getStats()
						.getUploadRateLimitBytesPerSecond();
					maxUp += maxul * 4;
					if (maxul == 0) {
						upSpeedUnlimited = true;
					} else {
						if (maxul > upSpeedSetMax) {
							upSpeedSetMax = maxul;
						}
					}
					if (maxul == -1) {
						maxul = 0;
						upSpeedDisabled = true;
					}
					totalUpSpeed += maxul;
					int maxdl = peer.getStats()
						.getDownloadRateLimitBytesPerSecond();
					maxDown += maxdl * 4;
					if (maxdl == 0) {
						downSpeedUnlimited = true;
					} else {
						if (maxdl > downSpeedSetMax) {
							downSpeedSetMax = maxdl;
						}
					}
					if (maxdl == -1) {
						maxdl = 0;
						downSpeedDisabled = true;
					}
					totalDownSpeed += maxdl;
				} catch (Exception ex) {
					Debug.printStackTrace(ex);
				}
			}
		}
		if (download_specific) {
			final MenuItem block_item = new MenuItem(menu, SWT.CHECK);
			PEPeer peer = (PEPeer) tv.getFirstSelectedDataSource();
			if (peer == null
				|| peer.getManager().getDiskManager()
					.getRemainingExcludingDND() > 0) {
				// disallow peer upload blocking when downloading
				block_item.setSelection(false);
				block_item.setEnabled(false);
			} else {
				block_item.setEnabled(true);
				block_item.setSelection(peer.isSnubbed());
			}
			Messages.setLanguageText(block_item, "PeersView.menu.blockupload");
			block_item.addListener(SWT.Selection,
				new TableSelectedRowsListener(tv) {

					public void run(TableRowCore row) {
						((PEPeer) row.getDataSource(true))
							.setSnubbed(block_item.getSelection());
					}
				});
		}
		final MenuItem ban_item = new MenuItem(menu, SWT.PUSH);
		Messages.setLanguageText(ban_item, "PeersView.menu.kickandban");
		ban_item.addListener(SWT.Selection, new TableSelectedRowsListener(tv) {

			public void run(TableRowCore row) {
				PEPeer peer = (PEPeer) row.getDataSource(true);
				String msg = MessageText
					.getString("PeersView.menu.kickandban.reason");
				IpFilterManagerFactory.getSingleton().getIPFilter().ban(
					peer.getIp(), msg, true);
				peer.getManager().removePeer(peer);
			}
		});
		// === advanced menu ===
		final MenuItem itemAdvanced = new MenuItem(menu, SWT.CASCADE);
		Messages.setLanguageText(itemAdvanced,
			"MyTorrentsView.menu.advancedmenu"); //$NON-NLS-1$
		itemAdvanced.setEnabled(hasSelection);
		final Menu menuAdvanced = new Menu(shell, SWT.DROP_DOWN);
		itemAdvanced.setMenu(menuAdvanced);
		// advanced > Download Speed Menu //
		ViewUtils.addSpeedMenu(shell, menuAdvanced, false, hasSelection,
			downSpeedDisabled, downSpeedUnlimited, totalDownSpeed,
			downSpeedSetMax, maxDown, upSpeedDisabled, upSpeedUnlimited,
			totalUpSpeed, upSpeedSetMax, maxUp, peers.length,
			new ViewUtils.SpeedAdapter() {

				public void setDownSpeed(int speed) {
					setSelectedPeersDownSpeed(speed, tv);
				}

				public void setUpSpeed(int speed) {
					setSelectedPeersUpSpeed(speed, tv);
				}
			});
		new MenuItem(menu, SWT.SEPARATOR);
	}

	public void addThisColumnSubMenu(String columnName, Menu menuThisColumn) {
	}

	@SuppressWarnings("rawtypes")
	private static void setSelectedPeersUpSpeed(int speed, TableView tv) {
		Object[] peers = tv.getSelectedDataSources().toArray();
		if (peers.length > 0) {
			for (int i = 0; i < peers.length; i++) {
				try {
					PEPeer peer = (PEPeer) peers[i];
					peer.getStats().setUploadRateLimitBytesPerSecond(speed);
				} catch (Exception e) {
					Debug.printStackTrace(e);
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private static void setSelectedPeersDownSpeed(int speed, TableView tv) {
		Object[] peers = tv.getSelectedDataSources().toArray();
		if (peers.length > 0) {
			for (int i = 0; i < peers.length; i++) {
				try {
					PEPeer peer = (PEPeer) peers[i];
					peer.getStats().setDownloadRateLimitBytesPerSecond(speed);
				} catch (Exception e) {
					Debug.printStackTrace(e);
				}
			}
		}
	}

	/* DownloadManagerPeerListener implementation */
	public void peerAdded(PEPeer created) {
		tv_.addDataSource(created);
	}

	public void peerRemoved(PEPeer removed) {
		tv_.removeDataSource(removed);
	}

	public void peerManagerWillBeAdded(PEPeerManager peer_manager) {
	}

	public void peerManagerAdded(PEPeerManager manager) {
	}

	public void peerManagerRemoved(PEPeerManager manager) {
		tv_.removeAllTableRows();
	}

	/**
	 * Add datasources already in existance before we called addListener. Faster
	 * than allowing addListener to call us one datasource at a time.
	 */
	private void addExistingDatasources() {
		DownloadManager manager;
		PEPeer[] dataSources;
		tv_.removeAllTableRows();
		manager = getDownloadManager();
		if (manager == null || tv_.isDisposed()) {
			return;
		}
		dataSources = manager.getCurrentPeers();
		if (dataSources == null || dataSources.length == 0) {
			return;
		}
		tv_.addDataSources(dataSources);
		tv_.processDataSourceQueue();
	}

	public void tableRefresh() {
		boolean selectionForced = false;
		synchronized (managers_) {
			if (downloadManagersModified_) {
//				selectedManagerIndex_ = downloadsCombo_.getSelectionIndex();
				downloadsCombo_.setItems(getDownloadsComboItems());
				if (selectedManagerIndex_ < 0 && !managers_.isEmpty()) {
					selectedManagerIndex_ = 0;
					selectionForced = true;
				}
				downloadsCombo_.select(selectedManagerIndex_);
				downloadManagersModified_ = false;
				if (selectionForced)
					newManagerSelected();
			}
//			if (downloadManagerRemoved_) {
//				selectedManagerIndex_ = downloadsCombo_.getSelectionIndex();
//				downloadsCombo_.setItems(getDownloadsComboItems());
//				if (selectedManagerIndex_ < 0 && !managers_.isEmpty()) {
//					selectedManagerIndex_ = 0;
//					selectionForced = true;
//				}
//				downloadsCombo_.select(selectedManagerIndex_);
//				downloadManagerRemoved_ = false;
//			}
		}
	}

	private void newManagerSelected() {
		DownloadManager oldManager, newManager;
		synchronized (managers_) {
			oldManager = getDownloadManager();
			selectedManagerIndex_ = downloadsCombo_.getSelectionIndex();
			newManager = getDownloadManager();
			if (newManager != oldManager) {
				if (oldManager != null)
					oldManager.removePeerListener(this);
				if (newManager != null)
					newManager.addPeerListener(this);
			}
			addExistingDatasources();
		}
	}
}
