package com.ti.bass.ui;


import java.util.*;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.gudy.azureus2.core3.download.DownloadManager;
import org.gudy.azureus2.core3.peer.PEPeer;
import org.gudy.azureus2.core3.peer.impl.PEPeerTransport;
import org.gudy.azureus2.ui.swt.components.graphics.PieUtils;
import org.gudy.azureus2.ui.swt.mainwindow.Colors;
import org.gudy.azureus2.ui.swt.views.AbstractIView;

import com.aelitis.azureus.core.peermanager.peerdb.PeerItem;
import com.ti.bass.core.BassPlugin;
import com.ti.bass.core.exception.BassException;
import com.ti.bass.core.peerselection.PeerSelectionService.PeerCost;

// Heavily based on Vuze PeersGraphicView class
public class BassPeersGraphicView extends AbstractIView /*implements
	DownloadManagerPeerListener*/ {

	private class PEPeerCost {
		public final PEPeer pepeer_;
		public final int cost_;
		
		public PEPeerCost(PEPeer pepeer, int cost) {
			pepeer_ = pepeer;
			cost_ = cost;
		}
	}
	
	private final static int 		UNKNOWN_COST = Integer.MAX_VALUE;
	private List<DownloadManager>	managers_;
	private int						selectedManagerIndex_;
	private String					lastSelectedManager_;
	private static final int		NB_ANGLES				= 1000;
	private double[]				angles_;
	// private double[] deltaPerimeters_;
	private double					perimeter_;
	private double[]				rs_;
	private double[]				deltaXXs_;
	private double[]				deltaXYs_;
	private double[]				deltaYXs_;
	private double[]				deltaYYs_;
	private Point					oldSize_;
	private BassPlugin				plugin_;
	// UI Stuff
	private Display					display_;
	private Composite				panel_;
	private static final int		PEER_SIZE				= 15;
	// private static final int PACKET_SIZE = 10;
	private static final int		OWN_SIZE				= 75;
	private boolean					antiAliasingAvailable_	= true;
	private Composite				tabPanel_;
	private Combo					downloadsCombo_;
	private boolean					downloadManagersModified_	= false;
//	private boolean					downloadManagerRemoved	= false;
	
	public BassPeersGraphicView(BassPlugin plugin) {
		plugin_ = plugin;
		selectedManagerIndex_ = -1;
		managers_ = new LinkedList<DownloadManager>();
		angles_ = new double[NB_ANGLES];
//		deltaPerimeters_ = new double[NB_ANGLES];
		rs_ = new double[NB_ANGLES];
		deltaXXs_ = new double[NB_ANGLES];
		deltaXYs_ = new double[NB_ANGLES];
		deltaYXs_ = new double[NB_ANGLES];
		deltaYYs_ = new double[NB_ANGLES];
		for (int i = 0; i < NB_ANGLES; i++) {
			angles_[i] = 2 * i * Math.PI / NB_ANGLES - Math.PI;
			deltaXXs_[i] = Math.cos(angles_[i]);
			deltaXYs_[i] = Math.sin(angles_[i]);
			deltaYXs_[i] = Math.cos(angles_[i] + Math.PI / 2);
			deltaYYs_[i] = Math.sin(angles_[i] + Math.PI / 2);
		}
	}

	public void dataSourceChanged(Object newDataSources) {
		DownloadManager[] array;
		List<DownloadManager> newDownloadManagers;
//		String selectedDownloadManager;
		
		array = (DownloadManager[]) newDataSources;
		newDownloadManagers = new ArrayList<DownloadManager>(array.length);
		Collections.addAll(newDownloadManagers, array);
		synchronized (managers_) {
			if (newDownloadManagers.size() != managers_.size() ||
				!managers_.containsAll(newDownloadManagers)) {
				// downloadManager list has been modified
				selectedManagerIndex_ = -1;
				if (lastSelectedManager_ != null) {
					// preserve last selection
					for (int i = 0; i < newDownloadManagers.size(); i++) {
						if (newDownloadManagers.get(i).getDisplayName()
							.equals(lastSelectedManager_)) {
							selectedManagerIndex_ = i;
							break;
						}
					}
				}
//				
//				
//				if (selectedManagerIndex_ >= 0) {
//					selectedDownloadManager = managers_
//						.get(selectedManagerIndex_).getDisplayName();
//					selectedManagerIndex_ = -1;
//					for (int i = 0; i < newDownloadManagers.size(); i++) {
////						manager = 
//						if (newDownloadManagers.get(i).getDisplayName()
//							.equals(selectedDownloadManager)) {
//							selectedManagerIndex_ = i;
//							break;
//						}
//					}
//				}

				managers_ = newDownloadManagers;
				downloadManagersModified_ = true;
			}
//			
//			
//			if (!managers_.contains(newDownloadManager)) {
//				managers_.add(newDownloadManager);
//				downloadManagersChanged = true;
//			} else {
//				managers_.remove(newDownloadManager);
//				downloadManagerRemoved = true;
//			}
		}
	}

	private String[] getDownloadsComboItems() {
		String[] result;
		
		synchronized (managers_) {
			result = new String[managers_.size()];
			for (int i = 0; i < result.length; i++)
				result[i] = managers_.get(i).getDisplayName();
		}
		return result;
	}
	
	public DownloadManager getDownloadManager() {
		synchronized (managers_) {
			if (managers_.isEmpty() || selectedManagerIndex_ < 0)
				return null;
			return managers_.get(selectedManagerIndex_);
		}
	}
	
	public Composite getComposite() {
		return tabPanel_;
	}
	
	public String getData() {
		return "BassPeersGraphicView.title";
	}
	
	public void initialize(Composite parentPanel) {
		GridData gridData;
		GridLayout gridLayout;
		
		tabPanel_ = new Composite(parentPanel, SWT.NULL);
		gridLayout = new GridLayout(1, true);
		tabPanel_.setLayout(gridLayout);

		panel_ = new Canvas(tabPanel_, SWT.NULL);
		gridData = new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1);
		panel_.setLayoutData(gridData);		

		downloadsCombo_ = new Combo(tabPanel_, SWT.READ_ONLY);
		downloadsCombo_.setItems(getDownloadsComboItems());
		if (downloadsCombo_.getItemCount() > 0) {
			downloadsCombo_.select(0);
			lastSelectedManager_ = downloadsCombo_.getItem(0);
		}
		downloadsCombo_.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		downloadsCombo_.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
		        synchronized (managers_) {
		        	selectedManagerIndex_ = downloadsCombo_.getSelectionIndex();	
				}
			}
		});
	}

	public void refresh() {
		doRefresh();
	}

	@SuppressWarnings("rawtypes")
	private void doRefresh() {
		SortedSet<PeerCost> sortedPeers;
		Map<PeerItem, PEPeerTransport> pepeersByPeerItem;
		List<PEPeerCost> sortedPepeers;
		DownloadManager manager;
		List<PEPeer> peers;
		Iterator iter;
		
		synchronized (managers_) {
			if (downloadManagersModified_) {
//				selectedManagerIndex_ = downloadsCombo_.getSelectionIndex();
				downloadsCombo_.setItems(getDownloadsComboItems());
				if (selectedManagerIndex_ < 0 && !managers_.isEmpty())
					selectedManagerIndex_ = 0;
				downloadsCombo_.select(selectedManagerIndex_);
				lastSelectedManager_ = downloadsCombo_.getItem(selectedManagerIndex_);
				downloadManagersModified_ = false;
			}
//			if (downloadManagerRemoved) {
//				selectedManagerIndex_ = downloadsCombo_.getSelectionIndex();
//				downloadsCombo_.setItems(getDownloadsComboItems());
//				if (selectedManagerIndex_ < 0 && !managers_.isEmpty())
//					selectedManagerIndex_ = 0;
//				downloadsCombo_.select(selectedManagerIndex_);
//				downloadManagerRemoved = false;
//			}
		}
		
		manager = getDownloadManager();
		
		pepeersByPeerItem = new HashMap<PeerItem, PEPeerTransport>();
		peers = manager == null || manager.getPeerManager() == null ? 
			new ArrayList<PEPeer>(0) : manager.getPeerManager().getPeers();
		iter = peers.iterator();
		while (iter.hasNext()) {
			PEPeerTransport pepeer = (PEPeerTransport) iter.next();
			if (pepeer.getConnectionState() == PEPeerTransport.CONNECTION_FULLY_ESTABLISHED)
				pepeersByPeerItem.put(pepeer.getPeerItemIdentity(), pepeer);
		}
		if (pepeersByPeerItem.isEmpty())
			sortedPepeers = new ArrayList<PEPeerCost>(0);
		else {
			try {
				sortedPeers = plugin_.getPeerSelectionService()
					.getPeerCosts(pepeersByPeerItem.keySet());
				sortedPepeers = new ArrayList<PEPeerCost>(sortedPeers
					.size());
				for (PeerCost peerCost : sortedPeers)
					sortedPepeers.add(new PEPeerCost(pepeersByPeerItem
						.get(peerCost.peerItem_), peerCost.cost_));
			} catch (BassException e) {
				// unable to sort
				sortedPepeers = new ArrayList<PEPeerCost>(pepeersByPeerItem.size());
				for (PeerItem peerItem : pepeersByPeerItem.keySet())
					sortedPepeers.add(new PEPeerCost(pepeersByPeerItem
						.get(peerItem), UNKNOWN_COST));
			}
		}
		render(sortedPepeers);
	}

	private void render(List<PEPeerCost> sortedPepeers) {
		DownloadManager manager;
		Point panelSize;
		int x0, y0, x1, y1, a, b, nbPeers, iAngle, minCost, maxCost, cost;
		Image buffer;
		GC gcBuffer, gcPanel;
		double currentPerimeter, r, r1, ratio;
		Iterator<PEPeerCost> iterator;
		PEPeerCost pepeerCost;
		PEPeer pepeer;
		int[] rectangle;
		RGB rgb;
		
		manager = getDownloadManager();
		if (panel_ == null || panel_.isDisposed() || manager == null)
			return;
		panelSize = panel_.getSize();
		x0 = panelSize.x / 2;
		y0 = panelSize.y / 2;
//		a = x0 - 20;
		a = x0 - 30;
//		b = y0 - 20;
		b = y0 - 50;
		
		if (a < 10 || b < 10)
			return;
		if (oldSize_ == null || !oldSize_.equals(panelSize)) {
			oldSize_ = panelSize;
			perimeter_ = 0;
			for (int i = 0; i < NB_ANGLES; i++) {
				rs_[i] = Math
					.sqrt(1 / (deltaYXs_[i] * deltaYXs_[i] / (a * a) + deltaYYs_[i]
						* deltaYYs_[i] / (b * b)));
				perimeter_ += rs_[i];
			}
		}
		buffer = new Image(display_, panelSize.x, panelSize.y);
		gcBuffer = new GC(buffer);
		gcBuffer.setBackground(Colors.white);
		gcBuffer.setForeground(Colors.blue);
		gcBuffer.fillRectangle(0, 0, panelSize.x, panelSize.y);
		if (SWT.getVersion() >= 3138 && antiAliasingAvailable_) {
			try {
				// gcBuffer.setTextAntialias(SWT.ON);
				// gcBuffer.setAntialias(SWT.ON);
			} catch (Exception e) {
				antiAliasingAvailable_ = false;
			}
		}
		gcBuffer.setBackground(Colors.blues[Colors.BLUES_MIDLIGHT]);
		nbPeers = sortedPepeers.size();
		iAngle = 0;

		minCost = 0;
		maxCost = 0;
		if (nbPeers > 0) {
			minCost = sortedPepeers.get(nbPeers - 1).cost_;
			maxCost = sortedPepeers.get(0).cost_;
		}
		
		iterator = sortedPepeers.iterator();
		currentPerimeter = 0;
		for (int i = 0; i < nbPeers; i++) {
			pepeerCost = iterator.next();
			pepeer = pepeerCost.pepeer_;
			cost = pepeerCost.cost_;
			do {
//				angle = angles_[iAngle];
				r = rs_[iAngle];
				currentPerimeter += r;
				if (iAngle + 1 < NB_ANGLES)
					iAngle++;
			} while (currentPerimeter < i * perimeter_ / nbPeers);
//			angle = (4 * i - nbPeers) * Math.PI / (2 * nbPeers) - Math.PI NB_ANGLES/ 2;
			rectangle = new int[8];
			if (!pepeer.isChokedByMe() || !pepeer.isChokingMe()) {
				gcBuffer.setForeground(Colors.blues[Colors.BLUES_MIDLIGHT]);
				x1 = x0 + (int) (r * deltaYXs_[iAngle]);
				y1 = y0 + (int) (r * deltaYYs_[iAngle]);
				gcBuffer.drawLine(x0, y0, x1, y1);
				/*
				 * rectangle[0] = x0 + (int) (deltaXXs[iAngle] * 3 + 0.5);
				 * rectangle[1] = y0 + (int) (deltaXYs[iAngle] * 3 + 0.5);
				 * rectangle[2] = x0 - (int) (deltaXXs[iAngle] * 3 + 0.5);
				 * rectangle[3] = y0 - (int) (deltaXYs[iAngle] * 3 + 0.5);
				 * 
				 * 
				 * rectangle[4] = x0 - (int) (deltaXXs[iAngle] * 3 - r *
				 * deltaYXs[iAngle]+ 0.5); rectangle[5] = y0 - (int)
				 * (deltaXYs[iAngle] * 3 - r * deltaYYs[iAngle] + 0.5);
				 * rectangle[6] = x0 + (int) (deltaXXs[iAngle] * 3 + r *
				 * deltaYXs[iAngle] + 0.5); rectangle[7] = y0 + (int)
				 * (deltaXYs[iAngle] * 3 + r * deltaYYs[iAngle] + 0.5);
				 * gcBuffer.drawPolygon(rectangle);
				 */
			}
			int percentSent = pepeer.getPercentDoneOfCurrentIncomingRequest();
			if (percentSent >= 0) {
				gcBuffer.setBackground(Colors.blues[Colors.BLUES_MIDDARK]);
				r1 = r - r * percentSent / 100;
				rectangle[0] = (int) (x0 + r1 * deltaYXs_[iAngle] + 0.5);
				rectangle[1] = (int) (y0 + r1 * deltaYYs_[iAngle] + 0.5);
				rectangle[2] = (int) (x0 + deltaXXs_[iAngle] * 4 + r1
					* deltaYXs_[iAngle] + 0.5);
				rectangle[3] = (int) (y0 + deltaXYs_[iAngle] * 4 + r1
					* deltaYYs_[iAngle] + 0.5);
				rectangle[4] = (int) (x0 + deltaXXs_[iAngle] * 4 + (r1 - 10)
					* deltaYXs_[iAngle] + 0.5);
				rectangle[5] = (int) (y0 + deltaXYs_[iAngle] * 4 + (r1 - 10)
					* deltaYYs_[iAngle] + 0.5);
				rectangle[6] = (int) (x0 + (r1 - 10) * deltaYXs_[iAngle] + 0.5);
				rectangle[7] = (int) (y0 + (r1 - 10) * deltaYYs_[iAngle] + 0.5);
				gcBuffer.fillPolygon(rectangle);
			}
			percentSent = pepeer.getPercentDoneOfCurrentOutgoingRequest();
			if (percentSent >= 0) {
				gcBuffer.setBackground(Colors.blues[Colors.BLUES_MIDLIGHT]);
				r1 = r * percentSent / 100;
				rectangle[0] = (int) (x0 + r1 * deltaYXs_[iAngle] + 0.5);
				rectangle[1] = (int) (y0 + r1 * deltaYYs_[iAngle] + 0.5);
				rectangle[2] = (int) (x0 - deltaXXs_[iAngle] * 4 + r1
					* deltaYXs_[iAngle] + 0.5);
				rectangle[3] = (int) (y0 - deltaXYs_[iAngle] * 4 + r1
					* deltaYYs_[iAngle] + 0.5);
				rectangle[4] = (int) (x0 - deltaXXs_[iAngle] * 4 + (r1 - 10)
					* deltaYXs_[iAngle] + 0.5);
				rectangle[5] = (int) (y0 - deltaXYs_[iAngle] * 4 + (r1 - 10)
					* deltaYYs_[iAngle] + 0.5);
				rectangle[6] = (int) (x0 + (r1 - 10) * deltaYXs_[iAngle] + 0.5);
				rectangle[7] = (int) (y0 + (r1 - 10) * deltaYYs_[iAngle] + 0.5);
				gcBuffer.fillPolygon(rectangle);
			}
			x1 = x0 + (int) (r * deltaYXs_[iAngle]);
			y1 = y0 + (int) (r * deltaYYs_[iAngle]);
			// gcBuffer.setBackground(Colors.blues[Colors.BLUES_MIDDARK]);
			rgb = Colors.blues[Colors.BLUES_MIDDARK].getRGB();
			ratio = minCost == maxCost ? (double) 0.5 : 
				(double) ((double) (cost - minCost) / (double) (maxCost - minCost));
			rgb.red = (int) (255 * ratio);
//			rgb.blue = (int) (rgb.blue * (1 - ratio));
			gcBuffer.setBackground(new org.eclipse.swt.graphics.Color(display_,
				rgb));
			if (pepeer.isSnubbed()) {
				gcBuffer.setBackground(Colors.grey);
			}
			/*
			 * int PS = (int) (PEER_SIZE); if (deltaXY == 0) { PS = (int)
			 * (PEER_SIZE * 2); } else { if (deltaYY > 0) { PS = (int)
			 * (PEER_SIZE / deltaXY); } }
			 */
			// PieUtils.drawPie(gcBuffer,(x1 - PS / 2),y1 - PS /
			// 2,PS,PS,peer.getPercentDoneInThousandNotation() / 10);
			PieUtils.drawPie(gcBuffer, x1 - PEER_SIZE / 2, y1 - PEER_SIZE / 2,
				PEER_SIZE, PEER_SIZE,
				pepeer.getPercentDoneInThousandNotation() / 10);
			gcBuffer.drawText("" + (cost == UNKNOWN_COST ? "n.a." : cost) , 
				x1 - (PEER_SIZE / 2) + (int) (40 * deltaYXs_[iAngle]), y1 - (PEER_SIZE / 2) 
				+ (int) (20 * deltaYYs_[iAngle]), true);
			
		}
		gcBuffer.setBackground(Colors.blues[Colors.BLUES_MIDDARK]);
		PieUtils.drawPie(gcBuffer, x0 - OWN_SIZE / 2, y0 - OWN_SIZE / 2,
			OWN_SIZE, OWN_SIZE, manager.getStats().getCompleted() / 10);
					
		gcBuffer.dispose();
		gcPanel = new GC(panel_);
		gcPanel.drawImage(buffer, 0, 0);
		gcPanel.dispose();
		buffer.dispose();
		
	}
}
