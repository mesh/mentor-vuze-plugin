/*
 * Created on 13-Oct-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package com.ti.bass.ui;

import java.util.*;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.gudy.azureus2.ui.swt.Messages;
import org.gudy.azureus2.ui.swt.plugins.*;
import org.gudy.azureus2.ui.swt.views.*;

import com.aelitis.azureus.ui.UIFunctions;
import com.aelitis.azureus.ui.UIFunctionsManager;
import com.ti.bass.core.BassPlugin;


/**
 * Main user interface of Bass plugin that handles all events triggered by the
 * user and permits the management of the peer selection strategy.
 */
public class BassUI implements UISWTViewEventListener {

	private UISWTView			view_;
	private Label				funLabel_;
	private BassPlugin			plugin_;
	private UISWTInstance		swtInstance_;
	private TabFolder			tabFolder_;
	private Map<String, IView>	tabMap_;
	private final static String IVIEW_DATA_TOKEN = "iview";

	
	public BassUI(BassPlugin plugin, UISWTInstance swtInstance) {
		tabMap_ = new LinkedHashMap<String, IView>();
		plugin_ = plugin;
		swtInstance_ = swtInstance;
	}

	public TabFolder getTabFolder() {
		return tabFolder_;
	}

	public UISWTInstance getSwtInstance() {
		return swtInstance_;
	}

	public BassPlugin getPlugin() {
		return plugin_;
	}

	public boolean eventOccurred(UISWTViewEvent event) {
		switch (event.getType()) {
		case UISWTViewEvent.TYPE_CREATE:
			if (view_ != null)
				return false;
			view_ = event.getView();
			break;
		case UISWTViewEvent.TYPE_INITIALIZE:
			Composite panel = (Composite) event.getData();
			initialize(panel);
			break;
		case UISWTViewEvent.TYPE_REFRESH:
			refreshSelectedTab();
			break;
		case UISWTViewEvent.TYPE_DESTROY:
			view_ = null;
			break;
		/*
		 * case UISWTViewEvent.TYPE_DATASOURCE_CHANGED:
		 * //System.out.println("TYPE_DATASOURCES_CHANGED Called"); break; case
		 * UISWTViewEvent.TYPE_FOCUSGAINED:
		 * //System.out.println("TYPE_FOCUSGAINED Called"); break; case
		 * UISWTViewEvent.TYPE_FOCUSLOST: //System.out.println("TYPE_FOCUSLOST
		 * Called"); break; case UISWTViewEvent.TYPE_LANGUAGEUPDATE:
		 * //System.out.println("TYPE_LANGUAGEUPDATE Called " // +
		 * Locale.getDefault().toString()); break;
		 */
		}
		return true;
	}

	private void initialize(Composite parent) {
		tabFolder_ = new TabFolder(parent, SWT.NONE);
		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.horizontalSpan = 2;
		tabFolder_.setLayoutData(gridData);
		Composite bottomSection = new Composite(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		gridLayout.marginHeight = 2;
		gridLayout.marginWidth = 0;
		bottomSection.setLayout(gridLayout);
		gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = 2;
		bottomSection.setLayoutData(gridData);
		funLabel_ = new Label(bottomSection, SWT.LEFT);
		funLabel_.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		Button btnConfig = new Button(bottomSection, SWT.PUSH);
		Messages.setLanguageText(btnConfig, "Bass.main_view.PreferencesButton");
		btnConfig.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				UIFunctions uiFunctions = UIFunctionsManager.getUIFunctions();
				if (uiFunctions != null) {
					uiFunctions.openView(UIFunctions.VIEW_CONFIG, "TI ORACLE");
				}
			}
		});
		Button buttonClose = new Button(bottomSection, SWT.CLOSE);
		Messages.setLanguageText(buttonClose, "Bass.main_view.CloseButton");
		buttonClose.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				view_.closeView();
			}
		});
	
		for (IView bassTab: getTabs()) {
			bassTab.initialize(tabFolder_);
//			if (bassTab instanceof PeersView) 
//				((PeersView)bassTab).tableViewInitialized();
		}
		
		addTabsToView();
		
		tabFolder_.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				refreshSelectedTab();
			}
		});
	}

	public IView attachTab(IView bassTab) {
		synchronized (tabMap_) {
			return tabMap_.put(bassTab.getFullTitle(), bassTab);
		}
	}

	public IView getTab(String tabFullTitle) {
		synchronized (tabMap_) {
			return tabMap_.get(tabFullTitle);
		}
	}

	public IView removeTab(String tabFullTitle) {
		synchronized (tabMap_) {
			return tabMap_.remove(tabFullTitle);
		}
	}
	
	public Collection<IView> getTabs() {
		synchronized (tabMap_) {
			return new ArrayList<IView>(tabMap_.values());
		}
	}

	private void addTabsToView() {
		Collection<IView> c = getTabs();
		Iterator<IView> iterator = c.iterator();
		while (iterator.hasNext()) {
			IView tabView = iterator.next();
			TabItem tab = new TabItem(tabFolder_, SWT.NONE);
			tab.setText(tabView.getShortTitle());
			tab.setControl(tabView.getComposite());
			tab.setData(IVIEW_DATA_TOKEN, tabView);
		}
	}

	private void refreshSelectedTab() {
		((IView)tabFolder_.getSelection()[0].getData(IVIEW_DATA_TOKEN)).refresh();
	}
}
