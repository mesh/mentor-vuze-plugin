package com.ti.bass.ui;

import org.gudy.azureus2.core3.peer.impl.PEPeerTransport;
import org.gudy.azureus2.plugins.ui.tables.*;
import org.gudy.azureus2.ui.swt.views.table.utils.CoreTableColumn;

public class ConnectionTypeItem extends CoreTableColumn 
	implements TableCellRefreshListener {

	public static final String COLUMN_ID = "connectiontype";

	public void fillTableColumnInfo(TableColumnInfo info) {
		info.addCategories(new String[] {
			CAT_CONTENT,
		});
	}	

	/** Default Constructor */
	public ConnectionTypeItem(String table_id) {
		super(COLUMN_ID, POSITION_LAST, 100, table_id);
		setRefreshInterval(INTERVAL_LIVE);
	}
	
	public void refresh(TableCell cell) {
	    PEPeerTransport pepeer;

	    pepeer = (PEPeerTransport)cell.getDataSource();
		pepeer.isIncoming();
		cell.setText(pepeer.isIncoming() ? "Incoming" : "Outgoing");
	}
}
