/*
 * Created on 12-Oct-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package com.ti.bass.ui;

import java.io.File;
import java.util.*;
import java.util.List;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.gudy.azureus2.core3.logging.LogEvent;
import org.gudy.azureus2.plugins.logging.LoggerChannelListener;
import org.gudy.azureus2.ui.swt.Messages;
import org.gudy.azureus2.ui.swt.views.AbstractIView;
import com.ti.bass.core.BassPlugin;

public class BassMainTab extends AbstractIView {

	public static final String		TAB_TITLE		= "Main";
	private StyledText				textArea_;
	private BassPlugin				plugin_;
	private BassUI					bassUI_;
	private boolean					bPaused_		= false;
	private boolean					bAutoScroll_	= true;
	private Display					display_;
	private Composite				tabPanel_;
	private LoggerChannelListener	lcl_;
	private List<logItem>			logMap_;


	public BassMainTab(BassUI bassUI, BassPlugin plugin) {
		bassUI_ = bassUI;
		plugin_ = plugin;
		logMap_ = new ArrayList<logItem>();
		lcl_ = new LoggerChannelListener() {

			public void messageLogged(int type, String content) {
				synchronized (BassMainTab.this) {
					if (logMap_.size() > 30)
						logMap_.clear();
					logMap_.add(new logItem(type, content));

				}
			}

			public void messageLogged(String str, Throwable error) {
				synchronized (BassMainTab.this) {
					if (logMap_.size() > 30)
						logMap_.clear();
					logMap_.add(new logItem(LogEvent.LT_ERROR, str));
					logMap_.add(new logItem(LogEvent.LT_ERROR, error
						.getLocalizedMessage()));
				}
			}
		};
		plugin_.getLogger().addListener(lcl_);
	}

	@Override
	public String getFullTitle() {
		return TAB_TITLE;
	}

	@Override
	public Composite getComposite() {
		return tabPanel_;
	}
	
	@Override
	public void initialize(Composite parentPanel) {
		GridData gridData;
		GridLayout gridLayout;
		
		display_ = parentPanel.getDisplay();
		tabPanel_ = new Composite(parentPanel, SWT.NULL);
		
		gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		tabPanel_.setLayout(gridLayout);
		gridData = new GridData(GridData.FILL_BOTH);
		tabPanel_.setLayoutData(gridData);
		Composite topSection = new Composite(tabPanel_, SWT.NONE);
		gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		topSection.setLayout(gridLayout);
		gridData = new GridData(GridData.FILL_HORIZONTAL);
		topSection.setLayoutData(gridData);
		final Image image = bassUI_.getSwtInstance().loadImage(
			"com" + File.separatorChar + "ti" + File.separatorChar + "bass"
				+ File.separatorChar + "ui" + File.separatorChar + "img"
				+ File.separatorChar + "bass_logo_h70.jpg");
		/*
		 * final Label lbl = new Label(topSection, SWT.NONE); lbl.setText("\n
		 * Benvenuto nel plugin Bass \n"); lbl.setLayoutData(new
		 * GridData(GridData.FILL_HORIZONTAL));
		 */
		final Label logo = new Label(topSection, SWT.LEFT);
		logo.setImage(image);
		//logo.setBackgroundImage(image);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_END);
		gridData.heightHint = 70;
		gridData.widthHint = 192;
		logo.setLayoutData(gridData);
		textArea_ = new StyledText(tabPanel_, SWT.READ_ONLY | SWT.V_SCROLL
			| SWT.H_SCROLL | SWT.BORDER);
		gridData = new GridData(GridData.FILL_BOTH);
		gridData.horizontalSpan = 2;
		textArea_.setLayoutData(gridData);
		Composite cLeft = new Composite(tabPanel_, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		layout.verticalSpacing = 10;
		cLeft.setLayout(layout);
		GridData gd = new GridData(SWT.TOP, SWT.LEAD, false, false);
		cLeft.setLayoutData(gd);
		Group gLogIDs = new Group(cLeft, SWT.NULL);
		Messages.setLanguageText(gLogIDs, "Bass.main_view.LoggingLabel");
		GridLayout layout2 = new GridLayout();
		layout2.marginHeight = 2;
		layout2.numColumns = 3;
		gLogIDs.setLayout(layout2);
		GridData gd2 = new GridData();
		gLogIDs.setLayoutData(gd2);
		Button buttonPause = new Button(gLogIDs, SWT.CHECK);
		Messages.setLanguageText(buttonPause, "Bass.main_view.PauseButton");
		gd = new GridData();
		buttonPause.setLayoutData(gd);
		buttonPause.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				if (e.widget == null || !(e.widget instanceof Button))
					return;
				Button btn = (Button) e.widget;
				bPaused_ = btn.getSelection();
			}
		});
		Button buttonAutoScroll = new Button(gLogIDs, SWT.CHECK);
		gd = new GridData();
		Messages.setLanguageText(buttonAutoScroll,
			"Bass.main_view.AutoScrollButton");
		buttonAutoScroll.setText("Autoscroll");
		buttonAutoScroll.setLayoutData(gd);
		buttonAutoScroll.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				if (e.widget == null || !(e.widget instanceof Button))
					return;
				Button btn = (Button) e.widget;
				bAutoScroll_ = btn.getSelection();
			}
		});
		buttonAutoScroll.setSelection(true);
		Button button = new Button(gLogIDs, SWT.PUSH);
		Messages.setLanguageText(button, "Bass.main_view.CleanButton");
		button.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				textArea_.setText("");
			}
		});
		Group gSlider = new Group(cLeft, SWT.NULL);
		Messages.setLanguageText(gSlider, "Bass.main_view.ImpactLabel");
		GridLayout sliderLayout_ = new GridLayout();
		sliderLayout_.marginHeight = 1;
		sliderLayout_.numColumns = 2;
		gSlider.setLayout(sliderLayout_);
		GridData gd1_ = new GridData(GridData.FILL_HORIZONTAL);
		gd1_.widthHint = 280;
		gSlider.setLayoutData(gd1_);
		final Slider slider = new Slider(gSlider, SWT.HORIZONTAL);
		gridData = new GridData();
		gridData.widthHint = 210;
		slider.setLayoutData(gridData);
		final Text text = new Text(gSlider, SWT.BORDER);
		gridData = new GridData();
		gridData.widthHint = 40;
		text.setLayoutData(gridData);
		text.setText(plugin_.getPeerSelectionWeightOnConnection() + "%");
		slider
			.setSelection(plugin_.getPeerSelectionWeightOnConnection() * 9 / 10);
		text.setEditable(false);
		final Label label = new Label(gSlider, SWT.NONE);
		gridData = new GridData();
		gridData.widthHint = 210;
		label.setLayoutData(gridData);
		Messages.setLanguageText(label, "Bass.main_view.handshakePhaseLabel");
		new Label(gSlider, SWT.NONE);
		slider.addListener(SWT.Selection, new Listener() {

			public void handleEvent(Event event) {
				/*
				 * switch (event.detail) { case SWT.ARROW_DOWN: outString =
				 * "Event: SWT.ARROW_DOWN"; break; case SWT.ARROW_UP: outString =
				 * "Event: SWT.ARROW_UP"; break; case SWT.DRAG: outString =
				 * "Event: SWT.DRAG"; break; case SWT.END: outString = "Event:
				 * SWT.END"; break; case SWT.HOME: outString = "Event:
				 * SWT.HOME"; break; case SWT.PAGE_DOWN: outString = "Event:
				 * SWT.PAGE_DOWN"; break; case SWT.PAGE_UP: outString = "Event:
				 * SWT.PAGE_UP"; break; }
				 */
				int out = (slider.getSelection() * 10 / 9);
				text.setText(out + "%");
				plugin_.setPeerSelectionWeightOnConnection(out);
			}
		});
		final Slider slider1 = new Slider(gSlider, SWT.HORIZONTAL);
		gridData = new GridData();
		gridData.widthHint = 210;
		slider1.setLayoutData(gridData);
		final Text text1 = new Text(gSlider, SWT.BORDER);
		gridData = new GridData();
		gridData.widthHint = 40;
		text1.setLayoutData(gridData);
		text1.setText(plugin_.getPeerSelectionWeightOnUnchoke() + "%");
		slider1
			.setSelection(plugin_.getPeerSelectionWeightOnUnchoke() * 9 / 10);
		final Label label1 = new Label(gSlider, SWT.NONE);
		gridData = new GridData();
		gridData.widthHint = 210;
		label1.setLayoutData(gridData);
		Messages.setLanguageText(label1, "Bass.main_view.unchokingPhaseLabel");
		new Label(gSlider, SWT.NONE);
		slider1.addListener(SWT.Selection, new Listener() {

			public void handleEvent(Event event) {
				int out = (slider1.getSelection() * 10 / 9);
				text1.setText(out + "%");
				plugin_.setPeerSelectionWeightOnUnchoke(out);
			}
		});
	}

	/**
	 * Refresh the view of the main tab
	 */
	public void refresh() {
		int nbLinesBefore;
		
		nbLinesBefore = textArea_.getLineCount();
		if (!bPaused_) {
			synchronized (this) {
				updateLoggingArea();
			}
		}
		if (bAutoScroll_)
			textArea_.setTopIndex(textArea_.getLineCount());
		if (nbLinesBefore > 400) {
			textArea_.replaceTextRange(0, textArea_.getOffsetAtLine(30), "");
			nbLinesBefore = textArea_.getLineCount();
		}
	}

	/**
	 * Update the logging area
	 */
	private void updateLoggingArea() {
		ListIterator<logItem> iterator = logMap_.listIterator();
		while (iterator.hasNext()) {
			logItem me = iterator.next();
			String key = me.getMessage();
			int value = me.getType();
			textArea_.append("\n" + key);
			if (value == BassPlugin.LOG_CONNECTION)
				textArea_.setLineBackground(textArea_.getLineCount() - 1, 1,
					display_.getSystemColor(SWT.COLOR_GRAY));
			else if (value == BassPlugin.LOG_OPTIMISTIC_UNCHOKE)
				textArea_.setLineBackground(textArea_.getLineCount() - 1, 1,
					display_.getSystemColor(SWT.COLOR_YELLOW));
			else if (value == BassPlugin.LOG_SERVER)
				textArea_.setLineBackground(textArea_.getLineCount() - 1, 1,
					display_.getSystemColor(SWT.COLOR_CYAN));
			else
				textArea_.setLineBackground(textArea_.getLineCount() - 1, 1,
					display_.getSystemColor(SWT.COLOR_WHITE));
		}
		logMap_.clear();
	}

	private class logItem {

		private int		type_;
		private String	message_;

		@SuppressWarnings("unused")
		public logItem(String message) {
			this(0, message);
		}

		public logItem(int type, String message) {
			type_ = type;
			message_ = message;
		}

		@SuppressWarnings("unused")
		public void setType_(int type) {
			type_ = type;
		}

		public int getType() {
			return type_;
		}

		@SuppressWarnings("unused")
		public void setMessage(String message) {
			message_ = message;
		}

		public String getMessage() {
			return message_;
		}
	}
}
