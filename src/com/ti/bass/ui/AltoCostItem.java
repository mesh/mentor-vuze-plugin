package com.ti.bass.ui;

import java.util.*;

import org.gudy.azureus2.core3.download.DownloadManager;
import org.gudy.azureus2.core3.peer.PEPeer;
import org.gudy.azureus2.core3.peer.impl.PEPeerTransport;
import org.gudy.azureus2.plugins.ui.tables.*;
import org.gudy.azureus2.ui.swt.views.table.utils.CoreTableColumn;

import com.aelitis.azureus.core.peermanager.peerdb.PeerItem;
import com.ti.bass.core.BassPlugin;
import com.ti.bass.core.exception.BassException;
import com.ti.bass.core.peerselection.PeerSelectionService;
import com.ti.bass.core.peerselection.PeerSelectionService.PeerCost;


public class AltoCostItem extends CoreTableColumn 
	implements TableCellRefreshListener {

	private static final long COSTS_UPDATE_PERIOD = 5000L; // millisecs
	private Map<PeerItem, Integer> peerCosts_;
	private BassPlugin plugin_;
	private BassPeersView peersView_;
	private long lastUpdateTime = 0;
	private DownloadManager lastDownloadManager_;
	
	public static final String COLUMN_ID = "altocost";

	public void fillTableColumnInfo(TableColumnInfo info) {
		info.addCategories(new String[] {
			CAT_BYTES,
		});
	}	

	/** Default Constructor */
	public AltoCostItem(String table_id, BassPlugin plugin, BassPeersView peersView) {
		super(COLUMN_ID, POSITION_LAST, 100, table_id);
		setRefreshInterval(INTERVAL_LIVE);
		plugin_ = plugin;
		peersView_ = peersView;
		peerCosts_ = new HashMap<PeerItem, Integer>();
	}
	
	public void refresh(TableCell cell) {
		Integer cost;
		DownloadManager downloadManager;
	    PEPeerTransport pepeer;
	    long now;
		
		downloadManager = peersView_.getDownloadManager();
		if (downloadManager == null) {
			lastDownloadManager_ = null;
			return;
		}
		now = System.currentTimeMillis();
		if (lastDownloadManager_ != downloadManager || 
			now > lastUpdateTime + COSTS_UPDATE_PERIOD) {
			lastDownloadManager_ = downloadManager;
			lastUpdateTime = now;
			updatePeerCosts(downloadManager);
		}
		pepeer = (PEPeerTransport)cell.getDataSource();
		cost = peerCosts_.get(pepeer.getPeerItemIdentity());
		if (cost == null)
			return;
		cell.setSortValue(cost);
		cell.setText(cost.toString());
	}
	
	private void updatePeerCosts(DownloadManager downloadManager) {
		PeerSelectionService peerSelectionService;
		Collection<PeerItem> peers;

		peerCosts_.clear();
		peerSelectionService = plugin_.getPeerSelectionService();
		peers = new LinkedList<PeerItem>();
		for (PEPeer pepeer: downloadManager.getPeerManager().getPeers())
			peers.add(((PEPeerTransport)pepeer).getPeerItemIdentity());
		try {
			for (PeerCost peerCost: peerSelectionService.getPeerCosts(peers)) 
				peerCosts_.put(peerCost.peerItem_, peerCost.cost_);
		} catch (BassException e) {
			// do nothing
		}
	}
}
