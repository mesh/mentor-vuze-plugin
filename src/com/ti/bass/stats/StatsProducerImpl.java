/*
 * Created on 21-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.stats;

import java.io.FileWriter;
import org.gudy.azureus2.core3.util.SystemTime;
import org.gudy.azureus2.plugins.PluginInterface;
import org.gudy.azureus2.plugins.PluginListener;
import org.gudy.azureus2.plugins.download.*;
import org.gudy.azureus2.plugins.peers.*;

import com.ti.bass.core.BassPlugin;
import com.ti.bass.core.exception.BassException;
import com.ti.bass.stats.BassPeerStats.PeerInfo;
import com.ti.bass.utils.Ip4Address;
import org.gudy.azureus2.plugins.peers.PeerManagerListener2;

/**
 * Implementation of Bass StatsPruducer: it updates and save statistics and 
 * triggers different StatsEvents 
 */
public class StatsProducerImpl implements StatsProducer, PeerManagerListener2  {

	private BassPlugin		plugin_;
	private PeerManager		peerManager_;
	private BassPeerStats	bassPeerStats_;
	private StatsGuardian	statsGuardian_;
	private boolean			shutdown_;
	private long timestamp_ = SystemTime.getCurrentTime();

	public StatsProducerImpl(PeerManager pm, BassPlugin plugin) {
		peerManager_ = pm;
		plugin_ = plugin;
		shutdown_ = false;
		bassPeerStats_ = new BassPeerStats();
		statsGuardian_ = new StatsGuardian(this);
		pm.addListener(this);
	}

	public void eventOccurred(PeerManagerEvent event) {
		boolean peerAdded;
		boolean updated;
		
		switch(event.getType()) {
		case PeerManagerEvent.ET_PEER_ADDED:
			try {
				new Ip4Address(event.getPeer().getIp());
				peerAdded = bassPeerStats_
					.addPeerStats(event.getPeer().getIp());
				updated = updateStats(event.getPeer());
				if (peerAdded)
					statsGuardian_.fireStatsEvent(StatsEvent.PEER_STATS_ADDED,
						bassPeerStats_);
				else if (updated)
					statsGuardian_.fireStatsEvent(StatsEvent.STATS_UPDATED,
						bassPeerStats_);
			} catch (BassException e) {
				// Do nothing
			}
			break;
		case PeerManagerEvent.ET_PEER_REMOVED:
			updated = updateStats(event.getPeer());
			if (updated)
				statsGuardian_.fireStatsEvent(StatsEvent.STATS_UPDATED,
					bassPeerStats_);
			break;
		case PeerManagerEvent.ET_PEER_DISCOVERED:
			try {
				new Ip4Address(event.getPeerDescriptor().getIP());
				peerAdded = bassPeerStats_.addPeerStats(event
					.getPeerDescriptor().getIP());
				if (peerAdded)
					statsGuardian_.fireStatsEvent(StatsEvent.PEER_STATS_ADDED,
						bassPeerStats_);
			} catch (BassException e) {
				// Do nothing
			}
			break;
		}
	}
	
	/**
	 * Main function for the calculations of stats metrics
	 * 
	 */
	public synchronized void updateStats() {
		Peer[] peers;
		Download download;
		PluginInterface pi;
		boolean updated;
		
		if (peerManager_ != null) {
			try {
				download = peerManager_.getDownload();
				if (download == null)
					return;
				download.addListener(new DownloadListener() {

					public void stateChanged(final Download download,
												int old_state, int new_state) {
						if (new_state == Download.ST_SEEDING) {
							statsGuardian_.fireStatsEvent(
								StatsEvent.SEEDING, bassPeerStats_);
							download.removeListener(this);
						}
					}

					public void positionChanged(Download download,
												int oldPosition, int newPosition) {
					}
				});
				
				pi = plugin_.getPluginInterface();
				pi.addListener(new PluginListener() {

					public void closedownInitiated() {
					}

					public void closedownComplete() {
						if (!shutdown_) {
							statsGuardian_.fireStatsEvent(
								StatsEvent.SHUTDOWN, bassPeerStats_);
							shutdown_ = true;
						}
					}

					public void initializationComplete() {
					}
				});
			} catch (DownloadException e) {
				plugin_.getLogger().log("Failed to handle PeerManager: " + e);
			}
			peers = peerManager_.getPeers();
			updated = false;
			for (int i = 0; i < peers.length; i++) {
				if (updateStats(peers[i]))
					updated = true;
			}
			if (updated) 
				statsGuardian_.fireStatsEvent(StatsEvent.STATS_UPDATED, bassPeerStats_);
		}
		return;
	}

	/**
	 * Main function for stats update. It returns true if stats updated, false
	 * otherwise.
	 * 
	 * @param peer
	 */
	private boolean updateStats(Peer p) {
		final Peer peer = p;
		final PeerInfo pi;
		PeerStats ps;
		long bytesReceived;
		long bytesSent;
		long timeElapsed;
		long totalConnectionTime;
		long totalConnectionTimeInDownload;
		long totalConnectionTimeInUpload;
		long instantDownRate;
		long instantUpRate;
		long avgDownRate;
		long avgUpRate;

		
		if (!bassPeerStats_.statsArePresent(peer))
			return false;
		pi = bassPeerStats_.getPeerStats(peer);
		ps = peer.getStats();
		if (pi.getIsResetted()) {
			pi.setBytesRecvInLastReset(ps.getTotalReceived());
			pi.setBytesSentInLastReset(ps.getTotalSent());
			pi.setConnTimeFromLastUpdate(ps
				.getTimeSinceConnectionEstablished());
			pi.setIsResetted(false);
		}
		// Downlink stats
		bytesReceived = ps.getTotalReceived() - pi.getDataReceived()
			- pi.getBytesRecvInLastReset();
		
		if (bytesReceived < 0) bytesReceived = 0;
				
		if (bytesReceived < 0) {
			try {
				FileWriter out_ = new FileWriter("/home/bri/Scrivania/log",
					true);
				out_.write("bytesReceived<0\n");
				out_.write("ps.getTotalReceived() = " + ps.getTotalReceived()
					+ "\n");
				out_.write("pi.getDataReceived() = " + pi.getDataReceived()
					+ "\n");
				out_.write("pi.getBytesRecvInLastReset() = "
					+ pi.getBytesRecvInLastReset() + "\n");
				out_.flush();
				out_.close();
			} catch (Exception ex) {// Catch exception if any
				System.err.println("Error: " + ex.getMessage());
			}
		}
								
		pi.setDataReceived(pi.getDataReceived() + bytesReceived);
		
		
		
		if (ps.getTimeSinceConnectionEstablished() > pi
			.getConnTimeFromLastUpdate())
			timeElapsed = ps.getTimeSinceConnectionEstablished()
				- pi.getConnTimeFromLastUpdate();
		else
			timeElapsed = ps.getTimeSinceConnectionEstablished();
		totalConnectionTime = pi.getTotalConnectionPeriod() + timeElapsed;
		pi.setTotalConnectionPeriod(totalConnectionTime);
		pi.setConnTimeFromLastUpdate(ps.getTimeSinceConnectionEstablished());
		if (pi.getTotalConnectionPeriod() != 0)
			avgDownRate = (pi.getDataReceived() * 1000)
				/ pi.getTotalConnectionPeriod();
		else
			avgDownRate = 0;
		pi.setAvgDownloadRate(avgDownRate);
		pi.setSampleBytesDown(pi.getSampleBytesDown() + bytesReceived);
		pi.setSampleTime(pi.getSampleTime() + timeElapsed);
		if (pi.getSampleTime() > 3000) {
			instantDownRate = (pi.getSampleBytesDown() * 1000)
				/ pi.getSampleTime();
			pi.setInstDownloadRate(instantDownRate);
			if (instantDownRate > pi.getPeakDownloadRate())
				pi.setPeakDownloadRate(instantDownRate);
			pi.setSampleBytesDown(0);
			
		}
		
		if (pi.getInstDownloadRate()>0) {
			
			totalConnectionTimeInDownload = pi.getTotalConnectionPeriodInDownload() + timeElapsed;
			pi.setTotalConnectionPeriodInDownload(totalConnectionTimeInDownload);
			
		} 
		
		// Uplink stats
		bytesSent = ps.getTotalSent() - pi.getDataSent()
			- pi.getBytesSentInLastReset();
		
		if (bytesSent <0) bytesSent = 0;
		
		pi.setDataSent(pi.getDataSent() + bytesSent);
		
		
					
		if (pi.getTotalConnectionPeriod() != 0)
			avgUpRate = (pi.getDataSent() * 1000)
				/ pi.getTotalConnectionPeriod();
		else
			avgUpRate = 0;
		pi.setAvgUploadRate(avgUpRate);
		pi.setSampleBytesUp(pi.getSampleBytesUp() + bytesSent);
		if (pi.getSampleTime() > 3000) {
			instantUpRate = (pi.getSampleBytesUp() * 1000)
				/ pi.getSampleTime();
			pi.setInstUploadRate(instantUpRate);
			
			pi.setSampleBytesUp(0);
			pi.setSampleTime(0);
		}
		
		if (pi.getInstUploadRate()>0) {
			
			totalConnectionTimeInUpload = pi.getTotalConnectionPeriodInUpload() + timeElapsed;
			pi.setTotalConnectionPeriodInUpload(totalConnectionTimeInUpload);
			
		} 
		
		
		PeerListener2 listener = new PeerListener2() {

			public void eventOccurred(PeerEvent event) {
				if (event.getType() == PeerEvent.ET_STATE_CHANGED) {
					if (((Integer) event.getData()).intValue() == Peer.TRANSFERING) {
						pi.setFirstConnectionSuccessful(SystemTime
							.getCurrentTime()
							- timestamp_);
						peer.removeListener(this);
					}
					if (((Integer) event.getData()).intValue() == Peer.CONNECTING
						&& pi.getFirstConnectionAttempt() == 0) {
						pi.setFirstConnectionAttempt(SystemTime
							.getCurrentTime()
							- timestamp_);
					}
					// to avoid the case of first succesful connection with
					// no connection attempt
					if (pi.getFirstConnectionAttempt() == 0)
						pi.setFirstConnectionAttempt(pi.getTimeOfInfoCreation());
				}
			}
		};
		if (pi.getFirstConnectionAttempt() == 0
			&& pi.getFirstConnectionSuccessful() == 0)
			peer.addListener(listener);
		
		if (p.getConnection() != null && pi.getFirstConnectionAttempt() != 0 )
			pi.setIsIncoming(p.getConnection().isIncoming());
		
		
		
		
		return true;
	}

	/**
	 * Add a listener to StatsGuardian
	 * 
	 * @param sl
	 */
	public void addStatsListener(StatsListener sl) {
		statsGuardian_.addListener(sl);
	}

	/**
	 * Remove a listener from StatsGuardian
	 * 
	 * @param sl
	 */
	public void removeStatsListener(StatsListener sl) {
		statsGuardian_.removeListener(sl);
	}

	/**
	 * Fires a StatsEvent
	 * 
	 * @param type
	 *            Type of event
	 * @param tips
	 *            Stats of the peer
	 */
	public void fireStatsEvent(int type, BassPeerStats tips) {
		statsGuardian_.fireStatsEvent(type, tips);
	}

	/**
	 * Fires a StatsEvent
	 * 
	 * @param type
	 *            Type of event
	 */
	public void fireStatsEvent(int type) {
		statsGuardian_.fireStatsEvent(type, bassPeerStats_);
	}
}
