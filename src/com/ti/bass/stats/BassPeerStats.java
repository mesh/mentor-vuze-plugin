/*
 * Created on 21-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.stats;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import org.gudy.azureus2.core3.util.SystemTime;
import org.gudy.azureus2.plugins.peers.Peer;

public class BassPeerStats {
	
	private long timestamp_ = SystemTime.getCurrentTime();

//	public class PeerKey {
//
//		private final String ip_;
//	
////		public Peer getPeer() {
////			return peer_;
////		}
//
//		public PeerKey(String ip) {
//			ip_ = peer.getIp();
//		}
//
//		@Override
//		public int hashCode() {
//			// TEMP
//			return ip_.hashCode();
//			/*
//			 * + peer_.getTCPListenPort() + peer_.getUDPListenPort()
//			 */
//		}
//
//		@Override
//		public boolean equals(Object obj) {
//			if (!(obj instanceof PeerKey))
//				return false;
//			PeerKey other = (PeerKey) obj;
//			return ip_.equals(other.ip_);
//			/*
//			 * && peer_.getTCPListenPort() == other.getTCPListenPort() &&
//			 * peer_.getUDPListenPort() == other.getUDPListenPort()
//			 */
//		}
//	}

	public class PeerInfo {

//		private Peer	peer_;
		private long	avgDownloadRate_;
		private long	instDownloadRate_;
		private long	instUploadRate_;
		private long	peakDownloadRate_;
		private long	dataReceived_;
		private long	avgUploadRate_;
		private long	dataSent_;
		private long	totalConnectionPeriod_;
		private long	totalConnectionPeriodInDownload_;
		private long	totalConnectionPeriodInUpload_;
		private long	bytesInLastUpdate_;
		private long	connTimeFromLastUpdate_;
		private long	sampleBytesDown_;
		private long	sampleBytesUp_;
		private long	sampleTime_;
		private boolean	isResetted_;
		private long	bytesRecvInLastReset_;
		private long	bytesSentInLastReset_;
		private long	timeOfInfoCreation_;
		private long	firstConnectionAttempt_;
		private long	firstConnectionSuccessful_;
		private boolean isIncoming_;
		private String ip_;

		public PeerInfo(String ip) {
//			peer_ = peer;
			ip_ = ip;
//			avgDownloadRate_ = 0;
//			instDownloadRate_ = 0;
//			peakDownloadRate_ = 0;
//			instUploadRate_ = 0;
//			dataReceived_ = 0;
//			avgUploadRate_ = 0;
//			dataSent_ = 0;
//			totalConnectionPeriod_ = 0;
//			totalConnectionPeriodInDownload_ = 0;
//			totalConnectionPeriodInUpload_ = 0;
//			bytesInLastUpdate_ = 0;
//			connTimeFromLastUpdate_ = 0;
//			sampleBytesDown_ = 0;
//			sampleBytesUp_ = 0;
//			sampleTime_ = 0;
			timeOfInfoCreation_ = SystemTime.getCurrentTime() - timestamp_;
			isResetted_ = true;
		}
		
//		public Peer getPeer() {
//			return peer_;
//		}

		public String getIp() {
			return ip_;
		}

		public void setIp(String ip) {
			ip_ = ip;
		}
		
		public long getAvgDownloadRate() {
			return avgDownloadRate_;
		}

		public void setAvgDownloadRate(long avgDownloadRate) {
			avgDownloadRate_ = avgDownloadRate;
		}


		public long getInstDownloadRate() {
			return instDownloadRate_;
		}

		public void setInstDownloadRate(long instDownloadRate) {
			instDownloadRate_ = instDownloadRate;
		}
		
		public void setInstUploadRate(long instUpRate) {
			this.instUploadRate_ = instUpRate;
		}
				
		public long getInstUploadRate() {
			return instUploadRate_;
		}

		public long getPeakDownloadRate() {
			return peakDownloadRate_;
		}

		public void setPeakDownloadRate(long peakDownloadRate) {
			peakDownloadRate_ = peakDownloadRate;
		}

		public long getDataReceived() {
			return dataReceived_;
		}

		public void setDataReceived(long dataReceived) {
			dataReceived_ = dataReceived;
		}


		public long getAvgUploadRate() {
			return avgUploadRate_;
		}

		public void setAvgUploadRate(long avgUploadRate) {
			avgUploadRate_ = avgUploadRate;
		}

		public long getDataSent() {
			return dataSent_;
		}

		public void setDataSent(long dataSent) {
			dataSent_ = dataSent;
		}

		public long getTotalConnectionPeriod() {
			return totalConnectionPeriod_;
		}

		public void setTotalConnectionPeriod(long totalConnectionPeriod) {
			totalConnectionPeriod_ = totalConnectionPeriod;
		}
		
		public long getTotalConnectionPeriodInDownload() {
			return totalConnectionPeriodInDownload_;
		}
				
		public long getTotalConnectionPeriodInUpload() {
			return totalConnectionPeriodInUpload_;
		}
		
		public void setTotalConnectionPeriodInDownload(
														long totalConnectionPeriodInDownload) {
			this.totalConnectionPeriodInDownload_ = totalConnectionPeriodInDownload;
		}
				
		public void setTotalConnectionPeriodInUpload(
														long totalConnectionPeriodInUpload) {
			this.totalConnectionPeriodInUpload_ = totalConnectionPeriodInUpload;
		}
		

		public long getBytesInLastUpdate() {
			return bytesInLastUpdate_;
		}

		public void setBytesInLastUpdate(long bytesInLastUpdate) {
			bytesInLastUpdate_ = bytesInLastUpdate;
		}

		public long getConnTimeFromLastUpdate() {
			return connTimeFromLastUpdate_;
		}

		public void setConnTimeFromLastUpdate(long connTimeFromLastUpdate) {
			connTimeFromLastUpdate_ = connTimeFromLastUpdate;
		}

		public long getSampleBytesDown() {
			return sampleBytesDown_;
		}

		public void setSampleBytesDown(long sampleBytes) {
			sampleBytesDown_ = sampleBytes;
		}
		
		
		public long getSampleBytesUp() {
			return sampleBytesUp_;
		}

		public void setSampleBytesUp(long sampleBytesUp) {
			this.sampleBytesUp_ = sampleBytesUp;
		}

		public long getSampleTime() {
			return sampleTime_;
		}

		public void setSampleTime(long sampleTime) {
			sampleTime_ = sampleTime;
		}

		public boolean getIsResetted() {
			return isResetted_;
		}

		public void setIsResetted(boolean isResetted) {
			isResetted_ = isResetted;
		}

		public long getBytesRecvInLastReset() {
			return bytesRecvInLastReset_;
		}

		public void setBytesRecvInLastReset(long bytesRecvInLastReset) {
			bytesRecvInLastReset_ = bytesRecvInLastReset;
		}

		public long getBytesSentInLastReset() {
			return bytesSentInLastReset_;
		}

		public void setBytesSentInLastReset(long bytesSentInLastReset) {
			bytesSentInLastReset_ = bytesSentInLastReset;
		}
				
		public long getTimeOfInfoCreation() {
			return timeOfInfoCreation_;
		}
				
		public void setFirstConnectionAttempt(long firstConnectionAttempt) {
			firstConnectionAttempt_ = firstConnectionAttempt;
		}
			
		public long getFirstConnectionAttempt() {
			return firstConnectionAttempt_;
		}
				
		public void setFirstConnectionSuccessful(long firstConnectionSuccessful) {
			firstConnectionSuccessful_ = firstConnectionSuccessful;
		}

		public long getFirstConnectionSuccessful() {
			return firstConnectionSuccessful_;
		}
				
		public void setIsIncoming(boolean isIncoming) {
			isIncoming_ = isIncoming;
		}
				
		public boolean getIsIncoming() {
			return isIncoming_;
		}
		
	}

	private HashMap<String, PeerInfo>	statsMap_;

	public BassPeerStats() {
		statsMap_ = new HashMap<String, PeerInfo>();
	}

	public synchronized Collection<PeerInfo> getPeerStats() {
		return new LinkedList<PeerInfo>(statsMap_.values());
	}

	public synchronized PeerInfo getPeerStats(Peer peer) {
		return statsMap_.get(peer.getIp());
	}

	public synchronized boolean addPeerStats(String ip) {
		if (statsMap_.containsKey(ip))
			return false;
		statsMap_.put(ip, new PeerInfo(ip));
		return true;
	}

	public synchronized PeerInfo removePeer(Peer peer) {
		return statsMap_.remove(peer.getIp());
	}

	public synchronized int size() {
		return statsMap_.size();
	}

	public synchronized boolean statsArePresent(Peer peer) {
		return statsMap_.containsKey(peer.getIp());
	}
}
