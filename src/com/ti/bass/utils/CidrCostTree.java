/*
 * Created on 9-Nov-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package com.ti.bass.utils;

import java.io.*;
import java.util.*;

import com.ti.bass.core.exception.BassException;

public class CidrCostTree {

	private class CidrNode {

		private Ip4Cidr		cidr_;
		private CidrNode	left_, right_;	/* left=0 right=1 */
		private int			cost_;

		public CidrNode() {
			//empty
		}

		public CidrNode(Ip4Cidr cidr, int cost) {
			cidr_ = cidr;
			cost_ = cost;
		}
	}

	public class CidrCost {

		public final Ip4Cidr	cidr_;
		public final int		cost_;

		public CidrCost(Ip4Cidr cidr, int cost) {
			cidr_ = cidr;
			cost_ = cost;
		}
	}

	private CidrNode			root_;
	private static long[]		BITMASKS;
	private static final int	NOT_REMOVED		= 0;
	private static final int	NONLEAF_REMOVED	= 1;
	private static final int	LEAF_REMOVED	= 2;
	
	static {
		BITMASKS = new long[32];
		long mask = 1;
		for (int i = 31; i >= 0; i--) {
			BITMASKS[i] = mask;
			mask = mask << 1;
		}
	}

	public CidrCostTree() {
		root_ = new CidrNode();
	}
	
	public synchronized CidrCost addCidr(Ip4Cidr cidr, int cost) {
		short level;
		CidrNode node;
		CidrCost result;
		level = cidr.getPrefixLength();
		result = null;
		if (level == 0) {
			// updating root
			result = new CidrCost(root_.cidr_, root_.cost_);
			root_.cidr_ = cidr;
			root_.cost_ = cost;
			return result;
		}
		node = root_;
		for (int i = 0; i < level; i++) {
			if ((cidr.getPrefix().numericForm_ & BITMASKS[i]) == 0) {
				// left
				if (i == level - 1) {
					if (node.left_ == null)
						node.left_ = new CidrNode(cidr, cost);
					else {
						result = new CidrCost(node.left_.cidr_,
							node.left_.cost_);
						node.left_.cidr_ = cidr;
						node.left_.cost_ = cost;
					}
					return result;
				}
				if (node.left_ == null)
					node.left_ = new CidrNode();
				node = node.left_;
			} else {
				// right
				if (i == level - 1) {
					if (node.right_ == null)
						node.right_ = new CidrNode(cidr, cost);
					else {
						result = new CidrCost(node.right_.cidr_,
							node.right_.cost_);
						node.right_.cidr_ = cidr;
						node.right_.cost_ = cost;
					}
					return result;
				}
				if (node.right_ == null)
					node.right_ = new CidrNode();
				node = node.right_;
			}
		}
		// impossible
		return null;
	}

	public synchronized CidrCost getLongestMatchingCidr(Ip4Address ip) {
		CidrNode node;
		CidrCost result;
		int i;
		node = root_;
		result = null;
		i = 0;
		do {
			if (node.cidr_ != null)
				result = new CidrCost(node.cidr_, node.cost_);
			if (i == 32) // end of tree
				break;
			if ((ip.numericForm_ & BITMASKS[i]) == 0)
				// left
				node = node.left_;
			else
				// right
				node = node.right_;
			i++;
		} while (node != null);
		return result;
	}
	
	public synchronized int getCost(Ip4Address ip, int defaultCost) {
		CidrCost result;
		
		result = getLongestMatchingCidr(ip);
		return result == null ? defaultCost : result.cost_;
	}

	public synchronized Collection<CidrCost> getCidrs() {
		SortedSet<CidrCost> result;
		result = new TreeSet<CidrCost>(new Comparator<CidrCost>() {

			public int compare(CidrCost arg0, CidrCost arg1) {
				int prefixCompare;
				long addressCompare;
				prefixCompare = arg0.cidr_.prefixLength_
					- arg1.cidr_.prefixLength_;
				if (prefixCompare != 0)
					return prefixCompare;
				addressCompare = arg0.cidr_.prefix_.numericForm_
					- arg1.cidr_.prefix_.numericForm_;
				if (addressCompare < 0)
					return -1;
				if (addressCompare > 0)
					return 1;
				return 0;
			}
		});
		scanNode(result, root_);
		return result;
	}

	public synchronized void clearTree() {
		root_ = new CidrNode();
	}
	
	public synchronized boolean removeNode(Ip4Cidr cidr){
		switch (doRemoveNode(root_, cidr)) {
		case NOT_REMOVED:
			return false;
		case LEAF_REMOVED:
		case NONLEAF_REMOVED:
			return true;
		default: // impossible
			return false;
		}
	}

	private int doRemoveNode(CidrNode node, Ip4Cidr cidr) {
		if (node == null) 
			return NOT_REMOVED;
		
		if (node.cidr_ != null && node.cidr_.equals(cidr)){
			node.cidr_ = null;
			node.cost_ = 0;
			return (node.left_ == null && node.right_ == null) ? 
				LEAF_REMOVED : NONLEAF_REMOVED;
		}
		
		switch (doRemoveNode(node.left_, cidr)) {
		case NOT_REMOVED: 
			break; //do nothing
		case NONLEAF_REMOVED:
			return NONLEAF_REMOVED;
		case LEAF_REMOVED:
			node.left_ = null;
			return node.right_ == null ? LEAF_REMOVED : NONLEAF_REMOVED;
		}
		
		switch (doRemoveNode(node.right_, cidr)) {
		case NOT_REMOVED: 
			break; //do nothing
		case NONLEAF_REMOVED:
			return NONLEAF_REMOVED;
		case LEAF_REMOVED:
			node.right_ = null;
			return node.left_ == null ? LEAF_REMOVED : NONLEAF_REMOVED;
		}
		
		return NOT_REMOVED;
	}

	private void scanNode(Collection<CidrCost> result, CidrNode node) {
		if (node == null)
			return;
		if (node.cidr_ == null) {
			scanNode(result, node.left_);
			scanNode(result, node.right_);
		} else {
			if (!isSuperfluous(node.left_, node.cost_))
				scanNode(result, node.left_);
			result.add(new CidrCost(node.cidr_, node.cost_));
			if (!isSuperfluous(node.right_, node.cost_))
				scanNode(result, node.right_);
		}
	}
	
	private boolean isSuperfluous(CidrNode node, int ancestorCost) {
		if (node == null)
			return true;
		if (node.cidr_ != null && node.cost_ != ancestorCost)
			return false;
		return isSuperfluous(node.left_, ancestorCost) && 
			isSuperfluous(node.right_, ancestorCost);
	}
	
	public synchronized void saveMapOnFile(String path) throws BassException {
		File f;
		FileOutputStream fos;
		Properties cidrList;
		cidrList = new Properties();
		for (CidrCost cidrCost: getCidrs())
			cidrList.put(cidrCost.cidr_.toString(), Integer.toString(cidrCost.cost_));
		try {
			f = new File(path);
			fos = new FileOutputStream(f);
			cidrList.store(fos, "Network Map");
			} catch (Exception ioe) {
				ioe.printStackTrace();
			throw new BassException("Save Map error", ioe);
		}
			return;
	}

	@SuppressWarnings("rawtypes")
	public synchronized void loadMapFromFile(String path) throws BassException {
		File f;
		FileInputStream fis;
		Enumeration keys, values;
		String cidrString;
		String costString;
		Properties cidrList;
		
		try {
			f = new File(path);
			fis = new FileInputStream(f);
			cidrList = new Properties();
			cidrList.load(fis);
		} catch (Exception ex) {
			throw new BassException("Load Map error", ex);
		}

		keys = cidrList.keys();
		values = cidrList.elements();
		try {
			clearTree();
			while (keys.hasMoreElements() && values.hasMoreElements()) {
				cidrString = keys.nextElement().toString();
				costString = values.nextElement().toString();
				addCidr(new Ip4Cidr(cidrString), Integer
					.parseInt(costString));
			}
		} catch (NumberFormatException nfe) {
			throw new BassException("Bad Map format", nfe);
		}
	}
}
