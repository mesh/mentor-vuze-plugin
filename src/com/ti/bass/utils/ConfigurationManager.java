/*
 * Created on 1-Dic-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.utils;

import java.io.*;
import java.util.Properties;

/**
 * Utility class for the management of a configuration file for BassPlugin
 */
public class ConfigurationManager {
	
	private static final String	CONFIG_FILE		= "bass.config";
	private Properties properties = new Properties();
	
	
	public ConfigurationManager() {
	
		// TODO: use external bass.config file to change cost weights
		// - they only affect handshaking and optimistic unchoking
		// - def values there are both 100
		// - we should set "Weight.on.connection" to 0 to use std vuze strategy
		// - we should set "Weight.on.unchoke" to 0 to use std vuze strategy
		
		// TODO: mesh modification
		//DEFAULT VALUES
//		properties.setProperty("Weight.on.connection","50");
//		properties.setProperty("Weight.on.unchoke","50");
		properties.setProperty("Weight.on.connection","0");
		properties.setProperty("Weight.on.unchoke","0");
		
		if (new File(CONFIG_FILE).exists()) 
			loadConfiguration();
	    else 
	    	saveConfiguration();
	
	}
	
	public void setProperty(String name, String value) {
		properties.setProperty(name, value);
		saveConfiguration();
	}	
	
	public String getProperty(String name) {
		return properties.getProperty(name);
	}	
	
	private void saveConfiguration() {
		try {
			properties.store(new FileOutputStream(CONFIG_FILE), null);
		} catch (IOException e) {
		}
	}

	private void loadConfiguration() {
	    try {
	        properties.load(new FileInputStream(CONFIG_FILE));
	    } catch (IOException e) {
	    }

	}
	
	
	
	
}
