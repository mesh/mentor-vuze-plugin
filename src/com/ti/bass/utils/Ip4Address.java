/*
 * Created on 1-Oct-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.utils;

import com.ti.bass.core.exception.BassException;

/**
 * Utility class for the management of IPv4 addresses. It checks the sintax and
 * the correctness of the numbers in the dotted decimal notation.  
 */
public class Ip4Address implements Comparable<Ip4Address> {
	
	final static long MAX_NUMERIC_FORM = 4294967295L;  
	final long numericForm_;
	
	public Ip4Address(String string) throws BassException {
		String[] ipByteStrings;
		long[] ipBytes;
		ipByteStrings = string.split("\\.", 5);
		if (ipByteStrings.length != 4)
			throw new BassException("Bad IP address format");
		ipBytes = new long[4];
		try {
			for (int i = 0; i < 4; i++) {
				ipBytes[i] = new Long(ipByteStrings[i]);
				if (ipBytes[i] < 0 || ipBytes[i] > 255)
					throw new BassException("Bad IP address format");
			}
		} catch (NumberFormatException nfe) {
			throw new BassException("Bad IP address format", nfe);
		}
		numericForm_ = ipBytes[3] + (ipBytes[2] << 8) + (ipBytes[1] << 16)
			+ (ipBytes[0] << 24);
	}

	public Ip4Address(long numericForm) throws BassException {
		if (numericForm < 0 || numericForm > MAX_NUMERIC_FORM)
			throw new BassException("Bad IP address numeric form");
		numericForm_ = numericForm;
	}

	@Override
	public boolean equals(Object obj) {
		Ip4Address other;
		
		if (!(obj instanceof Ip4Address))
			return false;
		
		other = (Ip4Address)obj;
		return other.numericForm_ == numericForm_;
	}

	@Override
	public int hashCode() {
		return new Long(numericForm_).hashCode();
	}
	
	@Override
	public String toString() {
		long mask = 255L;
		return Long.toString(numericForm_ >> 24) + "." +
			Long.toString(numericForm_ >> 16 & mask) + "." +
			Long.toString(numericForm_ >> 8 & mask) + "." +
			Long.toString(numericForm_ & mask);
	}

	public int compareTo(Ip4Address other) {
		return other.numericForm_ > numericForm_ ? 1 : 
			other.numericForm_ < numericForm_ ? -1 : 0;
	}
}
