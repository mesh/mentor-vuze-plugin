/*
 * Created on 1-Oct-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.utils;

import com.ti.bass.core.exception.BassException;

/**
 * Utility class for the management of IPv4 CIDR in the xxx.xxx.xxx.xxx/n 
 * notation. It manages network prefixes, network masks and determines whether
 * an IPv4 address belongs to a subnet or not
 */
public class Ip4Cidr {
	
	final short prefixLength_, leastSignificantBits_;
	final Ip4Address prefix_, netMask_;
	
	public Ip4Cidr(String string) throws BassException {
		Ip4Address ip;
		String[] splitted;
		long nm;
		
		splitted = string.split("/", 3);
		if (splitted.length != 2)
			throw new BassException("Bad CIDR format");
		try {
			ip = new Ip4Address(splitted[0]);
			prefixLength_ = new Short(splitted[1]);
			leastSignificantBits_ = (short) (32 - prefixLength_);
			nm = Ip4Address.MAX_NUMERIC_FORM >> leastSignificantBits_;
			nm <<= leastSignificantBits_;
			netMask_ = new Ip4Address(nm);
			prefix_ = new Ip4Address(ip.numericForm_ & netMask_.numericForm_);
		} catch (BassException be) {
			throw new BassException("Bad CIDR format", be);
		} catch (NumberFormatException nfe) {
			throw new BassException("Bad CIDR format", nfe);
		}
		if (prefixLength_ < 0 || prefixLength_ > 32)
			throw new BassException("Bad CIDR format");
	}

	
	public Ip4Address getPrefix() {
		return prefix_;
	}
	
	public short getPrefixLength() {
		return prefixLength_;
	}

	public Ip4Address getNetMask() {
		return netMask_;
	}
	
	public boolean contains(Ip4Address ip) {
		return (ip.numericForm_ & netMask_.numericForm_) == prefix_.numericForm_;
	}
	
	@Override
	public boolean equals(Object obj) {
		Ip4Cidr other;
		
		if (!(obj instanceof Ip4Cidr))
			return false;
		
		other = (Ip4Cidr)obj;
		return other.prefix_.equals(prefix_);
	}
	
	@Override
	public int hashCode() {
		return prefix_.hashCode();
	}
	
	@Override
	public String toString() {
		return prefix_ + "/" + prefixLength_;
	}
}
