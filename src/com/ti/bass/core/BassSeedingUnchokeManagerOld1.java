
package com.ti.bass.core;

import java.text.SimpleDateFormat; // TODO: mesh modification

import mesh.ConfFileParams; // TODO: mesh modification

import java.util.*;

import org.gudy.azureus2.core3.disk.DiskManager;
import org.gudy.azureus2.core3.peer.impl.PEPeerTransport;

import com.aelitis.azureus.core.peermanager.peerdb.PeerItem;
import com.aelitis.azureus.core.peermanager.unchoker.UnchokerUtil;
import com.ti.bass.core.adapters.SeedingUnchokerAdapter;
import com.ti.bass.core.exception.BassException;
import com.ti.bass.core.peerselection.PeerSelectionService.PeerCost;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class BassSeedingUnchokeManagerOld1 extends SeedingUnchokerAdapter {

	private BassPlugin	plugin_;
	private Random		random_;
	private ConfFileParams config_; // TODO: mesh modification

	public BassSeedingUnchokeManagerOld1(BassPlugin plugin) {
		plugin_ = plugin;
		random_ = new Random();
		config_ = ConfFileParams.getInstance(); // TODO: mesh modification
	}

	@Override
	public ArrayList getImmediateUnchokes(int maxToUnchoke, ArrayList allPeers)
	{
		// TODO: mesh modification
		System.out.println("Bass Seeding Unchoke Manager	-	getImmediateUnchokes()");
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
		System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		
		// TODO: mesh modification
		System.out.println("getImmediateUnchokes()	-	allPeers:");
		for (Object entry : allPeers) {
			PEPeerTransport p = (PEPeerTransport) entry;
			System.out.println(p.getIp());
		}
		
		// TODO: mesh modification
		maxToUnchoke = config_.getNODE_UPLOAD_COUNT();
		
		ArrayList toUnchoke = new ArrayList();
		// count all the currently unchoked peers
		int numUnchoked = 0;
		PEPeerTransport peer;
		for (int i = 0; i < allPeers.size(); i++) {
			peer = (PEPeerTransport) allPeers.get(i);
			if (!peer.isChokedByMe())
				numUnchoked++;
		}
		// if not enough unchokes
		int needed = maxToUnchoke - numUnchoked;
		if (needed > 0) {
			for (int i = 0; i < needed; i++) {
				// Bass code hooks here
				peer = getNextOptimisticPeer(allPeers);
				// end of Bass specific code
				if (peer == null)
					break; // no more new unchokes avail

				// TODO: mesh modification
				if (!toUnchoke.contains(peer)) {
					toUnchoke.add(peer);
					peer.setOptimisticUnchoke(true);
				}
			}
		}
		
		// TODO: mesh modification
		System.out.println("immediate unchokes:");
		for (Object entry : toUnchoke)
		{
			System.out.println(((PEPeerTransport)entry).getIp());
		}
		
		return toUnchoke;
	}

	
	
	@Override
	public void calculateUnchokes(int maxToUnchoke, ArrayList allPeers,
									boolean forceRefresh,
									boolean checkPriorityConnections)
	{
		
		// TODO: mesh modification
		System.out.println("Bass Seeding Unchoke Manager	-	calculateUnchokes()");
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
		System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		
		// TODO: mesh modification
		maxToUnchoke = config_.getNODE_UPLOAD_COUNT(); // number of upload slots (default 4 = 3 normal + 1 optimistic)
		
		// TODO: mesh modification
		System.out.println("calculateUnchokes()	-	allPeers:");
		for (Object entry : allPeers) {
			PEPeerTransport p = (PEPeerTransport) entry;
			System.out.println(p.getIp());
		}
		
		
		// TODO: it would be best to modify current BassSeedingUnchoker!
		// -> use code of original Vuze Seeding Unchoker in VANILLA mode and modified version of it in ALM mode
		// -> right now, in VANILLA mode peers are chosen as optimistics, since peers are not interesting for seeder
		//    and there is no considerable upload rate from them!
		// -> ALM mode is OK now, we removed "isInteresting" criterion, peers are chosen on ALM basis
		//    (nevertheless, adding this to original Seeding Unchoker would be better)
		
		
		// TODO: mesh modification
		// is VANILLA -> use unmodified choking, as in dummy plugin
		if (config_.getVANILLA())
		{
			System.out.println("Using VANILLA mode ...");
			
			// almost cloned from vuze DownloadingUnchoker implementation
			ArrayList chokes = getChokesField();
			ArrayList unchokes = getUnchokesField();
			// one optimistic unchoke for every 10 upload slots
			int maxOptimistic = ((maxToUnchoke - 1) / 10) + 1;
			ArrayList optimisticUnchokes = new ArrayList();
			ArrayList bestPeers = new ArrayList();
			long[] bests = new long[maxToUnchoke]; // ensure we never pick more slots than allowed to unchoke
			PEPeerTransport peer;
			long rate, uploadedRatio;
			int startPos;
			
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp());
			}
			
			// get all the currently unchoked peers
			for (int i = 0; i < allPeers.size(); i++) {
				peer = (PEPeerTransport) allPeers.get(i);
				if (!peer.isChokedByMe()) {
					if (UnchokerUtil.isUnchokable(peer, true)) {
						unchokes.add(peer);
						if (peer.isOptimisticUnchoke()) {
							optimisticUnchokes.add(peer);
						}
					} else { // should be immediately choked
						chokes.add(peer);
					}
				}
			}
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp());
			}
			
			if (!forceRefresh) { // ensure current optimistic unchokes remain unchoked
				for (int i = 0; i < optimisticUnchokes.size(); i++) {
					peer = (PEPeerTransport) optimisticUnchokes.get(i);
					if (i < maxOptimistic) {
						bestPeers.add(peer); // add them to the front of the "best" list
					} else { // too many optimistics
						peer.setOptimisticUnchoke(false);
					}
				}
			}
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp());
			}
			
			// TODO: mesh modification
			System.out.println("looking for peers to add, following rate ...");
			// fill slots with peers who we are currently downloading the fastest from
			startPos = bestPeers.size();
			for (int i = 0; i < allPeers.size(); i++) {
				peer = (PEPeerTransport) allPeers.get(i);
				
				// TODO: mesh modification
				System.out.println("IP: "+peer.getIp());
				System.out.println("isInteresting: "+peer.isInteresting());
				System.out.println("isUnchokable: "+UnchokerUtil.isUnchokable(peer, false));
				System.out.println("alreadyInBest: "+bestPeers.contains(peer));
				
				if (peer.isInteresting() && UnchokerUtil.isUnchokable(peer, false)
					&& !bestPeers.contains(peer)) { // viable peer found
					rate = peer.getStats().getSmoothDataReceiveRate();
					if (rate > 256) { // filter out really slow peers
						
						// TODO: mesh modification
						System.out.println("input for 1st updateLargestValueFirstSort(): "+peer.getIp()+", "+rate);
						UnchokerUtil.updateLargestValueFirstSort(rate, bests, peer, bestPeers, startPos);
					}
				}
			}
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp());
			}
			
			// TODO: mesh modification
			System.out.println("looking for peers to add, following past rate ...");
			// if we haven't yet picked enough slots
			if (bestPeers.size() < maxToUnchoke) {
				startPos = bestPeers.size();
				// fill the remaining slots with peers that we have downloaded from in the past
				for (int i = 0; i < allPeers.size(); i++) {
					peer = (PEPeerTransport) allPeers.get(i);
					
					// TODO: mesh modification
					System.out.println("IP: "+peer.getIp());
					System.out.println("isInteresting: "+peer.isInteresting());
					System.out.println("isUnchokable: "+UnchokerUtil.isUnchokable(peer, false));
					System.out.println("alreadyInBest: "+bestPeers.contains(peer));
					
					if (peer.isInteresting()
						&& UnchokerUtil.isUnchokable(peer, false)
						&& !bestPeers.contains(peer)) { // viable peer found
						uploadedRatio = peer.getStats().getTotalDataBytesSent()
							/ (peer.getStats().getTotalDataBytesReceived() + (DiskManager.BLOCK_SIZE - 1));
						// make sure we haven't already uploaded several times as
						// much data as they've sent us
						if (uploadedRatio < 3) {
							
							// TODO: mesh modification
							System.out.println("input for 1st updateLargestValueFirstSort(): "+peer.getIp()+", " + 
									peer.getStats().getTotalDataBytesReceived());
							
							UnchokerUtil.updateLargestValueFirstSort(
								peer.getStats().getTotalDataBytesReceived(), bests, peer, bestPeers, startPos);
						}
					}
				}
			}
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp());
			}
			
			if (forceRefresh) {
				
				// TODO: mesh modification
				System.out.println("30 s refresh -> new optimistic is chosen");
				
				// make space for new optimistic unchokes
				while (bestPeers.size() > maxToUnchoke - maxOptimistic) {
					bestPeers.remove(bestPeers.size() - 1);
				}
			}
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp());
			}
			
			// if we still have remaining slots
			while (bestPeers.size() < maxToUnchoke) {
				// Bass code hooks here
				peer = getNextOptimisticPeer(allPeers);
				// end of Bass specific code
				if (peer == null)
					break; // no more new unchokes avail
				if (!bestPeers.contains(peer)) {
					bestPeers.add(peer);
					peer.setOptimisticUnchoke(true);
					
					// TODO: mesh modification
					System.out.println("added new optimistic: "+peer.getIp());
					
				} else {
					// we're here because the given optimistic peer is already
					// "best", but is choked still,
					// which means it will continually get picked by the
					// getNextOptimisticPeer() method,
					// and we'll loop forever if there are no other peers to choose from
					peer.sendUnChoke();
					// send unchoke immediately, so it won't get
					// picked optimistically anymore
					// allPeers.remove( peer ); 
					// remove from allPeers list, so it
					// won't get picked optimistically anymore //TODO
				}
			}
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp());
			}
			
			// update chokes
			for (Iterator it = unchokes.iterator(); it.hasNext();) {
				peer = (PEPeerTransport) it.next();
				if (!bestPeers.contains(peer)) { // should be choked
					if (bestPeers.size() < maxToUnchoke) {
						// but there are still slots needed (no optimistics avail),
						// so don't bother choking them
						bestPeers.add(peer);
					} else {
						chokes.add(peer);
						it.remove();
					}
				}
			}
			// update unchokes
			for (int i = 0; i < bestPeers.size(); i++) {
				peer = (PEPeerTransport) bestPeers.get(i);
				if (!unchokes.contains(peer)) {
					unchokes.add(peer);
				}
			}
			
			
			// TODO: mesh modification
			// print chokes / unchokes arrays
			System.out.println("Bass Seeding Unchoke Manager	-	calculateUnchokes()");
			timestamp = new Date();
			simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
			System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
			
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp());
			}
			
			// TODO: mesh modification
			// apparently the vanilla plugin does not modify anything,
			// it would need to overwrite vuze's chokes and unchokes fields
			// with the just calculated values here -> we now do it here!
			setUnchokesField(unchokes);
			setChokesField(chokes);
		}
		
		
		
		
		// TODO: mesh modification
		// is not VANILLA -> use modified choking for 802.11s mesh with ALM
		else if (!config_.getVANILLA())
		{
			System.out.println("Using ALM mesh mode ...");
			
			
			// - the first code part just gets the current mesh nodes' IPs and associated ALM values from this node's perspective
			// - the mesh nodes IP<->metric maps are further separated into 1-hop neighbors and nodes in multi-hop distance
			// - the created arrays are later used in the modified chokes/unchokes calculation
			
			boolean useOnly1Hop = config_.getPREFER_ONEHOP_PEERS(); // allow only one-hop or all peers as possible downloaders
			
			// maps with IP <-> metric, ordered for 1-hop nodes and rest
			HashMap<String, Integer> sortedonehopmap =
					new LinkedHashMap<String, Integer>(plugin_.getPeerSelectionService().getsortedonehopmap()); // direct neighbors only
			
			HashMap<String, Integer> sortednhopmap =
					new LinkedHashMap<String, Integer>(plugin_.getPeerSelectionService().getsortednhopmap()); // all remaining nodes (Multi-Hop)
			
			// map with all nodes info (1-hop + multi-hop)
			Map<String, Integer> sortedAllNodesMap = new LinkedHashMap<String, Integer>();
			
			for (Map.Entry<String, Integer>	entry :	sortedonehopmap.entrySet())
				sortedAllNodesMap.put(entry.getKey(), entry.getValue()*(-1));
			for (Map.Entry<String, Integer>	entry :	sortednhopmap.entrySet())
				sortedAllNodesMap.put(entry.getKey(), entry.getValue()*(-1));
			
			sortedAllNodesMap = plugin_.getPeerSelectionService().sortMapByValues(sortedAllNodesMap);
			
			
			System.out.println();
			System.out.println("sortedonehopmap:");
			for (Map.Entry<String, Integer> entry : sortedonehopmap.entrySet()) {
				System.out.println("IP: "+entry.getKey()+"\t"+"Metric: "+entry.getValue());
			}
			
			System.out.println();
			System.out.println("sortednhopmap:");
			for (Map.Entry<String, Integer> entry : sortednhopmap.entrySet()) {
				System.out.println("IP: "+entry.getKey()+"\t"+"Metric: "+entry.getValue());
			}
			
			System.out.println();
			System.out.println("sortedAllNodesMap:");
			for (Map.Entry<String, Integer> entry : sortedAllNodesMap.entrySet()) {
				System.out.println("IP: "+entry.getKey()+"\t"+"Metric: "+entry.getValue());
			}
			System.out.println();
			
			
			// TODO: modified chokes/unchokes calculation starts here:
			
			ArrayList chokes = new ArrayList();
			ArrayList unchokes = new ArrayList();
			// one optimistic unchoke for every 10 upload slots
			int maxOptimistic = ((maxToUnchoke - 1) / 10) + 1;
			ArrayList optimisticUnchokes = new ArrayList();
			ArrayList bestPeers = new ArrayList();
			
			long[] bests = new long[maxToUnchoke]; // ensure we never pick more slots than allowed to unchoke
			Arrays.fill( bests, Long.MIN_VALUE ); // we want to store negative values (inverted ALM)
			
			PEPeerTransport peer;
			int startPos;
			
			
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
				
			// get all the currently unchoked peers
			for (int i = 0; i < allPeers.size(); i++) {
				peer = (PEPeerTransport) allPeers.get(i);
				if (!peer.isChokedByMe()) {
					if (UnchokerUtil.isUnchokable(peer, true)) {
						unchokes.add(peer);
						if (peer.isOptimisticUnchoke()) {
							optimisticUnchokes.add(peer);
						}
					} else { // should be immediately choked
						chokes.add(peer);
					}
				}
			}
			
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			if (!forceRefresh) { // ensure current optimistic unchokes remain unchoked
				for (int i = 0; i < optimisticUnchokes.size(); i++) {
					peer = (PEPeerTransport) optimisticUnchokes.get(i);
					if (i < maxOptimistic) {
						bestPeers.add(peer); // add them to the front of the "best" list
					} else { // too many optimistics
						peer.setOptimisticUnchoke(false);
					}
				}
			}
			
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			System.out.println("looking for peers to add, following ALM ...");
			startPos = bestPeers.size();
			for (int i = 0; i < allPeers.size(); i++)
			{
				peer = (PEPeerTransport) allPeers.get(i);
				String peerIP = peer.getIp();
				
				System.out.println("IP: "+peerIP);
//				System.out.println("isInteresting: "+peer.isInteresting());
				System.out.println("isUnchokable: "+UnchokerUtil.isUnchokable(peer, false));
				System.out.println("alreadyInBest: "+bestPeers.contains(peer));
				
				
				// TODO: we are Seeder -> leave out "isInteresting" criterion since this is always false
//				if (peer.isInteresting()
//						&& UnchokerUtil.isUnchokable(peer, false)
//						&& !bestPeers.contains(peer))
//				{
				if (UnchokerUtil.isUnchokable(peer, false)
						&& !bestPeers.contains(peer))
				{
					System.out.println("useOnly1Hop: "+useOnly1Hop);
					// TODO: here we evaluate the neighborhood scope limit!
					if (useOnly1Hop) // scope limited to 1-hop neighbors
					{
						// check if peerIP is among 1-hop nodes
						if (!sortedonehopmap.keySet().contains(peerIP))
						{
							// peer is in multi-hop distance -> do not consider it
							continue;
						}
					}
					
					// TODO: here we use ALM instead of UL rate of interested peers!
					// we use our metric data structures created above
					// -> get metric for peerIP
					long metric = (long) sortedAllNodesMap.get(peerIP);
					System.out.println("input for 1st updateLargestValueFirstSort(): "+peerIP+", "+metric);
					UnchokerUtil.updateLargestValueFirstSort(metric, bests, peer, bestPeers, startPos);
				}
			}
			
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			System.out.println("still slots avail, looking for peers to add, following ALM ...");
			// if we haven't yet picked enough slots
			if (bestPeers.size() < maxToUnchoke)
			{
				startPos = bestPeers.size();
				// fill the remaining slots with peers that we have downloaded from in the past
				for (int i = 0; i < allPeers.size(); i++)
				{
					peer = (PEPeerTransport) allPeers.get(i);
					String peerIP = peer.getIp();
					
					System.out.println("IP: "+peerIP);
//					System.out.println("isInteresting: "+peer.isInteresting());
					System.out.println("isUnchokable: "+UnchokerUtil.isUnchokable(peer, false));
					System.out.println("alreadyInBest: "+bestPeers.contains(peer));
					
					// TODO: we are Seeder -> leave out "isInteresting" criterion since this is always false
//					if (peer.isInteresting()
//							&& UnchokerUtil.isUnchokable(peer, false)
//							&& !bestPeers.contains(peer))
//					{
					if (UnchokerUtil.isUnchokable(peer, false)
							&& !bestPeers.contains(peer))
					{
						System.out.println("useOnly1Hop: "+useOnly1Hop);
						// TODO: here we evaluate the neighborhood scope limit!
						if (useOnly1Hop) // scope limited to 1-hop neighbors
						{
							// check if peerIP is among 1-hop nodes
							if (!sortedonehopmap.keySet().contains(peerIP))
							{
								// peer is in multi-hop distance -> do not consider it
								continue;
							}
						}
						
						// TODO: here we just fill bestPeers list with more nodes following ALM metric!
						long metric = (long) sortedAllNodesMap.get(peerIP);
						System.out.println("input for 2nd updateLargestValueFirstSort(): "+peerIP+", "+metric);
						UnchokerUtil.updateLargestValueFirstSort(metric, bests, peer, bestPeers, startPos);
					}
				}
			}
			
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			if (forceRefresh) {
				System.out.println("30 s refresh -> new optimistic is chosen");
				// make space for new optimistic unchokes
				while (bestPeers.size() > maxToUnchoke - maxOptimistic) {
					bestPeers.remove(bestPeers.size() - 1);
				}
			}
			
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			// if we still have remaining slots
			while (bestPeers.size() < maxToUnchoke) {
				// Bass code hooks here
				peer = getNextOptimisticPeer(allPeers); // TODO: if weights are set to 0, default vuze strategy is used here
				// end of Bass specific code
				if (peer == null)
					break; // no more new unchokes avail
				if (!bestPeers.contains(peer)) {
					bestPeers.add(peer);
					peer.setOptimisticUnchoke(true);
					System.out.println("added new optimistic: "+peer.getIp());
				} else {
					// we're here because the given optimistic peer is already
					// "best", but is choked still,
					// which means it will continually get picked by the
					// getNextOptimisticPeer() method,
					// and we'll loop forever if there are no other peers to choose from
					peer.sendUnChoke();
					// send unchoke immediately, so it won't get
					// picked optimistically anymore
					// allPeers.remove( peer ); 
					// remove from allPeers list, so it
					// won't get picked optimistically anymore //TODO
				}
			}
			
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			// update chokes
			for (Iterator it = unchokes.iterator(); it.hasNext();) {
				peer = (PEPeerTransport) it.next();
				if (!bestPeers.contains(peer)) { // should be choked
					if (bestPeers.size() < maxToUnchoke) {
						// but there are still slots needed (no optimistics avail),
						// so don't bother choking them
						bestPeers.add(peer);
					} else {
						chokes.add(peer);
						it.remove();
					}
				}
			}
			// update unchokes
			for (int i = 0; i < bestPeers.size(); i++) {
				peer = (PEPeerTransport) bestPeers.get(i);
				if (!unchokes.contains(peer)) {
					unchokes.add(peer);
				}
			}			
			
			
			// TODO: print chokes / unchokes arrays
			System.out.println("Bass Seeding Unchoke Manager	-	calculateUnchokes()");
			timestamp = new Date();
			simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
			System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
			
			System.out.println("best peers:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeerTransport entry : (ArrayList<PEPeerTransport>) chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			// TODO: apply calculated chokes / unchokes fields
			setUnchokesField(unchokes);
			setChokesField(chokes);
		}
		
	}

	
	private PEPeerTransport getNextOptimisticPeer(ArrayList allPeers)
	{
		// TODO: mesh modification
		System.out.println("Bass Seeding Unchoke Manager	-	getNextOptimisticPeer()");
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
		System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		
		// TODO: mesh modification
		System.out.println("getNextOptimisticPeer()	-	allPeers:");
		for (Object entry : allPeers) {
			PEPeerTransport p = (PEPeerTransport) entry;
			System.out.println(p.getIp());
		}
		
		List<PeerItem> peerItems;
		List<PEPeerTransport> pepeers;
		PEPeerTransport selectedPepeer;
		Map<PeerItem, PEPeerTransport> pepeerByPeerItem;
		PeerItem selectedPeerItem;
		SortedSet<PeerCost> peerCosts;
		Iterator<PeerCost> i;
		PeerCost peerCost;
		List<PeerItem> bestPeerItems;
		int bestCost;
		
		if (random_.nextInt(100) + 1 > plugin_.getPeerSelectionWeightOnUnchoke())
		{
			selectedPepeer = (PEPeerTransport)UnchokerUtil.getNextOptimisticPeer(allPeers, true, true);
			if (selectedPepeer != null)
			{
				plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
					"Standard Vuze unchoke strategy: next optimistic peer " + 
					selectedPepeer.getIp());
			
				// TODO: mesh modification
				System.out.println("next optimistic peer: "+selectedPepeer.getIp());
				
			} else {
				plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
					"Standard Vuze unchoke strategy: not any unchokable peer available");
				
				// TODO: mesh modification
				System.out.println("no next optimistic peer available!");
			}
			
			return selectedPepeer;
		}
		// get the peers potentially available for optimistic selection
		// (PEPeerTransport objects)
		pepeers = (List)UnchokerUtil.getNextOptimisticPeers(allPeers, false, true, allPeers.size());
		if (pepeers == null) {
			plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
				"Bass unchoke strategy: no unchokable peer available");
			return null;
		}
		// convert PEPeerTransport objects into PeerItem objects
		// and put PEPeerTransport objects into a PeerItem-indexed map, 
		// in order to do later the opposite conversion (PeerItem -> PEPeerTransport) 
		peerItems = new LinkedList<PeerItem>();
		pepeerByPeerItem = new HashMap<PeerItem, PEPeerTransport>();
		for (PEPeerTransport pepeer : pepeers) {
			peerItems.add(pepeer.getPeerItemIdentity());
			pepeerByPeerItem.put(pepeer.getPeerItemIdentity(), pepeer);
		}
		try {
			// the PeerSelectionService returns the cost for each peer
			peerCosts = plugin_.getPeerSelectionService().getPeerCosts(peerItems);
			// builds the list of peers with the lowest cost, 
			bestPeerItems = new LinkedList<PeerItem>();
			i = peerCosts.iterator();
			peerCost = i.next();
			bestCost = peerCost.cost_;
			bestPeerItems.add(peerCost.peerItem_);
			while (i.hasNext()) {
				peerCost = i.next();
				if (peerCost.cost_ != bestCost)
					break;
				bestPeerItems.add(peerCost.peerItem_);
			}
			if (bestPeerItems.size() == 1)
				selectedPeerItem = bestPeerItems.get(0);
			else 
				// if there are two or more peers with the lowest cost, 
				// it randomly chooses among them
				selectedPeerItem = bestPeerItems.get(random_.nextInt(bestPeerItems.size()));
			plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
				"Bass unchoke strategy: next optimistic peer " + 
				selectedPeerItem.getAddressString());
			// convert the chosen PeerItem back to a PEPeerTransport object
			return pepeerByPeerItem.get(selectedPeerItem); 
		} catch (BassException be) {
			// fallback
			plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
				"Unable to use Bass unchoke strategy: next optimistic peer selected using standard Vuze strategy");
			selectedPepeer = (PEPeerTransport)UnchokerUtil.getNextOptimisticPeer(allPeers, true, true);
			if (selectedPepeer != null)
				plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
					"Standard Vuze unchoke strategy: next optimistic peer " + 
					selectedPepeer.getIp());
			else
				plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
					"Standard Vuze unchoke strategy: no unchokable peer available");
			return selectedPepeer;
		}
	}
}
