/*
 * Created on 30-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.core.peerselection;

import java.util.*;
import org.gudy.azureus2.ui.swt.views.IView;
import com.aelitis.azureus.core.peermanager.peerdb.PeerItem;
import com.ti.bass.core.exception.BassException;
import com.ti.bass.ui.BassUI;
import com.ti.bass.utils.Ip4Address;

public interface PeerSelectionService {

	public class PeerCost implements Comparable<PeerCost>{
		
		public final PeerItem peerItem_;
		public final int cost_;
		
		public PeerCost(PeerItem peerItem, int cost) {
			peerItem_ = peerItem;
			cost_ = cost;
		}
		
		public PeerItem getPeerItem() {
			return peerItem_;
		}

		public int getCost() {
			return cost_;
		}
		
		public int compareTo(PeerCost other) {
			int addressCompare;
			if (cost_ != other.cost_)
				return cost_ - other.cost_;
			try {
				addressCompare = new Ip4Address(peerItem_.getIP())
					.compareTo(new Ip4Address(other.peerItem_.getIP()));
			} catch (BassException e) {
				return 0;
			}
			if (addressCompare != 0)
				return addressCompare;
			return other.peerItem_.getTCPPort() - peerItem_.getTCPPort(); 
		}
	}
	
	public SortedSet<PeerCost> getPeerCosts(Collection<PeerItem> peers)
		throws BassException;
	
	
	
	public Map<String, Integer> getsortedonehopmap(); // TODO: mesh modification
	public Map<String, Integer> getsortednhopmap();	 // TODO: mesh modification
	public Map<String, Integer> sortMapByValues(Map<String, Integer> passedMap); // TODO: mesh modification
	
	

//	public List<PeerItem> orderPeers(Collection<PeerItem> peers, int maxPeers)
//		throws BassException;

//	public PeerItem getGoodPeer(Collection<PeerItem> peers)
//		throws BassException;

	// may return null if the logic does not need an UI tab
	public IView getUITab(BassUI bassUI);
}
