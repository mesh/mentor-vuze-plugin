/*
 * Created on 21-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.core.adapters;

import java.lang.reflect.Field;
import java.util.LinkedList;
import org.gudy.azureus2.core3.util.AEMonitor;
import com.aelitis.azureus.core.peermanager.peerdb.*;
import com.aelitis.azureus.core.peermanager.peerdb.PeerExchangerItem.Helper;

/**
 * Wraps an instance of Vuze PeerDatabase, in order to intercept invocations of
 * PeerDatabase methods (most notably, of getNextOptimisticConnectPeer()
 * method); it also provides access to private fields of the wrapped object
 * ("discovered_peers", "map_mon", "cached_peer_popularities" 
 * and "popularity_pos" fields).
 */
public class PeerDatabaseWrapper extends PeerDatabase {

	@SuppressWarnings("rawtypes")
	private static Class	CLASS_PeerDatabase;
	private static Field	FIELD_PeerDatabase_discovered_peers;
	private static Field	FIELD_PeerDatabase_map_mon;
	private static Field	FIELD_PeerDatabase_cached_peer_popularities;
	private static Field	FIELD_PeerDatabase_popularity_pos;
//	private static Field	FIELD_PeerDatabase_last_rebuild_time;
	static {
		try {
			CLASS_PeerDatabase = Class
				.forName("com.aelitis.azureus.core.peermanager.peerdb.PeerDatabase");
			FIELD_PeerDatabase_discovered_peers = CLASS_PeerDatabase
				.getDeclaredField("discovered_peers");
			FIELD_PeerDatabase_map_mon = CLASS_PeerDatabase
				.getDeclaredField("map_mon");
			FIELD_PeerDatabase_cached_peer_popularities = CLASS_PeerDatabase
				.getDeclaredField("cached_peer_popularities");
			FIELD_PeerDatabase_popularity_pos = CLASS_PeerDatabase
				.getDeclaredField("popularity_pos");
//			FIELD_PeerDatabase_last_rebuild_time = CLASS_PeerDatabase
//				.getDeclaredField("last_rebuild_time");
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to locate required data through reflection", e);
		}
		FIELD_PeerDatabase_discovered_peers.setAccessible(true);
		FIELD_PeerDatabase_map_mon.setAccessible(true);
		FIELD_PeerDatabase_cached_peer_popularities.setAccessible(true);
		FIELD_PeerDatabase_popularity_pos.setAccessible(true);
//		FIELD_PeerDatabase_last_rebuild_time.setAccessible(true);
	}
	// delegate has to be set, in order to prevent nullpointer
	protected PeerDatabase	delegate_;

	/**
	 * Returns the wrapped PeerDatabase object.
	 * 
	 * @return the wrapped PeerDatabase object, or null if it has not been set
	 *         yet.
	 */
	public PeerDatabase getDelegate() {
		return delegate_;
	}

	/**
	 * Set PeerDatabase object to be wrapped.
	 * 
	 * @param delegate
	 *            the PeerDatabase object to be wrapped.
	 */
	public void setDelegate(PeerDatabase delegate) {
		delegate_ = delegate;
	}

	//************************************************************************//
	// Adapter methods to help accessing the private fields                   //
	//************************************************************************//

	// unsafe: use monitor
	@SuppressWarnings("unchecked")
	protected LinkedList<PeerItem> getDiscoveredPeersField() {
		try {
			return (LinkedList<PeerItem>) FIELD_PeerDatabase_discovered_peers
				.get(delegate_);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}

	protected AEMonitor getMapMonField() {
		try {
			return (AEMonitor) FIELD_PeerDatabase_map_mon.get(delegate_);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}

	protected PeerItem[] getCachedPeerPopularitiesField() {
		try {
			return (PeerItem[]) FIELD_PeerDatabase_cached_peer_popularities.get(delegate_);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}

	protected void setCachedPeerPopularitiesField(PeerItem[] cachedPeerPopularities) {
		try {
			FIELD_PeerDatabase_cached_peer_popularities.set(delegate_, cachedPeerPopularities);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}
	
	protected int getPopularityPosField() {
		try {
			return (Integer) FIELD_PeerDatabase_popularity_pos.get(delegate_);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}

	protected void setPopularityPosField(int popularityPos) {
		try {
			FIELD_PeerDatabase_popularity_pos.set(delegate_, popularityPos);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}
//
//	protected long getLastRebuildTimeField() {
//		try {
//			return (Long) FIELD_PeerDatabase_last_rebuild_time.get(delegate_);
//		} catch (Exception e) {
//			throw new RuntimeException(
//				"Failed to access required data through reflection", e);
//		}
//	}
//
//	protected void setLastRebuildTimeField(long lastRebuildTime) {
//		try {
//			FIELD_PeerDatabase_last_rebuild_time.set(delegate_, lastRebuildTime);
//		} catch (Exception e) {
//			throw new RuntimeException(
//				"Failed to access required data through reflection", e);
//		}
//	}
	
	// ************************************************************************//
	// Delegates all other methods                                            //
	//************************************************************************//
	public void addDiscoveredPeer(PeerItem peer) {
		delegate_.addDiscoveredPeer(peer);
	}

	public boolean equals(Object obj) {
		return delegate_.equals(obj);
	}

	public int getDiscoveredPeerCount() {
		return delegate_.getDiscoveredPeerCount();
	}

	public PeerItem[] getDiscoveredPeers() {
		return delegate_.getDiscoveredPeers();
	}

	public PeerItem[] getDiscoveredPeers(String address) {
		return delegate_.getDiscoveredPeers(address);
	}

	public PeerItem getNextOptimisticConnectPeer() {
		return delegate_.getNextOptimisticConnectPeer();
	}

	public PeerItem getSelfPeer() {
		return delegate_.getSelfPeer();
	}

	public String getString() {
		return delegate_.getString();
	}

	public int hashCode() {
		return delegate_.hashCode();
	}

	public PeerExchangerItem registerPeerConnection(PeerItem base_peer_item,
													Helper helper) {
		return delegate_.registerPeerConnection(base_peer_item, helper);
	}

	public void seedStatusChanged(PeerExchangerItem item) {
		delegate_.seedStatusChanged(item);
	}

	public void setSelfPeer(PeerItem self) {
		delegate_.setSelfPeer(self);
	}

	public String toString() {
		return delegate_.toString();
	}
}
