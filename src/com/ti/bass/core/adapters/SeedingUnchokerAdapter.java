/*
 * Created on 21-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.core.adapters;

import java.lang.reflect.Field;
import java.util.*;
import com.aelitis.azureus.core.peermanager.unchoker.SeedingUnchoker;

/**
 * Adapter class that extends Vuze SeedingUnchoker, providing access to
 * private fields ("chokes" and "unchokes" fields).
 */
public class SeedingUnchokerAdapter extends SeedingUnchoker {

	@SuppressWarnings("rawtypes")
	private static Class	CLASS_SeedingUnchoker;
	private static Field	FIELD_SeedingUnchoker_chokes;
	private static Field	FIELD_SeedingUnchoker_unchokes;
	static {
		try {
			CLASS_SeedingUnchoker = Class
				.forName("com.aelitis.azureus.core.peermanager.unchoker.SeedingUnchoker");
			FIELD_SeedingUnchoker_chokes = CLASS_SeedingUnchoker
				.getDeclaredField("chokes");
			FIELD_SeedingUnchoker_unchokes = CLASS_SeedingUnchoker
				.getDeclaredField("unchokes");
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to locate required data through reflection", e);
		}
		FIELD_SeedingUnchoker_chokes.setAccessible(true);
		FIELD_SeedingUnchoker_unchokes.setAccessible(true);
	}

	//************************************************************************//
	// Adapter methods to help accessing the private fields                   //
	//************************************************************************//
	@SuppressWarnings("rawtypes")
	protected ArrayList getChokesField() {
		try {
			return (ArrayList) FIELD_SeedingUnchoker_chokes.get(this);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}

	@SuppressWarnings("rawtypes")
	protected ArrayList getUnchokesField() {
		try {
			return (ArrayList) FIELD_SeedingUnchoker_unchokes.get(this);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}

	@SuppressWarnings("rawtypes")
	protected void setChokesField(ArrayList chokes) {
		try {
			FIELD_SeedingUnchoker_chokes.set(this, chokes);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}

	@SuppressWarnings("rawtypes")
	protected void setUnchokesField(ArrayList unchokes) {
		try {
			FIELD_SeedingUnchoker_unchokes.set(this, unchokes);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}
}
