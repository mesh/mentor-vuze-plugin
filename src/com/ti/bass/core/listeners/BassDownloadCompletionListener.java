
 // TODO: mesh modification

package com.ti.bass.core.listeners;

import java.text.SimpleDateFormat;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

import org.gudy.azureus2.core3.peer.impl.control.PEPeerControlImpl;
import org.gudy.azureus2.plugins.download.Download;
import org.gudy.azureus2.plugins.download.DownloadCompletionListener;
import org.gudy.azureus2.plugins.peers.PeerManager;
import com.aelitis.azureus.core.peermanager.unchoker.Unchoker;
import com.ti.bass.core.*;

public class BassDownloadCompletionListener implements DownloadCompletionListener {

	@SuppressWarnings("rawtypes")
	private static Class				CLASS_PeerManagerImpl;
	private static Field				FIELD_PeerManagerImpl_manager;
	private static Field				FIELD_PEPeerControlImpl_peer_database;
	@SuppressWarnings("rawtypes")
	private static Class				CLASS_PEPeerControlImpl;
	private static Field				FIELD_PEPeerControlImpl_unchoker;
	@SuppressWarnings("rawtypes")
	private static Class				CLASS_DownloadImpl;
	private static Field				FIELD_DownloadImpl_download_manager;

	private BassPlugin					plugin_;
	
	
	static {
		try {
			CLASS_PeerManagerImpl = Class
				.forName("org.gudy.azureus2.pluginsimpl.local.peers.PeerManagerImpl");
			FIELD_PeerManagerImpl_manager = CLASS_PeerManagerImpl
				.getDeclaredField("manager");
			CLASS_PEPeerControlImpl = Class
				.forName("org.gudy.azureus2.core3.peer.impl.control.PEPeerControlImpl");
			FIELD_PEPeerControlImpl_unchoker = CLASS_PEPeerControlImpl
				.getDeclaredField("unchoker");
			FIELD_PEPeerControlImpl_peer_database = CLASS_PEPeerControlImpl
				.getDeclaredField("peer_database");
			CLASS_DownloadImpl = Class
				.forName("org.gudy.azureus2.pluginsimpl.local.download.DownloadImpl");
			FIELD_DownloadImpl_download_manager = CLASS_DownloadImpl
				.getDeclaredField("download_manager");
			
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to locate required data through reflection", e);
		}
		FIELD_PeerManagerImpl_manager.setAccessible(true);
		FIELD_PEPeerControlImpl_unchoker.setAccessible(true);
		FIELD_PEPeerControlImpl_peer_database.setAccessible(true);
		FIELD_DownloadImpl_download_manager.setAccessible(true);
	}

	public BassDownloadCompletionListener(BassPlugin plugin) {
		plugin_ = plugin;
	}

	public void onCompletion(Download d)
	{
		// TODO: mesh modification
		System.out.println("Bass Download Completion Listener  -  onCompletion()");
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
		System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		
		// write file to indicate DL completion + timestamp
		File dlComplete = new File("dl_complete_" + simpleDateFormat.format(timestamp.getTime()));
		try {
			dlComplete.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BassSeedingUnchokeManager bassSeedingUnchokeManager = new BassSeedingUnchokeManager(plugin_);
		setUnchoker(d.getPeerManager(), bassSeedingUnchokeManager);
	}
	
	private void setUnchoker(PeerManager pm, Unchoker unchoker)
	{
		PEPeerControlImpl pci;
		try {
			pci = (PEPeerControlImpl) FIELD_PeerManagerImpl_manager.get(pm);
			FIELD_PEPeerControlImpl_unchoker.set(pci, unchoker);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}
}
