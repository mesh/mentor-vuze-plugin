/*
 * Created on 21-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.core.listeners;

import java.text.SimpleDateFormat; // TODO: mesh modification

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

import org.gudy.azureus2.core3.peer.impl.control.PEPeerControlImpl;
import org.gudy.azureus2.core3.torrent.TOTorrent;
import org.gudy.azureus2.core3.torrent.TOTorrentAnnounceURLSet;
import org.gudy.azureus2.core3.tracker.client.*;
import org.gudy.azureus2.core3.tracker.client.impl.*;
import org.gudy.azureus2.core3.tracker.client.impl.bt.TRTrackerBTAnnouncerImpl;
import org.gudy.azureus2.plugins.download.Download;
import org.gudy.azureus2.plugins.download.DownloadPeerListener;
import org.gudy.azureus2.plugins.peers.PeerManager;
import org.gudy.azureus2.ui.swt.views.IView;
import org.gudy.azureus2.core3.download.DownloadManager;
import com.aelitis.azureus.core.peermanager.peerdb.PeerDatabase;
import com.aelitis.azureus.core.peermanager.unchoker.Unchoker;
import com.aelitis.azureus.core.util.CopyOnWriteList;
import com.ti.bass.core.*;
import com.ti.bass.core.adapters.*;
import com.ti.bass.dummy.DummyStatsPrinter;
import com.ti.bass.stats.*;

/**
 * Implementation of Vuze DownloadPeerListener: this is the key hook of the Bass
 * plugin into Vuze code. When a new PeerManager is added, this class overwrites
 * its DownloadingUnchoker with a new one (a DummyUnchoker in current
 * implementation) and wraps its PeerDatabase with a PeerDatabaseWrapper (a
 * DummyPeerDatabaseWrapper in current implementation). Moreover, it adds a
 * TRTrackerAnnouncerFactoryListener to intercept the creation of BT Tracker
 * clients, substituting them with our wrapper
 * (TRTrackerBTAnnouncerImplWrapper). It also creates a StatsProducer and a
 * StatsConsumer, to collect statistics. The method
 * StatsProducer.calculateStatistics() is recalled using a period of 1 second.
 */

public class BassDownloadPeerListener implements DownloadPeerListener {

	@SuppressWarnings("rawtypes")
	private static Class				CLASS_PeerManagerImpl;
	private static Field				FIELD_PeerManagerImpl_manager;
	private static Field				FIELD_PEPeerControlImpl_peer_database;
	@SuppressWarnings("rawtypes")
	private static Class				CLASS_PEPeerControlImpl;
	private static Field				FIELD_PEPeerControlImpl_unchoker;
	@SuppressWarnings("rawtypes")
	private static Class				CLASS_DownloadImpl;
	private static Field				FIELD_DownloadImpl_download_manager;
	// Added by Fabrizio
	@SuppressWarnings("rawtypes")
	private static Class				CLASS_TRTrackerAnnouncerMuxer;
	private static Field				FIELD_TRTrackerAnnouncerMuxer_announcers;
	private static Field				FIELD_TRTrackerAnnouncerMuxer_activated;
	@SuppressWarnings("rawtypes")
	private static Class				CLASS_TRTrackerBTAnnouncerImpl;
	private static Field				FIELD_TRTrackerBTAnnouncerImpl_torrent;
	private static Field				FIELD_TRTrackerBTAnnouncerImpl_announce_urls;
	private static Field				FIELD_TRTrackerBTAnnouncerImpl_peer_networks;
	private static Field				FIELD_TRTrackerBTAnnouncerImpl_manual_control;
	private static Field				FIELD_TRTrackerBTAnnouncerImpl_helper;
	private static final long			SCHEDULER_PERIOD	= 1000L;
	private BassPlugin					plugin_;
	private Timer						scheduler_;
	private Map<PeerManager, Runnable>	tasks_;
	private List<DownloadManager>		downloadManagers_;
	
	
	static {
		try {
			CLASS_PeerManagerImpl = Class
				.forName("org.gudy.azureus2.pluginsimpl.local.peers.PeerManagerImpl");
			FIELD_PeerManagerImpl_manager = CLASS_PeerManagerImpl
				.getDeclaredField("manager");
			CLASS_PEPeerControlImpl = Class
				.forName("org.gudy.azureus2.core3.peer.impl.control.PEPeerControlImpl");
			FIELD_PEPeerControlImpl_unchoker = CLASS_PEPeerControlImpl
				.getDeclaredField("unchoker");
			FIELD_PEPeerControlImpl_peer_database = CLASS_PEPeerControlImpl
				.getDeclaredField("peer_database");
			CLASS_DownloadImpl = Class
				.forName("org.gudy.azureus2.pluginsimpl.local.download.DownloadImpl");
			FIELD_DownloadImpl_download_manager = CLASS_DownloadImpl
				.getDeclaredField("download_manager");
			// Added by Fabrizio
			CLASS_TRTrackerAnnouncerMuxer = Class
				.forName("org.gudy.azureus2.core3.tracker.client.impl.TRTrackerAnnouncerMuxer");
			FIELD_TRTrackerAnnouncerMuxer_announcers = CLASS_TRTrackerAnnouncerMuxer
				.getDeclaredField("announcers");
			FIELD_TRTrackerAnnouncerMuxer_activated = CLASS_TRTrackerAnnouncerMuxer
				.getDeclaredField("activated");
			CLASS_TRTrackerBTAnnouncerImpl = Class
				.forName("org.gudy.azureus2.core3.tracker.client.impl.bt.TRTrackerBTAnnouncerImpl");
			FIELD_TRTrackerBTAnnouncerImpl_torrent = CLASS_TRTrackerBTAnnouncerImpl
				.getDeclaredField("torrent");
			FIELD_TRTrackerBTAnnouncerImpl_announce_urls = CLASS_TRTrackerBTAnnouncerImpl
				.getDeclaredField("announce_urls");
			FIELD_TRTrackerBTAnnouncerImpl_peer_networks = CLASS_TRTrackerBTAnnouncerImpl
				.getDeclaredField("peer_networks");
			FIELD_TRTrackerBTAnnouncerImpl_manual_control = CLASS_TRTrackerBTAnnouncerImpl
				.getDeclaredField("manual_control");
			FIELD_TRTrackerBTAnnouncerImpl_helper = CLASS_TRTrackerBTAnnouncerImpl
				.getDeclaredField("helper");
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to locate required data through reflection", e);
		}
		FIELD_PeerManagerImpl_manager.setAccessible(true);
		FIELD_PEPeerControlImpl_unchoker.setAccessible(true);
		FIELD_PEPeerControlImpl_peer_database.setAccessible(true);
		FIELD_DownloadImpl_download_manager.setAccessible(true);
		// Added by Fabrizio
		FIELD_TRTrackerAnnouncerMuxer_announcers.setAccessible(true);
		FIELD_TRTrackerAnnouncerMuxer_activated.setAccessible(true);
		FIELD_TRTrackerBTAnnouncerImpl_torrent.setAccessible(true);
		FIELD_TRTrackerBTAnnouncerImpl_announce_urls.setAccessible(true);
		FIELD_TRTrackerBTAnnouncerImpl_peer_networks.setAccessible(true);
		FIELD_TRTrackerBTAnnouncerImpl_manual_control.setAccessible(true);
		FIELD_TRTrackerBTAnnouncerImpl_helper.setAccessible(true);
	}

	public BassDownloadPeerListener(BassPlugin plugin) {
		plugin_ = plugin;
		tasks_ = new HashMap<PeerManager, Runnable>();
		downloadManagers_ = new LinkedList<DownloadManager>();
		scheduler_ = new Timer();
		scheduler_.schedule(new TimerTask() {

			@Override
			public void run() {
				synchronized (tasks_) {
					// run all the scheduled tasks
					for (Runnable task : tasks_.values())
						task.run();
				}
			}
		}, 0, SCHEDULER_PERIOD);
		TRTrackerAnnouncerFactory
			.addListener(new TRTrackerAnnouncerFactoryListener() {

				public void clientDestroyed(TRTrackerAnnouncer client) {
				}

				@SuppressWarnings("unchecked")
				public void clientCreated(TRTrackerAnnouncer client) {
					if (client instanceof TRTrackerAnnouncerMuxer) {
						CopyOnWriteList<TRTrackerAnnouncerHelper> announcers;
						CopyOnWriteList<TRTrackerAnnouncerHelper> announcers_copy = new CopyOnWriteList<TRTrackerAnnouncerHelper>();
						Set<TRTrackerAnnouncerHelper> activated;
						Set<TRTrackerAnnouncerHelper> activated_copy = new HashSet<TRTrackerAnnouncerHelper>();
						TOTorrent torrent;
						TOTorrentAnnounceURLSet[] announce_urls;
						String[] peer_networks;
						boolean manual_control;
						TRTrackerAnnouncerImpl.Helper helper;
						try {
							announcers = (CopyOnWriteList<TRTrackerAnnouncerHelper>) FIELD_TRTrackerAnnouncerMuxer_announcers
								.get(client);
							activated = (Set<TRTrackerAnnouncerHelper>) FIELD_TRTrackerAnnouncerMuxer_activated
								.get(client);
						} catch (Exception e) {
							throw new RuntimeException(
								"Failed to access required data through reflection",
								e);
						}
						for (TRTrackerAnnouncerHelper ta : announcers) {
							if (ta instanceof TRTrackerBTAnnouncerImpl) {
								try {
									torrent = (TOTorrent) FIELD_TRTrackerBTAnnouncerImpl_torrent
										.get(ta);
									announce_urls = (TOTorrentAnnounceURLSet[]) FIELD_TRTrackerBTAnnouncerImpl_announce_urls
										.get(ta);
									peer_networks = (String[]) FIELD_TRTrackerBTAnnouncerImpl_peer_networks
										.get(ta);
									manual_control = (Boolean) FIELD_TRTrackerBTAnnouncerImpl_manual_control
										.get(ta);
									helper = (TRTrackerAnnouncerImpl.Helper) FIELD_TRTrackerBTAnnouncerImpl_helper
										.get(ta);
									TRTrackerAnnouncerHelper wrapper = new TRTrackerBTAnnouncerImplWrapper(
										torrent, announce_urls, peer_networks,
										manual_control, helper);
									announcers_copy.add(wrapper);
									if (activated.contains(ta))
										activated_copy.add(wrapper);
								} catch (Exception e) {
									throw new RuntimeException(
										"Failed to access required data through reflection",
										e);
								}
							} else {
								announcers_copy.add(ta);
								if (activated.contains(ta))
									activated_copy.add(ta);
							}
						}
						try {
							FIELD_TRTrackerAnnouncerMuxer_announcers.set(
								client, announcers_copy);
							FIELD_TRTrackerAnnouncerMuxer_activated.set(client,
								activated_copy);
						} catch (Exception e) {
							throw new RuntimeException(
								"Failed to access required data through reflection",
								e);
						}
					}
				}
			});
	}

	public void peerManagerAdded(final Download download,
									PeerManager peerManager)
	{
		// TODO: mesh modification
		System.out.println("Bass Download Peer Listener  -  peerManagerAdded()");
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
		System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		
		BassUnchokeManager bassUnchokeManager;
		BassSeedingUnchokeManager bassSeedingUnchokeManager; // TODO: mesh modification
		BassConnectionManager bassConnectionManager;
		
		// DownloadManager downloadManager;
		// DownloadManager[] managers;
		final StatsProducer statsCalc;
		StatsListener statsListener;
		plugin_.getLogger().log(
			"PeerManager added for " + download.getTorrent().getName());
		
		bassUnchokeManager = new BassUnchokeManager(plugin_);
		bassSeedingUnchokeManager = new BassSeedingUnchokeManager(plugin_); // TODO: mesh modification
		bassConnectionManager = new BassConnectionManager(plugin_);
		
		// setBTTrackerAnnouncer(getDownloadManager(download));
		if (plugin_.areStatsEnabled()) {
			statsCalc = new StatsProducerImpl(peerManager, plugin_);
			statsListener = new DummyStatsPrinter(peerManager, plugin_);
			statsCalc.addStatsListener(statsListener);
			plugin_.getLogger().log("Stats consumer added");
			synchronized (tasks_) {
				tasks_.put(peerManager, new Runnable() {

					public void run() {
						statsCalc.updateStats();
					}
				});
			}
		}
		
		// TODO: here BassUnchokeManager is registered
		// TODO: mesh modification
		// here we need to check whether we are leecher or seeder and then register the required unchoker
		if(peerManager.isSeeding())
		{
			// seeder -> use SeedingUnchokerAdapter Impl
			
			System.out.println("Bass Download Peer Listener  -  peerManagerAdded()");
			System.out.println("register seeding unchoker");
			timestamp = new Date();
			simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
			System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
			
			// write file to indicate UL start + timestamp
			File ulBegin = new File("ul_begin_" + simpleDateFormat.format(timestamp.getTime()));
			try {
				ulBegin.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			setUnchoker(peerManager, bassSeedingUnchokeManager);
		}
		else {
			// leecher -> use the DownloadingUnchokerAdapter Impl, initially provided by BASS plugin
			
			System.out.println("Bass Download Peer Listener  -  peerManagerAdded()");
			System.out.println("register downloading unchoker");
			timestamp = new Date();
			simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
			System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
			
			// write file to indicate DL start + timestamp
			File dlBegin = new File("dl_begin_" + simpleDateFormat.format(timestamp.getTime()));
			try {
				dlBegin.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			setUnchoker(peerManager, bassUnchokeManager);
		}
		
		setPeerDatabase(peerManager, bassConnectionManager); // TODO: here BassConnectionManager is registered
		
		synchronized (downloadManagers_) {
			downloadManagers_.add(getDownloadManager(download));
			
			try
			{
				for (IView tab : plugin_.getBassUI().getTabs())
					tab.dataSourceChanged(downloadManagers_
						.toArray(new DownloadManager[downloadManagers_.size()]));
				
			} catch (Exception e) {
				// TODO: in headless mode a NullPointerException is thrown here
			}
		}
	}

	public void peerManagerRemoved(Download download, PeerManager peerManager)
	{
		// TODO: mesh modification
		System.out.println("Bass Download Peer Listener  -  peerManagerRemoved()");
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
		System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		
		// DownloadManager downloadManager;
		plugin_.getLogger().log(
			"PeerManager removed for " + download.getTorrent().getName());
		synchronized (tasks_) {
			tasks_.remove(peerManager);
		}
		
		synchronized (downloadManagers_) {
			downloadManagers_.remove(getDownloadManager(download));
			
			try
			{
				for (IView tab : plugin_.getBassUI().getTabs())
					tab.dataSourceChanged(downloadManagers_
						.toArray(new DownloadManager[downloadManagers_.size()]));
				
			} catch (Exception e) {
				// TODO: in headless mode a NullPointerException is thrown here
			}
		}
		
		// downloadManager = getDownloadManager(download);
		// for (IView tab : plugin_.getBassUI().getTabs()) {
		// // if (tab instanceof BassPeersGraphicView &&
		// // ((BassPeersGraphicView)tab).getDownloadManager() ==
		// downloadManager) {
		// // plugin_.getBassUI().removeTab(tab.getFullTitle());
		// // break;
		// // }
		// tab.dataSourceChanged(downloadManager);
		// }
	}

	private void setUnchoker(PeerManager pm, Unchoker unchoker) {
		PEPeerControlImpl pci;
		try {
			pci = (PEPeerControlImpl) FIELD_PeerManagerImpl_manager.get(pm);
			FIELD_PEPeerControlImpl_unchoker.set(pci, unchoker);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}
	
	private void setPeerDatabase(PeerManager pm,
									PeerDatabaseWrapper peerDatabaseWrapper) {
		PEPeerControlImpl pci;
		PeerDatabase currentDatabase;
		try {
			pci = (PEPeerControlImpl) FIELD_PeerManagerImpl_manager.get(pm);
			currentDatabase = (PeerDatabase) FIELD_PEPeerControlImpl_peer_database
				.get(pci);
			peerDatabaseWrapper.setDelegate(currentDatabase);
			FIELD_PEPeerControlImpl_peer_database.set(pci, peerDatabaseWrapper);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}

	private DownloadManager getDownloadManager(Download download) {
		try {
			return (DownloadManager) FIELD_DownloadImpl_download_manager
				.get(download);
		} catch (Exception e) {
			throw new RuntimeException(
				"Failed to access required data through reflection", e);
		}
	}
}
