
package com.ti.bass.core;

import java.text.SimpleDateFormat; // TODO: mesh modification

import mesh.ConfFileParams; // TODO: mesh modification

import java.util.*;

import org.gudy.azureus2.core3.peer.PEPeer;
import org.gudy.azureus2.core3.peer.impl.PEPeerTransport;

import com.aelitis.azureus.core.peermanager.peerdb.PeerItem;
import com.aelitis.azureus.core.peermanager.unchoker.UnchokerUtil;
import com.ti.bass.core.adapters.SeedingUnchokerAdapter;
import com.ti.bass.core.exception.BassException;
import com.ti.bass.core.peerselection.PeerSelectionService.PeerCost;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class BassSeedingUnchokeManager extends SeedingUnchokerAdapter {

	private BassPlugin		plugin_;
	private Random			random_; // TODO: mesh modification
	private ConfFileParams config_; // TODO: mesh modification
	
	public BassSeedingUnchokeManager(BassPlugin plugin) {
		plugin_ = plugin;
		random_ = new Random(); // TODO: mesh modification
		config_ = ConfFileParams.getInstance(); // TODO: mesh modification
	}
	
	
	@Override
	public ArrayList<PEPeer> getImmediateUnchokes(int max_to_unchoke,
			ArrayList<PEPeer> all_peers)
	{
		// TODO: mesh modification
		System.out.println("Bass Seeding Unchoke Manager	-	getImmediateUnchokes()");
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
		System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		
		// TODO: mesh modification
		System.out.println("getImmediateUnchokes()	-	allPeers:");
		for (Object entry : all_peers) {
			PEPeerTransport p = (PEPeerTransport) entry;
			System.out.println(p.getIp());
		}
		
		// TODO: mesh modification
		max_to_unchoke = config_.getNODE_UPLOAD_COUNT();
		ArrayList to_unchoke = new ArrayList();
		
		int peer_count = all_peers.size();
		if (max_to_unchoke > peer_count) {
			max_to_unchoke = peer_count;
		}
		
		// count all the currently unchoked peers
		int num_unchoked = 0;
		for (int i = 0; i < all_peers.size(); i++) {
			PEPeer peer = all_peers.get(i);
			if (!peer.isChokedByMe())
				num_unchoked++;
		}
		
		// if not enough unchokes
		int needed = max_to_unchoke - num_unchoked;
		
		// TODO: mesh modification
		if (needed > 0)
		{
			for (int i = 0; i < needed; i++)
			{
				// Bass code hooks here
				PEPeer peer = getNextOptimisticPeer(all_peers);
				// end of Bass specific code
				
				if (peer == null)
					break; // no more new unchokes avail
				
				if (!to_unchoke.contains(peer))
				{
					to_unchoke.add(peer);
					peer.setOptimisticUnchoke(true);
				}
			}
		}
		
		System.out.println("immediate unchokes:");
		for (Object entry : to_unchoke)
		{
			System.out.println(((PEPeerTransport)entry).getIp());
		}
		
		return to_unchoke;
	}
	
	
	@Override
	public void calculateUnchokes(int max_to_unchoke,
			ArrayList<PEPeer> all_peers, boolean force_refresh,
			boolean check_priority_connections)
	{
		
		// TODO: mesh modification
		System.out.println("Bass Seeding Unchoke Manager	-	calculateUnchokes()");
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
		System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		
		// TODO: mesh modification
		ArrayList<PEPeer> chokes = getChokesField();
		ArrayList<PEPeer> unchokes = getUnchokesField();
		ArrayList<PEPeer> optimisticUnchokes = new ArrayList<PEPeer>();
		ArrayList<PEPeer> bestPeers = new ArrayList<PEPeer>();
		max_to_unchoke = config_.getNODE_UPLOAD_COUNT(); // number of upload slots (default 5 = 4 normal + 1 optimistic)
		
		
		// TODO: mesh modification
		int max_optimistic;
		if (config_.getALLOW_OPTIMISTIC()) {
			max_optimistic = ((max_to_unchoke - 1) / 5) + 1; // one optimistic unchoke for every 5 upload slots
		} else {
			max_optimistic = 0;
		}
		
		
		// TODO: mesh modification
		System.out.println("calculateUnchokes()	-	allPeers:");
		for (PEPeer entry : all_peers) {
			System.out.println(entry.getIp());
		}
		
		
		// TODO: mesh modification
		// is VANILLA -> use unmodified choking
		if (config_.getVANILLA())
		{
			System.out.println("Using VANILLA mode ...");	
			
			// following is original SeedingUnchoker code with some debug output ...
			
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp());
			}
			
			
			// get all the currently unchoked peers
			for (int i = 0; i < all_peers.size(); i++)
			{
				PEPeer peer = all_peers.get(i);
				
				if (!peer.isChokedByMe())
				{
					// TODO: mesh modification
					//if (UnchokerUtil.isUnchokable(peer, false))
					if (UnchokerUtil.isUnchokable(peer, config_.getALLOW_SNUBBED()))
					{
						unchokes.add(peer);
						
						// TODO: mesh modification
						if (peer.isOptimisticUnchoke())
						{
							optimisticUnchokes.add(peer);
						}
						
					} else { // should be immediately choked
						chokes.add(peer);
					}
				}
			}

			// if too many unchokes
			while (unchokes.size() > max_to_unchoke) {
				chokes.add(unchokes.remove(unchokes.size() - 1));
			}
			
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp());
			}
			
			
			// we only recalculate the uploads when we're forced to refresh the optimistic unchokes
			if (force_refresh)
			{
				// TODO: mesh modification
				System.out.println("30 s refresh -> new uploads & optimistics are chosen");
				
				// we need to make room for new opt unchokes by finding the "worst" peers
				ArrayList<PEPeer> peers_ordered_by_rate = new ArrayList<PEPeer>();
				ArrayList<PEPeer> peers_ordered_by_uploaded = new ArrayList<PEPeer>();

				long[] rates = new long[unchokes.size()]; // 0-initialized
				long[] uploaded = new long[rates.length]; // 0-initialized
				
				// TODO: mesh modification
				System.out.println("Sorting peers after UL rate ...");
				
				// calculate factor rankings
				for (int i = 0; i < unchokes.size(); i++)
				{
					PEPeer peer = unchokes.get(i);
					
					// TODO: mesh modification
					System.out.println("IP: "+peer.getIp());
					
					long rate = peer.getStats().getDataSendRate();
					if (rate > 256) // filter out really slow peers
					{
						// TODO: mesh modification
						System.out.println("input for updateLargestValueFirstSort(): " +
								peer.getIp()+", ULRate: "+rate+", ULBytes: "+peer.getStats().getTotalDataBytesSent());
						
						// calculate reverse order by our upload rate to them
						UnchokerUtil.updateLargestValueFirstSort(
								rate, rates, peer, peers_ordered_by_rate, 0);
						
						// calculate order by the total number of bytes we've uploaded to them
						UnchokerUtil.updateLargestValueFirstSort(
								peer.getStats().getTotalDataBytesSent(), uploaded,
								peer, peers_ordered_by_uploaded, 0);
					}
				}

				Collections.reverse(peers_ordered_by_rate); // we want higher rates at the end

				ArrayList<PEPeer> peers_ordered_by_rank = new ArrayList<PEPeer>();
				long[] ranks = new long[peers_ordered_by_rate.size()];
				Arrays.fill(ranks, Long.MIN_VALUE);
				
				// TODO: mesh modification
				System.out.println("Calculating rank factor using UL rate and bytes ...");
				
				// combine factor rankings to get best
				for (int i = 0; i < unchokes.size(); i++)
				{
					PEPeer peer = unchokes.get(i);
					
					// TODO: mesh modification
					System.out.println("IP: "+peer.getIp());
					
					// "better" peers have high indexes (toward the end of each list)
					long rate_factor = peers_ordered_by_rate.indexOf(peer);
					long uploaded_factor = peers_ordered_by_uploaded.indexOf(peer);

					if (rate_factor == -1)
						continue; // wasn't downloading fast enough,
								   // skip add so it will be choked automatically

					long rank_factor = rate_factor + uploaded_factor;
					
					// TODO: mesh modification
					System.out.println("input for updateLargestValueFirstSort(): " +
							peer.getIp()+", Rank: " + rank_factor);
					
					UnchokerUtil.updateLargestValueFirstSort(
							rank_factor, ranks, peer, peers_ordered_by_rank, 0);
				}
				
				// make space for new optimistic unchokes
				while (peers_ordered_by_rank.size() > max_to_unchoke - max_optimistic)
				{
					peers_ordered_by_rank.remove(peers_ordered_by_rank.size() - 1);
				}
				
				
				// TODO: mesh modification
				bestPeers = peers_ordered_by_rank;
				
				
				// TODO: mesh modification
				System.out.println("best peers:");
				for (PEPeer entry : bestPeers)
				{
					System.out.println(entry.getIp());
				}
				System.out.println("(last) optimistic unchokes:");
				for (PEPeer entry : optimisticUnchokes)
				{
					System.out.println(entry.getIp());
				}
				System.out.println("unchoked peers:");
				for (PEPeer entry : unchokes)
				{
					System.out.println(entry.getIp());
				}
				System.out.println("choked peers:");
				for (PEPeer entry : chokes)
				{
					System.out.println(entry.getIp());
				}
				
				
				// update choke list with drops and unchoke list with optimistic unchokes
				ArrayList<PEPeer> to_unchoke = new ArrayList<PEPeer>();
				for (Iterator<PEPeer> it = unchokes.iterator(); it.hasNext();)
				{
					PEPeer peer = it.next();

					peer.setOptimisticUnchoke(false);
					
					// TODO: mesh modification
					if (optimisticUnchokes.contains(peer))
						optimisticUnchokes.remove(peer);

					if (!peers_ordered_by_rank.contains(peer))
					{
						// should be choked
						// we assume that any/all chokes are to be replaced by optimistics
//						PEPeer optimistic_peer = UnchokerUtil.getNextOptimisticPeer( all_peers, false, false );
						
						// TODO: mesh modification
						PEPeer optimistic_peer = getNextOptimisticPeer(all_peers);
						
						if (optimistic_peer != null)
						{
							// only choke if we've got a peer to replace it with
							chokes.add(peer);
							it.remove();
							to_unchoke.add(optimistic_peer);
							optimistic_peer.setOptimisticUnchoke(true);
							
							// TODO: mesh modification
							if (!optimisticUnchokes.contains(optimistic_peer))
								optimisticUnchokes.add(optimistic_peer);
							
							// TODO: mesh modification
							System.out.println("added new optimistic: "+optimistic_peer.getIp());
						}
					}
				}
				
				for (int i = 0; i < to_unchoke.size(); i++) {
					unchokes.add(to_unchoke.get(i));
				}
			}
			
			
			// TODO: will never be true in our tests
			if (check_priority_connections) {
				// add Friend peers preferentially, leaving room for 1 non-friend
				// peer for every 5 upload slots
				setBuddyUnchokes(max_to_unchoke - max_optimistic, all_peers);
			}
			
			
			// TODO: mesh modification
			System.out.println("Bass Seeding Unchoke Manager	-	calculateUnchokes()");
			timestamp = new Date();
			simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
			System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
			
			// TODO: mesh modification
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("unchoked peers:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp());
			}
			System.out.println("choked peers:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp());
			}
		}
		
		
		
		// TODO: mesh modification
		// is not VANILLA -> use modified choking for 802.11s mesh with ALM
		else if (!config_.getVANILLA())
		{
			System.out.println("Using ALM mesh mode ...");
			
			// - the first code part just gets the current mesh nodes' IPs and associated ALM values from this node's perspective
			// - the mesh nodes IP<->metric maps are further separated into 1-hop neighbors and nodes in multi-hop distance
			// - the created arrays are later used in the modified chokes/unchokes calculation
			
			boolean prefer1Hop = config_.getPREFER_ONEHOP_PEERS(); // prefer direct neighbors as possible downloaders
			
			// maps with IP <-> metric, ordered for 1-hop nodes and rest
			HashMap<String, Integer> sortedonehopmap =
					new LinkedHashMap<String, Integer>(plugin_.getPeerSelectionService().getsortedonehopmap()); // direct neighbors only
			
			HashMap<String, Integer> sortednhopmap =
					new LinkedHashMap<String, Integer>(plugin_.getPeerSelectionService().getsortednhopmap()); // all remaining nodes (Multi-Hop)
			
			// map with all nodes info (1-hop + multi-hop)
			Map<String, Integer> sortedAllNodesMap = new LinkedHashMap<String, Integer>();
			
			// ALMs in sorted 1hop/nhop map are stored as negated (were sorted with 'sortMapByValues')
			// 'sortMapByValues' expects positive input, sorts in ascending order, stores negated
			// this way, smallest ALM will still come first also after applying 'LargestValueFirstSort'
			for (Map.Entry<String, Integer>	entry :	sortedonehopmap.entrySet())
				sortedAllNodesMap.put(entry.getKey(), entry.getValue()*(-1));
			for (Map.Entry<String, Integer>	entry :	sortednhopmap.entrySet())
				sortedAllNodesMap.put(entry.getKey(), entry.getValue()*(-1));
			
			sortedAllNodesMap = plugin_.getPeerSelectionService().sortMapByValues(sortedAllNodesMap);
			
			System.out.println();
			System.out.println("sortedonehopmap:");
			for (Map.Entry<String, Integer> entry : sortedonehopmap.entrySet()) {
				System.out.println("IP: "+entry.getKey()+"\t"+"Metric: "+entry.getValue());
			}
			
			System.out.println();
			System.out.println("sortednhopmap:");
			for (Map.Entry<String, Integer> entry : sortednhopmap.entrySet()) {
				System.out.println("IP: "+entry.getKey()+"\t"+"Metric: "+entry.getValue());
			}
			
			System.out.println();
			System.out.println("sortedAllNodesMap:");
			for (Map.Entry<String, Integer> entry : sortedAllNodesMap.entrySet()) {
				System.out.println("IP: "+entry.getKey()+"\t"+"Metric: "+entry.getValue());
			}
			System.out.println();
			
			
			// TODO: modified chokes/unchokes calculation starts here:
			
			
			long[] bests = new long[max_to_unchoke]; // ensure we never pick more slots than allowed to unchoke
			Arrays.fill( bests, Long.MIN_VALUE ); // we want to store negative values (inverted ALM)
			int startPos;
			
			
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			
			// get all the currently unchoked peers
			for (int i = 0; i < all_peers.size(); i++)
			{
				PEPeer peer = all_peers.get(i);
				
				if (!peer.isChokedByMe())
				{
					if (UnchokerUtil.isUnchokable(peer, config_.getALLOW_SNUBBED()))
					{
						unchokes.add(peer);
						
						if (peer.isOptimisticUnchoke())
						{
							optimisticUnchokes.add(peer);
						}
						
					} else { // should be immediately choked
						chokes.add(peer);
					}
				}
			}
			
			
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			
			if (!force_refresh) { // ensure current optimistic unchokes remain unchoked
				for (int i = 0; i < optimisticUnchokes.size(); i++) {
					PEPeer peer = optimisticUnchokes.get(i);
					if (i < max_optimistic) {
						bestPeers.add(peer); // add them to the front of the "best" list
					} else { // too many optimistics
						peer.setOptimisticUnchoke(false);
					}
				}
			}
			
			
			System.out.println("looking for peers to add, following ALM ...");
			startPos = bestPeers.size();
			for (int i = 0; i < all_peers.size(); i++)
			{
				PEPeer peer = (PEPeerTransport) all_peers.get(i);
				String peerIP = peer.getIp();
				
				System.out.println("IP: "+peerIP);
				System.out.println("isUnchokable: "+UnchokerUtil.isUnchokable(peer, config_.getALLOW_SNUBBED()));
				System.out.println("alreadyInBest: "+bestPeers.contains(peer));
				
				if (UnchokerUtil.isUnchokable(peer, config_.getALLOW_SNUBBED())
					&& !bestPeers.contains(peer))
				{
					
					System.out.println("prefer1Hop: "+prefer1Hop);
					if (prefer1Hop) // prefer 1-hop neighbors
					{
						// check if peerIP is among 1-hop nodes
						if (!sortedonehopmap.keySet().contains(peerIP))
						{
							// peer is in multi-hop distance -> skip it for now
							continue;
						}
					}
					
					// TODO: here we use ALM instead of UL rate of interested peers!
					// we use our metric data structures created above
					// -> get metric for peerIP
					long metric = (long) sortedAllNodesMap.get(peerIP);
					System.out.println("input for 1st updateLargestValueFirstSort(): "+peerIP+", "+metric);
					UnchokerUtil.updateLargestValueFirstSort(metric, bests, peer, bestPeers, startPos);
				}
			}
			
			
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			
			// if we haven't yet picked enough slots
			// TODO: this can only occur when "prefer1Hop" is true (too few neighbors to fill UL slots in the first loop)
			if (bestPeers.size() < max_to_unchoke)
			{
				System.out.println("still slots avail, looking for peers to add, following ALM ...");
				
				startPos = bestPeers.size();
				
				// fill the remaining slots with peers in multi-hop distance, following ALM
				for (int i = 0; i < all_peers.size(); i++)
				{
					PEPeer peer = (PEPeerTransport) all_peers.get(i);
					String peerIP = peer.getIp();
					
					System.out.println("IP: "+peerIP);
					System.out.println("isUnchokable: "+UnchokerUtil.isUnchokable(peer, config_.getALLOW_SNUBBED()));
					System.out.println("alreadyInBest: "+bestPeers.contains(peer));
					
					if (UnchokerUtil.isUnchokable(peer, config_.getALLOW_SNUBBED())
						&& !bestPeers.contains(peer))
					{
						// TODO: here we just fill bestPeers list with more nodes following ALM metric!
						long metric = (long) sortedAllNodesMap.get(peerIP);
						System.out.println("input for 2nd updateLargestValueFirstSort(): "+peerIP+", "+metric);
						UnchokerUtil.updateLargestValueFirstSort(metric, bests, peer, bestPeers, startPos);
					}
				}
			}
			
			
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			
			if (force_refresh) {
				System.out.println("30 s refresh -> new optimistics are chosen");
				// make space for new optimistic unchokes
				while (bestPeers.size() > max_to_unchoke - max_optimistic) {
					bestPeers.remove(bestPeers.size() - 1);
				}
			}
			
			
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			
			// if we still have remaining slots
			// TODO: should not happen if enough interested peers
			while (bestPeers.size() < max_to_unchoke)
			{
				// Bass code hooks here
				PEPeer peer = getNextOptimisticPeer(all_peers); // TODO: if weights are set to 0, default vuze strategy is used here
				// end of Bass specific code
				
				if (peer == null)
					break; // no more new unchokes avail
				
				if (!bestPeers.contains(peer)) {
					bestPeers.add(peer);
					peer.setOptimisticUnchoke(true);
					System.out.println("added new optimistic: "+peer.getIp());
				}
				else
				{
					// we're here because the given optimistic peer is already
					// "best", but is choked still,
					// which means it will continually get picked by the
					// getNextOptimisticPeer() method,
					// and we'll loop forever if there are no other peers to choose from
					peer.sendUnChoke();
					// send unchoke immediately, so it won't get
					// picked optimistically anymore
					// allPeers.remove( peer ); 
					// remove from allPeers list, so it
					// won't get picked optimistically anymore //TODO
				}
			}
			
			
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			
			
			// update chokes
			for (Iterator it = unchokes.iterator(); it.hasNext();) {
				PEPeer peer = (PEPeerTransport) it.next();
				if (!bestPeers.contains(peer)) { // should be choked
					if (bestPeers.size() < max_to_unchoke) {
						// but there are still slots needed (no optimistics avail),
						// so don't bother choking them
						bestPeers.add(peer);
					} else {
						chokes.add(peer);
						it.remove();
					}
				}
			}
			// update unchokes
			for (int i = 0; i < bestPeers.size(); i++) {
				PEPeer peer = (PEPeerTransport) bestPeers.get(i);
				if (!unchokes.contains(peer)) {
					unchokes.add(peer);
				}
			}
			
			
			System.out.println("Bass Seeding Unchoke Manager	-	calculateUnchokes()");
			timestamp = new Date();
			simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
			System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
			
			System.out.println("best peers:");
			for (PEPeer entry : bestPeers)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("(last) optimistic unchokes:");
			for (PEPeer entry : optimisticUnchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("unchokes:");
			for (PEPeer entry : unchokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
			System.out.println("chokes:");
			for (PEPeer entry : chokes)
			{
				System.out.println(entry.getIp()+"\t"+sortedAllNodesMap.get(entry.getIp()));
			}
		}
		
		
		// TODO: mesh modification
		// apply calculated chokes / unchokes fields
		setUnchokesField(unchokes);
		setChokesField(chokes);
	}
	
	
	// TODO: will never be called in our tests (no priority connections)
	private void setBuddyUnchokes(int max_buddies, ArrayList<PEPeer> all_peers)
	{
		// TODO: mesh modification
		ArrayList<PEPeer> chokes = getChokesField();
		ArrayList<PEPeer> unchokes = getUnchokesField();
		
		
		if (unchokes.isEmpty())
			return; // don't bother trying to replace peers in an empty list

		ArrayList<PEPeer> buddies = new ArrayList<PEPeer>();

		// find all buddies
		for (int i = 0; i < all_peers.size(); i++)
		{
			PEPeer peer = all_peers.get(i);

			if (peer.isPriorityConnection()
				&& UnchokerUtil.isUnchokable(peer, true))
			{
				buddies.add(peer);
			}
		}

		// we want to give all connected friends an equal chance if there are
		// more than max_friends allowed
		Collections.shuffle(buddies);

		int num_unchoked = 0;

		int max = max_buddies > unchokes.size() ? unchokes.size() : max_buddies;

		while (num_unchoked < max && !buddies.isEmpty())
		{
			// go through unchoke list and replace non-buddy peers with buddy ones
			
			PEPeer peer = unchokes.remove(0); // pop peer from front of unchoke list
			
			if (buddies.remove(peer))
			{
				// peer is already in the buddy list so insert confirmed buddy peer
				// back into list at the end
				unchokes.add(peer);
				
			} else {
				// not a buddy, so replace
				PEPeer buddy = buddies.remove(buddies.size() - 1); // get next buddy
				chokes.remove(buddy); // just in case
				unchokes.add(buddy); // add buddy to back of list
			}

			num_unchoked++;
			
			
			// TODO: mesh modification
			// TODO: apply calculated chokes / unchokes fields
			setUnchokesField(unchokes);
			setChokesField(chokes);
		}
	}

	
	private PEPeerTransport getNextOptimisticPeer(ArrayList allPeers)
	{
		// TODO: mesh modification
		System.out.println("Bass Seeding Unchoke Manager	-	getNextOptimisticPeer()");
		Date timestamp = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
		System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
		
		// TODO: mesh modification
		System.out.println("getNextOptimisticPeer()	-	allPeers:");
		for (Object entry : allPeers) {
			PEPeerTransport p = (PEPeerTransport) entry;
			System.out.println(p.getIp());
		}
		
		List<PeerItem> peerItems;
		List<PEPeerTransport> pepeers;
		PEPeerTransport selectedPepeer;
		Map<PeerItem, PEPeerTransport> pepeerByPeerItem;
		PeerItem selectedPeerItem;
		SortedSet<PeerCost> peerCosts;
		Iterator<PeerCost> i;
		PeerCost peerCost;
		List<PeerItem> bestPeerItems;
		int bestCost;
		
		if (random_.nextInt(100) + 1 > plugin_.getPeerSelectionWeightOnUnchoke())
		{
			// TODO: this is always called due to our default setting for "PeerSelectionWeightOnUnchoke"
			
			selectedPepeer = (PEPeerTransport)UnchokerUtil.getNextOptimisticPeer(allPeers, true, true);
			if (selectedPepeer != null)
			{
				plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
					"Standard Vuze unchoke strategy: next optimistic peer " + 
					selectedPepeer.getIp());
			
				// TODO: mesh modification
				System.out.println("next optimistic peer: "+selectedPepeer.getIp());
				
			} else {
				plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
					"Standard Vuze unchoke strategy: not any unchokable peer available");
				
				// TODO: mesh modification
				System.out.println("no next optimistic peer available!");
			}
			
			return selectedPepeer;
		}
		// get the peers potentially available for optimistic selection
		// (PEPeerTransport objects)
		pepeers = (List)UnchokerUtil.getNextOptimisticPeers(allPeers, false, true, allPeers.size());
		if (pepeers == null) {
			plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
				"Bass unchoke strategy: no unchokable peer available");
			return null;
		}
		// convert PEPeerTransport objects into PeerItem objects
		// and put PEPeerTransport objects into a PeerItem-indexed map, 
		// in order to do later the opposite conversion (PeerItem -> PEPeerTransport) 
		peerItems = new LinkedList<PeerItem>();
		pepeerByPeerItem = new HashMap<PeerItem, PEPeerTransport>();
		for (PEPeerTransport pepeer : pepeers) {
			peerItems.add(pepeer.getPeerItemIdentity());
			pepeerByPeerItem.put(pepeer.getPeerItemIdentity(), pepeer);
		}
		try {
			// the PeerSelectionService returns the cost for each peer
			peerCosts = plugin_.getPeerSelectionService().getPeerCosts(peerItems);
			// builds the list of peers with the lowest cost, 
			bestPeerItems = new LinkedList<PeerItem>();
			i = peerCosts.iterator();
			peerCost = i.next();
			bestCost = peerCost.cost_;
			bestPeerItems.add(peerCost.peerItem_);
			while (i.hasNext()) {
				peerCost = i.next();
				if (peerCost.cost_ != bestCost)
					break;
				bestPeerItems.add(peerCost.peerItem_);
			}
			if (bestPeerItems.size() == 1)
				selectedPeerItem = bestPeerItems.get(0);
			else 
				// if there are two or more peers with the lowest cost, 
				// it randomly chooses among them
				selectedPeerItem = bestPeerItems.get(random_.nextInt(bestPeerItems.size()));
			plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
				"Bass unchoke strategy: next optimistic peer " + 
				selectedPeerItem.getAddressString());
			// convert the chosen PeerItem back to a PEPeerTransport object
			return pepeerByPeerItem.get(selectedPeerItem); 
		} catch (BassException be) {
			// fallback
			plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
				"Unable to use Bass unchoke strategy: next optimistic peer selected using standard Vuze strategy");
			selectedPepeer = (PEPeerTransport)UnchokerUtil.getNextOptimisticPeer(allPeers, true, true);
			if (selectedPepeer != null)
				plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
					"Standard Vuze unchoke strategy: next optimistic peer " + 
					selectedPepeer.getIp());
			else
				plugin_.getLogger().log(BassPlugin.LOG_OPTIMISTIC_UNCHOKE, 
					"Standard Vuze unchoke strategy: no unchokable peer available");
			
			return selectedPepeer;
		}
	}
}
