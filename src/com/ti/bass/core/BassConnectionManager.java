/*
 * Created on 30-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.core;

import java.util.*;

import org.gudy.azureus2.core3.util.AEMonitor;
import com.aelitis.azureus.core.peermanager.peerdb.PeerItem;
import com.ti.bass.core.adapters.PeerDatabaseWrapper;
import com.ti.bass.core.exception.BassException;
import com.ti.bass.core.peerselection.PeerSelectionService.PeerCost;


public class BassConnectionManager extends PeerDatabaseWrapper {

	private BassPlugin	plugin_;
	private Random		random_;
	
	public BassConnectionManager(BassPlugin plugin) {
		plugin_ = plugin;
		random_ = new Random();
	}
	
	@Override
	public PeerItem getNextOptimisticConnectPeer() {
		AEMonitor monitor;
		// peers discovered via Tracker and DHT
		LinkedList<PeerItem> discoveredPeers;
		PeerItem selectedPeer;
		
		if (random_.nextInt(100) + 1 > plugin_.getPeerSelectionWeightOnConnection())
		{
			// USING STANDARD VUZE STRATEGY
			selectedPeer = super.getNextOptimisticConnectPeer();
			if (selectedPeer != null)
				plugin_.getLogger().log(BassPlugin.LOG_CONNECTION, 
					"Standard Vuze connection strategy: next connection peer " + 
					selectedPeer.getAddressString());
			else
				plugin_.getLogger().log(BassPlugin.LOG_CONNECTION, 
					"Standard Vuze connection strategy: not any additional peer to connect to");
			return selectedPeer;
		}

		// USING BASS STRATEGY
		monitor = getMapMonField();
		discoveredPeers = getDiscoveredPeersField();		
		try {
			monitor.enter();
			if (discoveredPeers.isEmpty()) {
				// No Tracker/DHT peers. Let's try with pex peers.
				// Since we are in the monitor critical section
				// the discoveredPeers field cannot be modified by other threads:
				// super.getNextOptimisticConnectPeer() will elaborate on the
				// pex peers only, building/updating
				// the cached_peer_popularities field
				selectedPeer = getGoodPexPeer();
				if (selectedPeer != null)
					plugin_.getLogger().log(BassPlugin.LOG_CONNECTION,
						"Bass connection strategy next connection peer (pex) " + 
							selectedPeer.getAddressString());
				else
					// if the pex peers list is empty too, 
					// there's nothing else to do
					plugin_.getLogger().log(BassPlugin.LOG_CONNECTION, 
						"Bass connection strategy: no additional peer to connect to");
			} else {
				// selecting a "good" tracker/dht peer
				selectedPeer = getGoodPeer(discoveredPeers);
				discoveredPeers.remove(selectedPeer);
				plugin_.getLogger().log(BassPlugin.LOG_CONNECTION,
					"Bass connection strategy next connection peer (tracker/dht) " + 
						selectedPeer.getAddressString());
			}
			return selectedPeer;
		} catch (BassException be) {
			// FALLBACK ON STANDARD VUZE STRATEGY
			plugin_.getLogger().log(BassPlugin.LOG_CONNECTION, 
				"Unable to use Bass connection strategy: next connection peer selected using standard Vuze strategy");
			selectedPeer = super.getNextOptimisticConnectPeer();
			if (selectedPeer != null)
				plugin_.getLogger().log(BassPlugin.LOG_CONNECTION, 
					"Standard Vuze connection strategy: next connection peer " + 
					selectedPeer.getAddressString());
			else
				plugin_.getLogger().log(BassPlugin.LOG_CONNECTION, 
					"Standard Vuze connection strategy: no additional peer to connect to");
			return selectedPeer;
		} finally {
			monitor.exit();
		}
	}

	private PeerItem getGoodPeer(LinkedList<PeerItem> peers) 
		throws BassException {
		SortedSet<PeerCost> peerCosts;
		List<PeerItem> bestPeers;
		Iterator<PeerCost> iterator;
		PeerCost peerCost;
		int bestCost;
		
		
		plugin_.getLogger().log(BassPlugin.LOG_CONNECTION, 
			"Bass connection strategy: Peers List Size: "+peers.size());
			
		Iterator<PeerItem> j = peers.iterator();
		while (j.hasNext()) {
			PeerItem pi = j.next();
			plugin_.getLogger().log(BassPlugin.LOG_CONNECTION,
					"Peer in list: " + pi.getIP() + ":" + pi.getTCPPort());
		}		
		
		// the PeerSelectionService returns the cost of each peer
		peerCosts = plugin_.getPeerSelectionService()
			.getPeerCosts(peers);
		// builds the list of peers with the lowest cost,
		bestPeers = new LinkedList<PeerItem>();
		iterator = peerCosts.iterator();
		peerCost = iterator.next();
		
		plugin_.getLogger().log(
			BassPlugin.LOG_CONNECTION,
			"Sorted peer:  "
				+ peerCost.peerItem_.getAddressString() + " cost "
				+ peerCost.cost_);
		
		bestCost = peerCost.cost_;
		bestPeers.add(peerCost.peerItem_);
		// group peers with the lowest cost
		while (iterator.hasNext()) {
			peerCost = iterator.next();
			
			plugin_.getLogger().log(
				BassPlugin.LOG_CONNECTION,
				"Sorted peer:  "
					+ peerCost.peerItem_.getAddressString() + " cost "
					+ peerCost.cost_);
			
			if (peerCost.cost_ != bestCost)
				break;
			bestPeers.add(peerCost.peerItem_);
		}
		// if there are two or more peers with the lowest cost,
		// it randomly chooses among them
		return bestPeers.size() == 1 ? bestPeers.get(0) : bestPeers
			.get(random_.nextInt(bestPeers.size()));
	}

	private PeerItem getGoodPexPeer() throws BassException {
		// peers discovered via pex
		PeerItem[] cachedPeerPopularities;
		LinkedList<PeerItem> pexPeers;
		PeerItem selectedPeer;
		int popularityPos, i;
		
		pexPeers = new LinkedList<PeerItem>();
		super.getNextOptimisticConnectPeer();
		cachedPeerPopularities = getCachedPeerPopularitiesField();
		popularityPos = getPopularityPosField();
		if (cachedPeerPopularities != null) {
			if (popularityPos > 0) 
				setPopularityPosField(--popularityPos);
			for (i = popularityPos; i < cachedPeerPopularities.length; i++)
				pexPeers.add(cachedPeerPopularities[i]);
		}
		if (pexPeers.isEmpty()) 
			return null;
		selectedPeer = getGoodPeer(pexPeers);
		pexPeers.clear();
		pexPeers.add(selectedPeer);
		for (i = 0; i < cachedPeerPopularities.length; i++)
			if (cachedPeerPopularities[i] != selectedPeer)
				pexPeers.add(cachedPeerPopularities[i]);
		cachedPeerPopularities = pexPeers.toArray(cachedPeerPopularities);
		setCachedPeerPopularitiesField(cachedPeerPopularities);
		setPopularityPosField(++popularityPos);
		return selectedPeer;
	}
}
