/* Created on 21-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.core;

import java.io.*;

//import org.gudy.azureus2.core3.config.COConfigurationManager;
import org.gudy.azureus2.plugins.*;
import org.gudy.azureus2.plugins.download.DownloadPeerListener;
import org.gudy.azureus2.plugins.download.DownloadCompletionListener; // TODO: mesh modification
import org.gudy.azureus2.plugins.logging.LoggerChannel;
import org.gudy.azureus2.plugins.logging.LoggerChannelListener;
import org.gudy.azureus2.plugins.ui.UIInstance;
import org.gudy.azureus2.plugins.ui.UIManagerListener;
import org.gudy.azureus2.plugins.ui.config.*;
import org.gudy.azureus2.plugins.ui.model.BasicPluginConfigModel;
import org.gudy.azureus2.ui.swt.plugins.UISWTInstance;
import org.gudy.azureus2.ui.swt.views.IView;

import com.ti.bass.core.exception.BassException;
import com.ti.bass.core.listeners.BassDownloadCompletionListener;
import com.ti.bass.core.listeners.BassDownloadPeerListener;
import com.ti.bass.core.peerselection.PeerSelectionService;
import com.ti.bass.dummy.DummyPeerSelectionService;
import com.ti.bass.ui.*;
import com.ti.bass.utils.ConfigurationManager;

/**
 * @author bri
 * 
 */
public class BassPlugin implements UnloadablePlugin {

	private static final Boolean		IS_DEBUG				= true;
	private static final String		DEBUG_LOG				= "debug.log";
	public static final String		PLUGIN_NAME				= "BassPlugin";
	public static final int			LOG_OPTIMISTIC_UNCHOKE	= 10;
	public static final int			LOG_CONNECTION			= 100;
	public static final int			LOG_SERVER				= 1000;
	private static final String		DEFAULT_DIR				= File.separatorChar + "stats";
	private static final String		VIEWID					= "Bass.name";
	
	private PluginInterface				pluginInterface_;
	private DownloadPeerListener		downloadPeerListener_;
	private DownloadCompletionListener	downloadCompletionListener_; // TODO: mesh modification
	
	private LoggerChannel				logger_;
	
	private boolean					statsEnabled_;
	private String						statsDir_;
	
	private FileParameter				defaultMapFile_;
	private boolean					defaultMapEnabled_;
	
	private UISWTInstance				swtInstance_;
	private BassUI						bassUI_;
	private IView						bassMainTab_;
	
	// [0, 100] weight of PeerSelectionService on peer connection
	private int						peerSelectionWeightOnConnection_;
	// [0, 100] weight of PeerSelectionService on peer unchoke
	private int						peerSelectionWeightOnUnchoke_;
	
	private PeerSelectionService		peerSelectionService_;
	private ConfigurationManager		configurationManager_;
	private FileWriter					out_;
	
	
	public PeerSelectionService getPeerSelectionService() {
		return peerSelectionService_;
	}
	
	public void setPeerSelectionService(PeerSelectionService service) {
		peerSelectionService_ = service;
	}
	
	public int getPeerSelectionWeightOnConnection() { // TODO: called by BassConnectionManager -> getNextOptimisticConnectPeer()
		return peerSelectionWeightOnConnection_;
	}
	
	public void setPeerSelectionWeightOnConnection(int weight) {
		this.peerSelectionWeightOnConnection_ = weight;
		configurationManager_.setProperty("Weight.on.connection",String.valueOf(weight));
	}
	
	public int getPeerSelectionWeightOnUnchoke() { // TODO: called by BassUnchokeManager -> getNextOptimisticConnectPeer()
		return peerSelectionWeightOnUnchoke_;
	}
	
	public void setPeerSelectionWeightOnUnchoke(int weight) {
		this.peerSelectionWeightOnUnchoke_ = weight;
		configurationManager_.setProperty("Weight.on.unchoke",String.valueOf(weight));
	}
	
	public PluginInterface getPluginInterface() {
		return pluginInterface_;
	}
	
	public LoggerChannel getLogger() {
		return logger_;
	}
	
	public String getStatsDir() {
		return statsDir_;
	}
	
	public boolean areStatsEnabled() {
		return statsEnabled_;
	}
	
	public FileParameter getMapFile() {
		return defaultMapFile_;
	}
	
	public boolean isDefaultMapEnabled() {
		return defaultMapEnabled_;
	}
	
	public BassUI getBassUI() {
		return bassUI_;
	}
	
	public static void load(PluginInterface plugin_interface) {
		plugin_interface.getPluginProperties().setProperty("plugin.version", "01");
		plugin_interface.getPluginProperties().setProperty("plugin.name", PLUGIN_NAME);
	}
	
	public void initialize(PluginInterface pluginInterface) throws PluginException
	{
		BasicPluginConfigModel bassConfig;
		BooleanParameter enableParam, defaultMapParam;
		DirectoryParameter statsDirParam;
		
		configurationManager_ = new ConfigurationManager();
		
		peerSelectionWeightOnConnection_ = Integer.parseInt(configurationManager_.getProperty("Weight.on.connection"));
		peerSelectionWeightOnUnchoke_ = Integer.parseInt(configurationManager_.getProperty("Weight.on.unchoke"));
		
		pluginInterface_ = pluginInterface;
		
		logger_ = pluginInterface.getLogger().getTimeStampedChannel(PLUGIN_NAME);
		
		bassConfig = pluginInterface.getUIManager().createBasicPluginConfigModel(
				ConfigSection.SECTION_PLUGINS, "Bass.name");
		
		bassConfig.addLabelParameter2("Bass.welcome");
		
		// Stats settings
		enableParam = bassConfig.addBooleanParameter2("Bass.enable.stats", "Bass.enable.stats", false);
		
		statsDirParam = bassConfig.addDirectoryParameter2(
			"Bass.main_settings.stats_dir", "Bass.main_settings.stats_dir",
			pluginInterface_.getUtilities().getAzureusUserDir() + DEFAULT_DIR);
		
		LabelParameter statsWarn_ = bassConfig.addLabelParameter2("Bass.main_settings.statsWarn");
		
		enableParam.addEnabledOnSelection(statsDirParam);
		enableParam.addEnabledOnSelection(statsWarn_);
		
		bassConfig.createGroup("Bass.main_settings.group", new Parameter[] {statsDirParam, statsWarn_ });
		
		defaultMapParam = bassConfig.addBooleanParameter2(
			"Bass.enable.defaultMap", "Bass.enable.defaultMap", false);
		
		defaultMapFile_ = bassConfig.addFileParameter2(
			"Bass.main_settings.defult_map_file",
			"Bass.main_settings.defult_map_file", pluginInterface_
				.getUtilities().getAzureusUserDir());
		
		defaultMapParam.addEnabledOnSelection(defaultMapFile_);
		
		LabelParameter mapWarn_ = bassConfig
			.addLabelParameter2("Bass.main_settings.defaultMapWarn");
		
		bassConfig.createGroup("Bass.main_settings.netMapGroup",
			new Parameter[] { defaultMapFile_, mapWarn_ });

		
		statsEnabled_ = true; // TODO: mesh modification
		statsDir_ = pluginInterface_.getUtilities().getAzureusUserDir() + DEFAULT_DIR; // TODO: mesh modification
//		statsEnabled_ = enableParam.getValue();
//		statsDir_ = statsDirParam.getValue();
		
		
		defaultMapEnabled_ = defaultMapParam.getValue();
		
		
		try {
			peerSelectionService_ = new DummyPeerSelectionService(this); // TODO: here DummyPeerSelectionService is set
		} catch (BassException be) {
			throw new PluginException("Bad default map", be);
		}
		
		downloadPeerListener_ = new BassDownloadPeerListener(this); // TODO: here BassDownloadPeerListener is set
		downloadCompletionListener_ = new BassDownloadCompletionListener(this); // TODO: mesh modification
		
		
		if (IS_DEBUG) {
			try {
				out_ = new FileWriter(getPluginInterface()
					.getPluginDirectoryName()
					+ "/" + DEBUG_LOG, false);
			} catch (IOException ex) {
				System.err.println("Error: " + ex.getMessage());
			}
			logger_.addListener(new LoggerChannelListener() {

				public void messageLogged(int type, String message) {
					try {
						out_.write(message + "\n");
						out_.flush();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				public void messageLogged(String str, Throwable error) {
					try {
						out_.write(str + "\n");
						out_.write(error.getLocalizedMessage() + "\n");
						out_.flush();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
		}
		
		
		// Get notified when the UI is attached
		pluginInterface.getUIManager().addUIListener(new UIManagerListener()
		{
			public void UIAttached(UIInstance instance) {
				IView peerSelectionUITab, peersGraphicViewTab, peersViewTab;
				
				if (instance instanceof UISWTInstance) {
					swtInstance_ = ((UISWTInstance) instance);
					bassUI_ = new BassUI(BassPlugin.this, swtInstance_);

					bassMainTab_ = new BassMainTab(bassUI_, BassPlugin.this);
					bassUI_.attachTab(bassMainTab_);
					
					peerSelectionUITab = peerSelectionService_.getUITab(bassUI_);
					if (peerSelectionUITab != null)
						// the peer selection logic has a graphical interface
						bassUI_.attachTab(peerSelectionUITab);

					peersGraphicViewTab = new BassPeersGraphicView(BassPlugin.this);
					bassUI_.attachTab(peersGraphicViewTab);
					
					peersViewTab = new BassPeersView(BassPlugin.this);
					bassUI_.attachTab(peersViewTab);
					
					// Add bassUI to the menu
					swtInstance_.addView(UISWTInstance.VIEW_MAIN, VIEWID, bassUI_);
				}
			}

			public void UIDetached(UIInstance instance) {
				if (instance instanceof UISWTInstance)
					instance = null;
			}
		});
		
		pluginInterface.getDownloadManager().getGlobalDownloadEventNotifier()
			.addPeerListener(downloadPeerListener_); // TODO: here BassDownloadPeerListener is registered
		
		// TODO: mesh modification
		pluginInterface.getDownloadManager().getGlobalDownloadEventNotifier()
			.addCompletionListener(downloadCompletionListener_); // TODO: here BassDownloadCompletionListener is registered
	}
	
	public void unload() throws PluginException {
		if (swtInstance_ != null)
			swtInstance_.removeViews(UISWTInstance.VIEW_MAIN, VIEWID);
	}
}
