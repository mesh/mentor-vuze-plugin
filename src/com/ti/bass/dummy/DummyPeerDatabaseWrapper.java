/*
 * Created on 21-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.dummy;

import com.aelitis.azureus.core.peermanager.peerdb.PeerDatabase;
import com.aelitis.azureus.core.peermanager.peerdb.PeerItem;
import com.ti.bass.core.BassPlugin;
import com.ti.bass.core.adapters.PeerDatabaseWrapper;

/**
 * Extension of PeerDatabaseWrapper that logs invocations of
 * getNextOptimisticConnectPeer().
 */
public class DummyPeerDatabaseWrapper extends PeerDatabaseWrapper {

	private BassPlugin	plugin_;

	public DummyPeerDatabaseWrapper(BassPlugin plugin) {
		plugin_ = plugin;
	}

	public DummyPeerDatabaseWrapper(BassPlugin plugin, PeerDatabase delegate) {
		plugin_ = plugin;
		delegate_ = delegate;
	}

	public PeerItem getNextOptimisticConnectPeer() {
		plugin_.getLogger().log(
			"getNextOptimisticConnectPeer() - discovered_peers size: "
				+ getDiscoveredPeersField().size());
		
		// this is a good point where to ask ALTO service what to do:
		// - use utility method getDiscoveredPeersField() to access 
		//   the discovered_peer field (a LinkedList of PeerItem objects);
		// - to get the Peer IP, use PeerItem.getIP();
		// - choose the next Peer to connect with, using information 
		//   from the ALTO service;
		// - return the corresponding PeerItem object.
		
		return delegate_.getNextOptimisticConnectPeer();
	}
}
