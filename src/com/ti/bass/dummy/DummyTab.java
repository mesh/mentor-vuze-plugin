/*
 * Created on 14-Oct-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package com.ti.bass.dummy;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.gudy.azureus2.core3.internat.MessageText;
import org.gudy.azureus2.ui.swt.Messages;
import org.gudy.azureus2.ui.swt.plugins.UISWTInstance;
import org.gudy.azureus2.ui.swt.views.AbstractIView;
import com.ti.bass.core.BassPlugin;
import com.ti.bass.core.exception.BassException;
import com.ti.bass.ui.BassUI;
import com.ti.bass.utils.CidrCostTree;
import com.ti.bass.utils.Ip4Cidr;
import com.ti.bass.utils.CidrCostTree.CidrCost;

public class DummyTab extends AbstractIView {

	public static final String		TAB_TITLE		= "Map manager";
	Display							display_;
	BassPlugin						plugin_;
	BassUI							bassUI_;
	CidrCostTree					cidrCostTree_;
	UISWTInstance					swtInstance_;
	Composite						tabPanel_;
	private static final String[]	FILTER_NAMES	= {
		"ALTO Network Map (*.anm)", "All Files (*.*)" };
	// These filter extensions are used to filter which files are displayed.
	private static final String[]	FILTER_EXTS		= { "*.anm", "*.*" };

	public DummyTab(BassUI bassUI, CidrCostTree cidrCostTree)  {
		bassUI_ = bassUI;
		plugin_ = bassUI_.getPlugin();
		cidrCostTree_ = cidrCostTree;
	}

	@Override
	public String getFullTitle() {
		return TAB_TITLE;
	}

	@Override
	public Composite getComposite() {
		return tabPanel_;
	}

	@Override
	public void initialize(Composite composite) {
		display_ = bassUI_.getTabFolder().getDisplay();
		tabPanel_ = new Composite(bassUI_.getTabFolder(), SWT.NONE);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		tabPanel_.setLayout(gridLayout);
		Composite topSection = new Composite(tabPanel_, SWT.NONE);
		gridLayout = new GridLayout();
		swtInstance_ = bassUI_.getSwtInstance();
		gridLayout.numColumns = 1;
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		topSection.setLayout(gridLayout);
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		topSection.setLayoutData(gridData);
		final Image image = swtInstance_.loadImage("com" + File.separatorChar
			+ "ti" + File.separatorChar + "bass" + File.separatorChar + "ui"
			+ File.separatorChar + "img" + File.separatorChar
			+ "bass_netmap_278x110.jpg");
		final Label logo = new Label(topSection, SWT.LEFT);
		logo.setBackgroundImage(image);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_END);
		gridData.heightHint = 110;
		gridData.widthHint = 278;
		logo.setLayoutData(gridData);
		Group altoSection = new Group(tabPanel_, SWT.NONE);
		Messages.setLanguageText(altoSection,
			"Bass.main_view.PreferredPeersLabel");
		GridLayout layoutAlto_ = new GridLayout(4, false);
		altoSection.setLayout(layoutAlto_);
		Composite IpSection = new Composite(altoSection, SWT.NONE);
		IpSection.setLayout(new GridLayout(10, false));
		Composite ButtonSection = new Composite(altoSection, SWT.NONE);
		ButtonSection.setLayout(new GridLayout(1, false));
		Composite List = new Composite(altoSection, SWT.NONE);
		List.setLayout(new GridLayout(1, false));
		gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = 2;
		gridData.heightHint = 200;
		gridData.widthHint = 250;
		List.setLayoutData(gridData);
		Label ip = new Label(IpSection, SWT.NULL);
		Messages.setLanguageText(ip, "Bass.main_view.Ip");
		final Text ip1 = new Text(IpSection, SWT.SINGLE | SWT.BORDER);
		gridData = new GridData();
		gridData.widthHint = 30;
		ip1.setLayoutData(gridData);
		Label div1 = new Label(IpSection, SWT.NULL);
		div1.setText(".");
		final Text ip2 = new Text(IpSection, SWT.SINGLE | SWT.BORDER);
		gridData = new GridData();
		gridData.widthHint = 30;
		ip2.setLayoutData(gridData);
		Label div2 = new Label(IpSection, SWT.NULL);
		div2.setText(".");
		final Text ip3 = new Text(IpSection, SWT.SINGLE | SWT.BORDER);
		gridData = new GridData();
		gridData.widthHint = 30;
		ip3.setLayoutData(gridData);
		Label div3 = new Label(IpSection, SWT.NULL);
		div3.setText(".");
		final Text ip4 = new Text(IpSection, SWT.SINGLE | SWT.BORDER);
		gridData = new GridData();
		gridData.widthHint = 30;
		ip4.setLayoutData(gridData);
		Label netMask = new Label(IpSection, SWT.NULL);
		netMask.setText("/");
		final Spinner subnet = new Spinner(IpSection, SWT.BORDER);
		subnet.setMinimum(0);
		subnet.setMaximum(32);
		subnet.setSelection(24);
		subnet.setIncrement(1);
		Button buttonAggiungi = new Button(ButtonSection, SWT.BUTTON1);
		buttonAggiungi.setLayoutData(new GridData(
			GridData.HORIZONTAL_ALIGN_CENTER));
		Messages.setLanguageText(buttonAggiungi, "Bass.main_view.AddButton");
		Button buttonRimuovi = new Button(ButtonSection, SWT.BUTTON1);
		buttonRimuovi.setLayoutData(new GridData(
			GridData.HORIZONTAL_ALIGN_CENTER));
		Messages.setLanguageText(buttonRimuovi, "Bass.main_view.RemoveButton");
		final Table tableNetMap = new Table(List, SWT.BORDER | SWT.MULTI
			| SWT.V_SCROLL);
		tableNetMap.setLinesVisible(true);
		tableNetMap.setHeaderVisible(true);
		tableNetMap.setLayoutData(new GridData(GridData.FILL_BOTH));
		String[] titles = { "IP address/mask", "Cost" };
		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(tableNetMap, SWT.NONE);
			Messages.setLanguageText(column, "Bass.main_view.table.column" + i);
		}
		for (int i = 0; i < titles.length; i++) {
			tableNetMap.getColumn(i).pack();
		}
		tableNetMap.removeAll();
		for (CidrCost cidrCost: cidrCostTree_.getCidrs() ) {
			String ipAddress = cidrCost.cidr_.toString();
			String cost = Integer.toString(cidrCost.cost_);
			TableItem item = new TableItem(tableNetMap, SWT.NONE);
			item.setText(0, ipAddress);
			item.setText(1, cost);
		}

		final Shell shell = new Shell(display_);
		shell.setText("File Dialog");
		Composite ListButtons = new Composite(List, SWT.NONE);
		ListButtons.setLayout(new GridLayout(2, false));
		Button buttonLoad = new Button(ListButtons, SWT.BUTTON1);
		Messages.setLanguageText(buttonLoad, "Bass.main_view.buttonLoad");
		buttonLoad.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent event) {
				FileDialog dlg = new FileDialog(shell, SWT.OPEN);
				dlg.setFilterNames(FILTER_NAMES);
				dlg.setFilterExtensions(FILTER_EXTS);
				String selected = dlg.open();
				if (selected != null) 
				try {
					cidrCostTree_.loadMapFromFile(selected);
						
						tableNetMap.removeAll();
						plugin_.getLogger().log(
							"Network map loaded from file: " + selected);
						for (CidrCost cidrCost : cidrCostTree_
							.getCidrs()) {
							String ipAddress = cidrCost.cidr_.toString();
							String cost = Integer
								.toString(cidrCost.cost_);
							TableItem item = new TableItem(tableNetMap,
								SWT.NONE);
							item.setText(0, ipAddress);
							item.setText(1, cost);
						}
				} catch (BassException be) {
					MessageBox messageBox = new MessageBox(shell,
						SWT.ICON_ERROR | SWT.OK);
					messageBox.setMessage(MessageText
						.getString("Bass.main_view.notValidFileMessage"));
					messageBox.setText(MessageText
						.getString("Bass.main_view.notValidFileTitle"));
					messageBox.open();
				};
			}
		});
		Button buttonSave = new Button(ListButtons, SWT.BUTTON1);
		buttonSave.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent event) {
				// User has selected to open a single file
				FileDialog dlg = new FileDialog(shell, SWT.SAVE);
				dlg.setFilterNames(FILTER_NAMES);
				dlg.setFilterExtensions(FILTER_EXTS);
				dlg.setFilterPath(plugin_.getPluginInterface().getUtilities()
					.getAzureusProgramDir());
				try {
					while (true) {
						String selected = dlg.open();
						File f = new File(selected);
						if (f.exists()) {
							MessageBox messageBox = new MessageBox(shell,
								SWT.ICON_QUESTION | SWT.NO | SWT.YES);
							messageBox
								.setMessage(MessageText
									.getString("Bass.main_view.overwriteFileMessage"));
							messageBox
								.setText(MessageText
									.getString("Bass.main_view.overwriteFileTitle"));
							int response = messageBox.open();
							if (response == SWT.YES) {
								cidrCostTree_.saveMapOnFile(selected);
								plugin_.getLogger().log(
									"Network map saved in file: " + selected);
								break;
							}
						} else {
							cidrCostTree_.saveMapOnFile(selected);
							plugin_.getLogger().log(
								"Network map saved in file: " + selected);
							break;
						}
					}
				} catch (BassException be) {
					MessageBox messageBox = new MessageBox(shell,
						SWT.ICON_ERROR | SWT.OK);
					messageBox.setMessage(MessageText
						.getString("Bass.main_view.mapNotSavedMessage"));
					messageBox.setText(MessageText
						.getString("Bass.main_view.mapNotSavedTitle"));
					messageBox.open();					
				}
			}
		});
		Messages.setLanguageText(buttonSave, "Bass.main_view.buttonSave");
		Label cost = new Label(IpSection, SWT.NULL);
		Messages.setLanguageText(cost, "Bass.main_view.CostLabel");
		final Spinner costSpinner = new Spinner(IpSection, SWT.BORDER);
		costSpinner.setMinimum(-1);
		costSpinner.setMaximum(100);
		costSpinner.setSelection(0);
		costSpinner.setIncrement(1);
		costSpinner.setLayoutData(new GridData(GridData.FILL_BOTH));
		buttonAggiungi.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				if (Integer.parseInt(ip1.getText()) < 0
					|| Integer.parseInt(ip1.getText()) > 255) {
					ip1
						.setBackground(display_
							.getSystemColor(SWT.COLOR_YELLOW));
					return;
				} else
					ip1.setBackground(display_.getSystemColor(SWT.COLOR_WHITE));
				if (Integer.parseInt(ip2.getText()) < 0
					|| Integer.parseInt(ip2.getText()) > 255) {
					ip2
						.setBackground(display_
							.getSystemColor(SWT.COLOR_YELLOW));
					return;
				} else
					ip2.setBackground(display_.getSystemColor(SWT.COLOR_WHITE));
				if (Integer.parseInt(ip3.getText()) < 0
					|| Integer.parseInt(ip3.getText()) > 255) {
					ip3
						.setBackground(display_
							.getSystemColor(SWT.COLOR_YELLOW));
					return;
				} else
					ip3.setBackground(display_.getSystemColor(SWT.COLOR_WHITE));
				if (Integer.parseInt(ip4.getText()) < 0
					|| Integer.parseInt(ip4.getText()) > 255) {
					ip4
						.setBackground(display_
							.getSystemColor(SWT.COLOR_YELLOW));
					return;
				} else
					ip4.setBackground(display_.getSystemColor(SWT.COLOR_WHITE));
				TableItem item = new TableItem(tableNetMap, SWT.NONE);
				item.setText(0, ip1.getText() + "." + ip2.getText() + "."
					+ ip3.getText() + "." + ip4.getText() + "/"
					+ subnet.getSelection());
				item.setText(1, Integer.toString(costSpinner.getSelection()));
				try {
					cidrCostTree_.addCidr(
						new Ip4Cidr(item.getText(0)), Integer.parseInt(item.getText(1)));
				} catch (NumberFormatException e1) {
					// impossible
					e1.printStackTrace();
				} catch (BassException be) {
					// impossible
					be.printStackTrace();
				}
				for (int i = 0; i < tableNetMap.getColumnCount(); i++) 
					tableNetMap.getColumn(i).pack();
				ip1.setText("");
				ip2.setText("");
				ip3.setText("");
				ip4.setText("");
			}
		});
		ip1.addKeyListener(new KeyListener() {

			public void keyReleased(KeyEvent e) {
				if (!isANumber(e.character)) {
					ip1.setText("");
					ip1.redraw();
				}
			}

			public void keyPressed(KeyEvent e) {
			}
		});
		ip2.addKeyListener(new KeyListener() {

			public void keyReleased(KeyEvent e) {
				if (!isANumber(e.character)) {
					ip2.setText("");
					ip2.redraw();
				}
			}

			public void keyPressed(KeyEvent e) {
			}
		});
		ip3.addKeyListener(new KeyListener() {

			public void keyReleased(KeyEvent e) {
				if (!isANumber(e.character)) {
					ip3.setText("");
					ip3.redraw();
				}
			}

			public void keyPressed(KeyEvent e) {
			}
		});
		ip4.addKeyListener(new KeyListener() {

			public void keyReleased(KeyEvent e) {
				if (!isANumber(e.character)) {
					ip4.setText("");
					ip4.redraw();
				}
			}

			public void keyPressed(KeyEvent e) {
			}
		});
		buttonRimuovi.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				int selection[] = tableNetMap.getSelectionIndices();
		        TableItem[] selection_ = tableNetMap.getSelection();
 					try {
						cidrCostTree_.removeNode(new Ip4Cidr(selection_[0].getText().toString()));
					} catch (BassException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				tableNetMap.remove(selection);
				tableNetMap.redraw();
			}
		});
	}
	
	/**
	 * Check if a char is a number or DEL, BS, CR.
	 * 
	 * @param char
	 */
	private boolean isANumber(char ch) {
		return (ch == '0' || ch == '1' || ch == '2' || ch == '3' || ch == '4'
			|| ch == '5' || ch == '6' || ch == '7' || ch == '8' || ch == '9'
			|| ch == SWT.DEL || ch == SWT.BS || ch == SWT.CR || ch == SWT.TAB);
	}
}
