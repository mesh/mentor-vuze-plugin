/*
 * Created on 5-Oct-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package com.ti.bass.dummy;

import java.util.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat; // TODO: mesh modification

import mesh.ConfFileParams; // TODO: mesh modification
import mesh.MeshPathListEntry;
import mesh.ProcessCall;
import mesh.SnmpClient; // TODO: mesh modification

import org.snmp4j.smi.OID; // TODO: mesh modification
import org.gudy.azureus2.plugins.download.Download;
import org.gudy.azureus2.ui.swt.views.IView;

import com.aelitis.azureus.core.peermanager.peerdb.PeerItem;
import com.google.gson.Gson;
import com.ti.bass.core.BassPlugin;
import com.ti.bass.core.exception.BassException;
import com.ti.bass.core.peerselection.PeerSelectionService;
import com.ti.bass.ui.BassUI;
import com.ti.bass.utils.CidrCostTree;
import com.ti.bass.utils.Ip4Address;
import com.ti.bass.utils.Ip4Cidr;


/**
 * Implementation of Vuze PeerSelectionService: this code realizes our strategy
 * for peer selection. Every time method getGoodPeer is called, our peer selection 
 * service returns the peer with lowest cost between the collection peers
 * passed as input. The operation is realized through the scan of the cidr tree.
 */
public class DummyPeerSelectionService implements PeerSelectionService {

	private BassPlugin				plugin_;
	
	private IView					uiTab_;
	
	private CidrCostTree			cidrCostTree_;
	public static final int			DEFAULT_COST = 1000; // TODO: DEFAULT_COST (default value in CIDR cost tree -> BASS GUI)
	
	// TODO: mesh modification
	private ConfFileParams config_ = ConfFileParams.getInstance(); // TODO: mesh modification
	
	// TODO: mesh modification
	private Map<String, Integer> onehopmap_tmp = new HashMap<String, Integer>();
	private Map<String, Integer> nhopmap_tmp = new HashMap<String, Integer>();
	private Map<String, Integer> onehopmap = new HashMap<String, Integer>();
	private Map<String, Integer> nhopmap = new HashMap<String, Integer>();
	
	private ScheduledThreadPoolExecutor executor; // TODO: mesh modification
	Runnable updateThread; // TODO: mesh modification
	
	
	private List<List<String>> meshPaths; // TODO: mesh modification
	
	
	SnmpClient client; // TODO: mesh modification
	
	// TODO: mesh modification
	static final OID meshPathListsTable		= new OID(".1.3.6.1.3.1234.1.6");
	static final OID meshInterfaceNameMPATH	= new OID(".1.3.6.1.3.1234.1.6.1");
	static final OID localMacAddressMPATH	= new OID(".1.3.6.1.3.1234.1.6.2");
	static final OID destinationMacAddress	= new OID(".1.3.6.1.3.1234.1.6.3");
	static final OID nextHopMacAddress		= new OID(".1.3.6.1.3.1234.1.6.4");
	static final OID metric					= new OID(".1.3.6.1.3.1234.1.6.5");
	static final OID expirationTime			= new OID(".1.3.6.1.3.1234.1.6.6");
	
	
	// TODO: mesh modification
	private Gson gson;
	private File iwModPath;
	
	
	boolean initSeedRdyIndicate = false; // TODO: mesh modification
	
	
	
	
	// TODO: mesh modification
	// constructor creates local SNMP client (if enabled) and periodic ALM update thread
	public DummyPeerSelectionService(BassPlugin plugin) throws BassException
	{
		plugin_ = plugin;
		cidrCostTree_ = new CidrCostTree();
		
		if (config_.getVANILLA() == false)
		{
			// we are in mesh peer selection mode
			// -> initialize objects needed for ALM info updating
			
			if (config_.getSNMP_QUERY_ACTIVE() == true)
			{
				// TODO: query SNMP agent on local mesh IP
				client = new SnmpClient("udp:"+
						config_.getMESH_IP()+
						"/"+String.valueOf(config_.getSNMP_PORT()));
			}
			else
			{
				gson = new Gson();
				iwModPath = new File("bin_c/");
			}
			
			meshPaths = new ArrayList<List<String>>();
		}
		
		
		executor = new ScheduledThreadPoolExecutor(1);
		
		updateThread = new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					updateFunction();
				
				} catch (BassException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(Exception e)
				{
					// TODO: occurs when local SNMP agent is not running yet
					e.printStackTrace();
				}
				
			}
		};
		
		// TODO: info update interval
		// -> normal unchoking is triggered all 10s
		// -> optimistic unchoking is triggered all 30s
		// -> update interval should directly depend on choking interval
		// -> update interval should also consider mesh info update interval of MeshSW (if SNMP query, 5s)
		executor.scheduleAtFixedRate(
				updateThread, 0, config_.getALM_UPDATE_IVAL(), TimeUnit.SECONDS);
	}
	
	
	// TODO: mesh modification	
	private void updateFunction() throws BassException
	{
		
		// TODO: helper code to determine rdy state of initial seed
		if (!initSeedRdyIndicate)
		{
			// ensure that we are no leecher
			// -> check for existence of "dl_*" files
			String pathToScan = ".";
			File folderToScan = new File(pathToScan);
			File[] listOfFiles = folderToScan.listFiles();
			String fileName;
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					fileName = listOfFiles[i].getName();
					if (fileName.startsWith("dl_")) {
						// we are leecher
						// -> exit and do not call helper code again
						initSeedRdyIndicate = true;
						break;
					}
				}
			}
			if (!initSeedRdyIndicate)
			{
				for (Download download : plugin_.getPluginInterface()
						.getDownloadManager().getDownloads())
				{
					// there will be only one Download element (seed or leechers)
					if (download.getState() == Download.ST_STOPPED
							&& !download.isChecking() && download.isComplete())
					{
						// we are the initial seed and waiting to start UL
						// -> write indication file if not done yet
						Date timestamp = new Date();
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
								"dd.MM.yyyy.hh.mm.ss");
						File ulRdy = new File("ul_ready_"
								+ simpleDateFormat.format(timestamp.getTime()));
						try {
							System.out.println("initial seed ready!");
							System.out.println("Timestamp: "
									+ simpleDateFormat.format(timestamp.getTime()));
							ulRdy.createNewFile();
							initSeedRdyIndicate = true;
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		
		if (config_.getVANILLA() == false)
		{
			// we are in mesh mode -> update mesh path / ALM info
			
			meshPaths.clear();
			
			onehopmap_tmp = onehopmap;
			nhopmap_tmp = nhopmap;
			
			if (config_.getSNMP_QUERY_ACTIVE() == true)
			{
				// query ALM info via SNMP (MeshNode SW)
				
				System.out.println("Dummy Peer Selection Service  -  update SNMP data");
				Date timestamp = new Date();
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
				System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
				
				try {
					meshPaths = client.getTableAsStrings(new OID[] {
							new OID(meshInterfaceNameMPATH.toString()),
							new OID(localMacAddressMPATH.toString()),
							new OID(destinationMacAddress.toString()),
							new OID(nextHopMacAddress.toString()),
							new OID(metric.toString()),
							new OID(expirationTime.toString()) });

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else {
				
				// use iw to get ALM info
				
				System.out.println("Dummy Peer Selection Service  -  update ALM via iw");
				Date timestamp = new Date();
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss");
				System.out.println("Timestamp: " + simpleDateFormat.format(timestamp.getTime()));
				
				List<MeshPathListEntry> mpleList = getMeshPathList();
				
				for (MeshPathListEntry mple : mpleList)
				{
					List<String> mpleStringList = new ArrayList<String>();
					
					mpleStringList.add(mple.getMeshPointLocalInterfaceName());
					mpleStringList.add(mple.getMeshPointLocalMacAddress());
					mpleStringList.add(mple.getDestinationMacAddress());
					mpleStringList.add(mple.getNextHopMacAddress());
					mpleStringList.add(String.valueOf(mple.getMetric()));
					mpleStringList.add(String.valueOf(mple.getExpirationTime()));
					
					meshPaths.add(mpleStringList);
				}
			}
			
			
			synchronized (cidrCostTree_)
			{
				// TODO: cidrCostTree affects connection handshake & optimistic unchoking
				// - only if weight properties (see ConfigurationManager) are not set to 0
				// - else, this just affects GUI of bass plugin, nice to have
				cidrCostTree_.clearTree();
				
				String ipAddrCIDR = null;
				String ipAddr = null;
				int metric = 0; // TODO: best default value for metric ?
				
				boolean is1Hop;
				boolean destMAC_unknown;

				for (int i = 0; i < meshPaths.size(); i++)
				{
					is1Hop = false;
					destMAC_unknown = false;
					
					String destMAC	= meshPaths.get(i).get(2);	// path destination MAC
					String nhMAC	= meshPaths.get(i).get(3);	// path next hop MAC
					
					// destMAC = nhMAC -> 1-hop neighbor
					if (destMAC.equals(nhMAC))
						is1Hop = true;
					
					
					switch (destMAC)
					{
					
					// Galileo Boards
					// --------------------------------------

					case "04:f0:21:10:c3:23": // Galileo 1
						ipAddrCIDR = "192.168.123.10/32";
						ipAddr = "192.168.123.10";
						break;
						
					case "04:f0:21:10:c3:20": // Galileo 2
						ipAddrCIDR = "192.168.123.11/32";
						ipAddr = "192.168.123.11";
						break;
						
					case "04:f0:21:10:c8:0f": // Galileo 3
						ipAddrCIDR = "192.168.123.12/32";
						ipAddr = "192.168.123.12";
						break;
						
					case "04:f0:21:10:c3:4f": // Galileo 4
						ipAddrCIDR = "192.168.123.13/32";
						ipAddr = "192.168.123.13";
						break;
						
					case "04:f0:21:10:c2:90": // Galileo 5
						ipAddrCIDR = "192.168.123.14/32";
						ipAddr = "192.168.123.14";
						break;
						
					case "04:f0:21:10:c2:8a": // Galileo 6
						ipAddrCIDR = "192.168.123.15/32";
						ipAddr = "192.168.123.15";
						break;
						
					case "04:f0:21:10:c8:18": // Galileo 7
						ipAddrCIDR = "192.168.123.16/32";
						ipAddr = "192.168.123.16";
						break;
						
					case "04:f0:21:10:c3:2b": // Galileo 8
						ipAddrCIDR = "192.168.123.17/32";
						ipAddr = "192.168.123.17";
						break;
						
					case "04:f0:21:10:c8:15": // Galileo 9
						ipAddrCIDR = "192.168.123.18/32";
						ipAddr = "192.168.123.18";
						break;
						
					case "04:f0:21:10:c3:57": // Galileo 10
						ipAddrCIDR = "192.168.123.19/32";
						ipAddr = "192.168.123.19";
						break;
						
					case "04:f0:21:10:c3:41": // Galileo 11
						ipAddrCIDR = "192.168.123.20/32";
						ipAddr = "192.168.123.20";
						break;
						
					case "04:f0:21:10:c3:71": // Galileo 12
						ipAddrCIDR = "192.168.123.21/32";
						ipAddr = "192.168.123.21";
						break;
						
					case "04:f0:21:10:c3:2a": // Galileo 13
						ipAddrCIDR = "192.168.123.22/32";
						ipAddr = "192.168.123.22";
						break;
						
					case "04:f0:21:10:c3:62": // Galileo 14
						ipAddrCIDR = "192.168.123.23/32";
						ipAddr = "192.168.123.23";
						break;
						
					case "04:f0:21:10:c3:55": // Galileo 15
						ipAddrCIDR = "192.168.123.24/32";
						ipAddr = "192.168.123.24";
						break;
						
					case "04:f0:21:10:c2:8e": // Galileo 16
						ipAddrCIDR = "192.168.123.25/32";
						ipAddr = "192.168.123.25";
						break;
						
					case "04:f0:21:0f:77:54": // Galileo 17
						ipAddrCIDR = "192.168.123.26/32";
						ipAddr = "192.168.123.26";
						break;
						
					case "04:f0:21:10:c3:39": // Galileo 18
						ipAddrCIDR = "192.168.123.27/32";
						ipAddr = "192.168.123.27";
						break;
						
					case "04:f0:21:10:c3:33": // Galileo 19
						ipAddrCIDR = "192.168.123.28/32";
						ipAddr = "192.168.123.28";
						break;
						
					case "04:f0:21:10:c5:af": // Galileo 20
						ipAddrCIDR = "192.168.123.29/32";
						ipAddr = "192.168.123.29";
						break;
						
					case "04:f0:21:10:c3:3f": // Galileo 21
						ipAddrCIDR = "192.168.123.30/32";
						ipAddr = "192.168.123.30";
						break;
						
					case "04:f0:21:10:c3:51": // Galileo 22
						ipAddrCIDR = "192.168.123.31/32";
						ipAddr = "192.168.123.31";
						break;
						
					case "04:f0:21:10:c3:4c": // Galileo 23
						ipAddrCIDR = "192.168.123.32/32";
						ipAddr = "192.168.123.32";
						break;
						
					case "04:f0:21:10:c2:88": // Galileo 24
						ipAddrCIDR = "192.168.123.33/32";
						ipAddr = "192.168.123.33";
						break;
						
					case "04:f0:21:10:c8:0d": // Galileo 25
						ipAddrCIDR = "192.168.123.34/32";
						ipAddr = "192.168.123.34";
						break;
						
					case "04:f0:21:10:c3:53": // Galileo 26
						ipAddrCIDR = "192.168.123.35/32";
						ipAddr = "192.168.123.35";
						break;
						
					case "04:f0:21:10:c5:52": // Galileo 27
						ipAddrCIDR = "192.168.123.36/32";
						ipAddr = "192.168.123.36";
						break;
						
					case "04:f0:21:10:c8:17": // Galileo 28
						ipAddrCIDR = "192.168.123.37/32";
						ipAddr = "192.168.123.37";
						break;
						
					case "04:f0:21:10:c5:51": // Galileo 29
						ipAddrCIDR = "192.168.123.38/32";
						ipAddr = "192.168.123.38";
						break;
						
					case "04:f0:21:10:c3:59": // Galileo 30
						ipAddrCIDR = "192.168.123.39/32";
						ipAddr = "192.168.123.39";
						break;
						
					case "04:f0:21:10:c3:52": // Galileo 31
						ipAddrCIDR = "192.168.123.40/32";
						ipAddr = "192.168.123.40";
						break;
						
					case "04:f0:21:10:c3:54": // Galileo 32
						ipAddrCIDR = "192.168.123.41/32";
						ipAddr = "192.168.123.41";
						break;
						
					case "04:f0:21:10:c3:38": // Galileo 33
						ipAddrCIDR = "192.168.123.42/32";
						ipAddr = "192.168.123.42";
						break;
						
					case "04:f0:21:10:c3:65": // Galileo 34
						ipAddrCIDR = "192.168.123.43/32";
						ipAddr = "192.168.123.43";
						break;
						
					case "04:f0:21:10:c3:21": // Galileo 35
						ipAddrCIDR = "192.168.123.44/32";
						ipAddr = "192.168.123.44";
						break;
						
					case "04:f0:21:10:c5:53": // Galileo 36
						ipAddrCIDR = "192.168.123.45/32";
						ipAddr = "192.168.123.45";
						break;
						
					case "04:f0:21:10:c8:0c": // Galileo 37
						ipAddrCIDR = "192.168.123.46/32";
						ipAddr = "192.168.123.46";
						break;
						
					case "04:f0:21:10:c3:3b": // Galileo 38
						ipAddrCIDR = "192.168.123.47/32";
						ipAddr = "192.168.123.47";
						break;
						
					case "04:f0:21:10:c8:0e": // Galileo 39
						ipAddrCIDR = "192.168.123.48/32";
						ipAddr = "192.168.123.48";
						break;
						
					case "04:f0:21:14:c6:5e": // Galileo 40
						ipAddrCIDR = "192.168.123.49/32";
						ipAddr = "192.168.123.49";
						break;
						
						
//					// Raspberry Pis
//					// --------------------------------------
//
//					case "10:6f:3f:a9:47:ce": // Pi 1
//						ipAddrCIDR = "192.168.123.50/32";
//						ipAddr = "192.168.123.50";
//						break;
//						
//					case "10:6f:3f:a9:47:69": // Pi 2
//						ipAddrCIDR = "192.168.123.51/32";
//						ipAddr = "192.168.123.51";
//						break;
//						
//					case "4c:e6:76:f1:ff:ce": // Pi 3
//						ipAddrCIDR = "192.168.123.52/32";
//						ipAddr = "192.168.123.52";
//						break;
//						
//					case "4c:e6:76:f1:fd:ca": // Pi 4
//						ipAddrCIDR = "192.168.123.53/32";
//						ipAddr = "192.168.123.53";
//						break;
//						
//					case "10:6f:3f:a9:47:9d": // Pi 5
//						ipAddrCIDR = "192.168.123.54/32";
//						ipAddr = "192.168.123.54";
//						break;
//						
//					case "10:6f:3f:a9:47:45": // Pi 6
//						ipAddrCIDR = "192.168.123.55/32";
//						ipAddr = "192.168.123.55";
//						break;
//						
//					case "4c:e6:76:f1:fe:32": // Pi 7
//						ipAddrCIDR = "192.168.123.56/32";
//						ipAddr = "192.168.123.56";
//						break;
//						
//					case "10:6f:3f:5b:d7:58": // Pi 8
//						ipAddrCIDR = "192.168.123.57/32";
//						ipAddr = "192.168.123.57";
//						break;
//						
//					case "10:6f:3f:a9:47:e6": // Pi 9
//						ipAddrCIDR = "192.168.123.58/32";
//						ipAddr = "192.168.123.58";
//						break;
//						
//					case "4c:e6:76:1a:d8:35": // Pi 10
//						ipAddrCIDR = "192.168.123.59/32";
//						ipAddr = "192.168.123.59";
//						break;
//						
//					// --------------------------------------
					
					default: // unknown
						destMAC_unknown = true;
						break;
						
					}
					
					// get ALM metric
					metric = Integer.parseInt(meshPaths.get(i).get(4));
					
					if (!destMAC_unknown)
					{
						if (is1Hop) {
							onehopmap_tmp.put(ipAddr, metric);
							nhopmap_tmp.remove(ipAddr);
						}
						else {
							nhopmap_tmp.put(ipAddr, metric);
							onehopmap_tmp.remove(ipAddr);
						}
		
						// TODO: cidrCostTree affects connection handshake & optimistic unchoking
						// - only if weight properties (see ConfigurationManager) are not set to 0
						// - else, this just affects GUI of bass plugin, nice to have
						cidrCostTree_.addCidr(new Ip4Cidr(ipAddrCIDR), metric);
					}
				}
				
				onehopmap = onehopmap_tmp;
				nhopmap = nhopmap_tmp;
			}
		}
		
	}
	
	
	// TODO: mesh modification
	private List<MeshPathListEntry> getMeshPathList()
	{
		List<MeshPathListEntry> meshPathList = new ArrayList<MeshPathListEntry>();
		
		Scanner scan = null;
		
		try
		{
			scan = ProcessCall.runCommandWithOutput("./iw_mod", iwModPath, "--java",
					"dev", config_.getMESH_IFNAME(), "mpath", "dump");
			
			String rawDump = scan.nextLine();
			
			meshPathList = new ArrayList<MeshPathListEntry>(
				Arrays.asList(gson.fromJson(rawDump, MeshPathListEntry[].class)));
			
			meshPathList.removeAll(Collections.singleton(null));
			
		} catch (Exception e) {
			if (scan!=null) scan.close();
			System.out.println("\nException in getMeshPathList()\n");
			e.printStackTrace(); // TODO: remove debug print
			return new ArrayList<MeshPathListEntry>();
		}
		
		if (scan!=null) scan.close();
		return meshPathList;
	}
	
	
	//TODO: mesh modification
	public Map<String, Integer> sortMapByValues(Map<String, Integer> passedMap)
	{
		List<String> unsortedKeys = new ArrayList<String>(passedMap.keySet());
		List<Integer> unsortedMetrics = new ArrayList<Integer>(passedMap.values());
		
		List<Integer> sortedMetrics = new ArrayList<Integer>(passedMap.values());
		Collections.sort(sortedMetrics);
		
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		
		for (Integer metric : sortedMetrics)
		{
			for (int i=0; i<unsortedMetrics.size(); i++)
			{
				if (unsortedMetrics.get(i).equals(metric) && !sortedMap.containsKey(unsortedKeys.get(i)))
				{
					// store ALM metric -> negate cost value for best peer sorting
					// smallest metric = smallest cost = best peer
					// highest value in list will be least negative metric
					sortedMap.put(unsortedKeys.get(i), metric*(-1));
					
					break;
				}
			}
		}
		
		return sortedMap;
	}
	
	
	// TODO: mesh modification
	public Map<String, Integer> getsortedonehopmap()
	{
		return sortMapByValues(new HashMap<String, Integer>(onehopmap));
	}
	
	// TODO: mesh modification
	public Map<String, Integer> getsortednhopmap()
	{
		return sortMapByValues(new HashMap<String, Integer>(nhopmap));
	}
	
	
	
	// TODO: original constructor
//	public DummyPeerSelectionService(BassPlugin plugin) throws BassException
//	{
//		plugin_ = plugin;
//		cidrCostTree_ = new CidrCostTree();
//		if (plugin_.isDefaultMapEnabled())
//			cidrCostTree_.loadMapFromFile(plugin_.getMapFile().getValue());
//	}
	
	public CidrCostTree getCidrCostTree()
	{
		return cidrCostTree_;
	}
	
	// TODO: DummyPeerSelectionService -> function getPeerCosts()
	// - called by BassConnectionManager in function getGoodPeer() called by getNextOptimisticPeer()
	// - called by BassUnchokeManager in function getNextOptimisticPeer()
	// - these functions are only called, if weight properties are not set to 0 (e.g. bass strategy is used)
	// - we set them to 0 -> cidr cost tree values do not affect anything but GUI (nice for debugging)
	public SortedSet<PeerCost> getPeerCosts(Collection<PeerItem> peers) throws BassException
	{
		SortedSet<PeerCost> result;
		int cost;
		if (peers.isEmpty())
			throw new BassException("Empty peer list");
		result = new TreeSet<PeerCost>();
		for (PeerItem peer : peers) {
			cost = cidrCostTree_.getCost(new Ip4Address(peer.getIP()), DEFAULT_COST);
			result.add(new PeerCost(peer, cost));
		}
		return result;
	}
	
	public IView getUITab(BassUI bassUI) {
		if (uiTab_ == null)
			uiTab_ = new DummyTab(bassUI, cidrCostTree_);
		return uiTab_;
	}
	
}
