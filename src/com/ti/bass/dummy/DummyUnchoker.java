/*
 * Created on 21-Sep-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package com.ti.bass.dummy;

import java.util.ArrayList;

import com.ti.bass.core.BassPlugin;
import com.ti.bass.core.adapters.DownloadingUnchokerAdapter;

/**
 * Overrides the methods of the Vuze DownloadingUnchoker, logging method
 * invocations. 
 */
public class DummyUnchoker extends DownloadingUnchokerAdapter {

	BassPlugin			plugin_;

	public DummyUnchoker(BassPlugin plugin) {
		plugin_ = plugin;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void calculateUnchokes(int max_to_unchoke, ArrayList all_peers,
									boolean force_refresh,
									boolean check_priority_connections) {
		plugin_.getLogger().log(
			"calculateUnchokes() - all_peers size: " + all_peers.size()
				+ " - unchokes size: " + getUnchokesField().size()
				+ " - chokes size: " + getChokesField().size());
		
		// this is a good point where to ask ALTO service what to do:
		// - use utility methods getChokesField() and getUnchokesField()
		//   to access the chokes and unchokes fields (ArrayLists of 
		//   PEPeerTransport objects);
		// - to get the Peer IP, use 
		//   PEPeerTransport.getPeerItemIdentity().getIP();
		// - choose which Peer to choke and which Peer to unchoke, 
		//   using information from the ALTO service;
		// - set the chokes and the unchokes fields accordingly, using the
		//   utility methods setChokesField() and setUnchokesField().

		super.calculateUnchokes(max_to_unchoke, all_peers, force_refresh,
			check_priority_connections);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ArrayList getChokes() {
		plugin_.getLogger().log("getChokes()");
		return super.getChokes();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ArrayList getImmediateUnchokes(int max_to_unchoke, ArrayList all_peers) {
		plugin_.getLogger().log("getImmediateUnchokes()");
		return super.getImmediateUnchokes(max_to_unchoke, all_peers);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ArrayList getUnchokes() {
		plugin_.getLogger().log("getUnchokes()");
		return super.getUnchokes();
	}
}
