/* Created on 26-Oct-2009
 * Created by Telecom Italia
 * Copyright (c) 2009, Telecom Italia
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 *   Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.    
 *   Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the Telecom Italia nor the names of its contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY TELECOM ITALIA ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TELECOM ITALIA BE LIABLE FOR ANYDIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED ANDON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package com.ti.bass.dummy;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import org.gudy.azureus2.core3.util.SystemTime;
import org.gudy.azureus2.plugins.peers.PeerManager;
import com.ti.bass.core.BassPlugin;
import com.ti.bass.stats.*;
import com.ti.bass.stats.BassPeerStats.PeerInfo;
import java.lang.Math;

/**
 * Implementation of Bass StatsListener: it prints stats on local files when 
 * triggered by different StatsEvents
 */
public class DummyStatsPrinter implements StatsListener {

	private static String	TOTAL_FILENAME				= "total_report.csv";
	private static String	DOWNLOAD_SUMMARY_FILENAME	= "download_report.csv";
	private static String	STATUS_FILENAME				= "summary.csv";
	private PeerManager		peerManager_;
	private BassPlugin		plugin_;
	private String			savePath_;
	private Boolean			firstLine_;
	private Boolean			nowIsSeed_;
	private long 			timestamp_;
	private long 			lastUpdate_;

	public String getSavePath() {
		return savePath_;
	}

	public void setSavePath(String sp) {
		savePath_ = sp;
	}

	public DummyStatsPrinter(PeerManager peerManager, BassPlugin plugin) {
		peerManager_ = peerManager;
		plugin_ = plugin;
		firstLine_ = true;
		nowIsSeed_ = false;
		timestamp_ = SystemTime.getCurrentTime();
		lastUpdate_ = 0;
		setSavePath(plugin_.getStatsDir());
		try {
			new File(getSavePath() + File.separatorChar).mkdir();
			setSavePath(getSavePath() + File.separatorChar
				+ Math.abs((peerManager_.getDownload().getName()).hashCode()) + "-"
				+ getDateTime());
			new File(getSavePath()).mkdir();
		} catch (Exception ex) {
		}
	}

	public void statsEventOccured(StatsEvent event) {
		BassPeerStats bassPeerStats;
		
		switch (event.getType()) {
		case StatsEvent.PEER_STATS_ADDED:
		case StatsEvent.STATS_UPDATED:
			bassPeerStats = event.getBassPeerStats();
			saveStats(bassPeerStats, TOTAL_FILENAME);
			if (((SystemTime.getCurrentTime() - lastUpdate_) / 1000) > 10) {
				saveMyStatus(STATUS_FILENAME);
				lastUpdate_ = SystemTime.getCurrentTime();
			}
			break;
		case StatsEvent.PEER_MANAGER_REMOVED:
			break;
		case StatsEvent.SEEDING:
			if (!nowIsSeed_) {
				bassPeerStats = event.getBassPeerStats();
				saveStats(bassPeerStats, DOWNLOAD_SUMMARY_FILENAME);
				saveMyStatus(STATUS_FILENAME);
				nowIsSeed_ = true;
			}
			break;
		case StatsEvent.SHUTDOWN:
			bassPeerStats = event.getBassPeerStats();
			saveStats(bassPeerStats, TOTAL_FILENAME);
			break;
		}
	}
	
	/**
	 * Saves the stats contained in BassPeerStats structure on a file called filename
	 * 
	 * @param BassPeerStats
	 * @param filename
	 */
	private void saveStats(BassPeerStats TIps, String filename) {
		try {
		
			FileWriter out_ = new FileWriter(getSavePath() + File.separatorChar
				+ filename, false);
			out_.write("\"IP\",");
			out_.write("\"Outcoming\",");
			out_.write("\"Stats creation [msec]\",");
			out_.write("\"First connection attempt [msec]\",");
			out_.write("\"First succesful connection [msec]\",");
			out_.write("\"Connection time [msec]\",");
			out_.write("\"Download time [msec]\",");
			out_.write("\"Downrate AVG [Byte/sec]\",");
			out_.write("\"Downrate AVG in download periods [Byte/sec]\",");
			out_.write("\"Downrate PEAK [Byte/sec]\",");
			out_.write("\"Downloaded [Byte]\",");
			out_.write("\"Upload time[msec]\",");
			out_.write("\"Uprate AVG [Byte/sec]\",");
			out_.write("\"Uprate AVG in upload periods [Byte/sec]\",");
			out_.write("\"Uploaded [Byte]\"");
			out_.write("\n");
			for (PeerInfo pi : TIps.getPeerStats()) {
				out_.write("\"" + pi.getIp() + "\",");
				out_.write("\"" + !pi.getIsIncoming() + "\",");
				out_.write(pi.getTimeOfInfoCreation() + ",");
				out_.write(pi.getFirstConnectionAttempt() + ",");
				out_.write(pi.getFirstConnectionSuccessful() + ",");
				out_.write(pi.getTotalConnectionPeriod() + ",");
				out_.write(pi.getTotalConnectionPeriodInDownload() + ",");
				out_.write(pi.getAvgDownloadRate() + ",");
				
				if (pi.getTotalConnectionPeriodInDownload() != 0) 
					out_.write(pi.getDataReceived()*1000/pi.getTotalConnectionPeriodInDownload() + ",");
				else out_.write("0,");
				
				out_.write(pi.getPeakDownloadRate() + ",");
				out_.write(pi.getDataReceived() + ",");
				out_.write(pi.getTotalConnectionPeriodInUpload() + ",");
				out_.write(pi.getAvgUploadRate() + ",");
				if (pi.getTotalConnectionPeriodInUpload() != 0)
					out_.write(pi.getDataSent()*1000/pi.getTotalConnectionPeriodInUpload() + ",");
				else out_.write("0,");
				out_.write(pi.getDataSent() + "\n");
			}
			out_.flush();
			out_.close();
			
		} catch (Exception ex) {// Catch exception if any
			System.err.println("Error: " + ex.getMessage());
		}
	}

	/**
	 * Saves the current status of the client on a file called filename
	 * 
	 * @param filename
	 */
	@SuppressWarnings("static-access")
	private void saveMyStatus(String filename) {
		try {
			FileWriter out_ = new FileWriter(getSavePath() + File.separatorChar
				+ filename, true);
			if (firstLine_) {
				out_.write("\"Time [sec]\",\"Status\",\"Download rate [Byte/sec]\",\"Downloaded [KByte]\",\"Upload rate [Byte/sec]\",\"Uploaded [KByte]\",\"Leechers\",\"Seeds\",\"Impact on connection [%]\",\"Impact on unchoke [%]\"");
				out_.write("\n");
				firstLine_ = false;
			}
			out_.write((SystemTime.getCurrentTime()-timestamp_)/1000+",");
			out_.write(peerManager_.getDownload().ST_NAMES[peerManager_.getDownload().getState()]+",");
			out_.write(peerManager_.getStats().getDownloadAverage()+",");
			out_.write((peerManager_.getStats().getDownloaded())/1000+",");
			out_.write(peerManager_.getStats().getUploadAverage()+",");
			out_.write(peerManager_.getStats().getUploaded()/1000+",");
			out_.write(peerManager_.getStats().getConnectedLeechers()+",");
			out_.write(peerManager_.getStats().getConnectedSeeds()+",");
			out_.write(plugin_.getPeerSelectionWeightOnConnection()+",");
			out_.write(plugin_.getPeerSelectionWeightOnUnchoke()+"");
			out_.write("\n");
			out_.flush();
			out_.close();
			
		} catch (Exception ex) {// Catch exception if any
			System.err.println("Error: " + ex.getMessage());
		}
		
	}

	/**
	 * It returns a string with the DateTime infos
	 * 
	 * @return DateFormat Current date in the format yyyy_MM_dd-HH_mm_ss
	 */
	private String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
}
